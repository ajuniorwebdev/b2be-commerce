<tbody>
@if(count($orders) > 0)
    @foreach($orders as $os)
        <tr>
            <td><strong>{{ $os->number_order }}</strong></td>
            <td><strong>{{ ($os->numpedvan) ? $os->numpedvan : '-' }}</strong></td>
            <td><strong>{{ ($os->invoice) ? $os->invoice : '-' }}</strong></td>
            <td>{{ $os->dt_order }}</td>
            <td>Pedido Eletrônico</td>
            <td>{{ $os->fantasia }}</td>
            <td>{{ $os->status_name }}</td>
            <td class="center">R$ {{ number_format($os->total, '2', ',', '.') }}</td>
            <td class="center">
                <span title="Visualizar e Reimprimir Faturas" class="ver-faturas"><span class="icon-pedidos-1 pedidos-toggle" data-id="{{ $os->number_order }}"></span></span>
                <a href="{{ route('orders.details', ['number_order' => $os->number_order]) }}" title="Ver Detalhes do Pedido"><span class="icon-carrinho-1"></span></a>
            </td>
        </tr>
        <tr class="faturas-hidden" data-id="{{ $os->number_order }}">
            <td colspan="8">
                <table>
                    <thead>
                    <tr>
                        <th>Nº Parcela</th>
                        <th>Data de Vencimento</th>
                        <th>Status</th>
                        <th class="center">Valor</th>
                        <th>Data de Pagamento</th>
                        <th class="center"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="12" style="text-align: center;">
                            <div class="loader" style="text-align: center; height: 100px; line-height: 140px;">
                                <svg width="48" height="48" viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg" version="1.1">
                                    <path d="M 150,0 a 150,150 0 0,1 106.066,256.066 l -35.355,-35.355 a -100,-100 0 0,0 -70.711,-170.711 z" fill="#0071bb">
                                        <animateTransform attributeName="transform" attributeType="XML" type="rotate" from="0 150 150" to="360 150 150" begin="0s" dur=".5s" fill="freeze" repeatCount="indefinite"></animateTransform>
                                    </path>
                                </svg>
                            </div>
                            <h4>Aguarde um instante...</h4>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="12" style="text-align: center;">
            <h4>Nenhum pedido foi encontrado</h4>
        </td>
    </tr>
@endif
</tbody>