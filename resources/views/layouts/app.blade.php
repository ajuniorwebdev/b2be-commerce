<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ (isset($config['title'])) ? $config['title'] : 'Exfar Distribuidora - Portal de Vendas' }}</title>
    <meta name="author" content="B2B">
    <meta name="description" content="">
    <meta name="keyword" content="">
    <meta name="robots" content="index, follow">

    <link rel="icon" href="{{ asset('favicon32x32.png') }}" sizes="32x32">
    <link rel="icon" href="{{ asset('favicon192x192.png') }}" sizes="192x192">
    <link rel="apple-touch-icon" href="{{ asset('favicon180x180.png') }}">

    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app-p.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css"/>
    <!-- Google Tag Manager -->
    <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MVLNDPG');
    </script>
    <!-- End Google Tag Manager -->
</head>
<body class="{{ (isset($config['body_class'])) ? $config['body_class'] : '' }}">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MVLNDPG"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header>
    <div class="wrapper afterclear">
        <div class="site-branding">
            <h1 class="site-title">
                <a href="{{ route('/') }}"><span>GrupoMED Online</span></a>
            </h1>
        </div>
        <!-- MOBILE-MENU-CONTROL - Substitui o menu desktop e traz o controle para mostrar ou esconder os itens de menu em dispositivos menores que 990px -->
        <div class="mobile-menu-control ferramentas-topo">
            <ul>
                @auth
                    <li class="menu-item carrinho">
                        <a href="{{ route('cart') }}"><span class="icon-carrinho-1"><span class="alert">{{ ($header['qt'] == 0) ? 0 : $header['qt'] }}</span></span></a>
                    </li>
                @endauth
                <li class="menu-item toggle-menu">
                    <a href="javascript:void(0)"> <span class="icon-menu-1"></span></a>
                </li>
            </ul>
        </div>

        <nav class="menu-principal">
            <div class="ferramentas-topo">
                <ul>
                    @auth
                        @role('customer')
                            <li class="menu-item area-cliente">
                                <a href="javascript:void(0)"><span class="icon-cliente-1">{{--<span class="alert">1</span>--}}</span><span class="texto"> Área do Cliente</span></a>
                                <ul>
                                    <li>
                                        <form>
                                            <label for="filial-topo">Filial Selecionada</label>
                                            <select name="filial-topo" id="filial-topo">
                                                @foreach($header['cli']['subsidiary'] as $sub)
                                                    <option value="{{ $sub->codcli }}" {{ ($sub->codcli == $header['cli']['selected']) ? 'selected' : '' }}>{{ $sub->cliente }} - {{ $sub->cgcent }}</option>
                                                @endforeach
                                            </select>
                                        </form>
                                    </li>
                                    <li><a href="{{ route('overview') }}">Visão Geral</a></li>
                                    <li><a href="{{ route('account') }}">Minha Conta</a></li>
                                    <li><a href="{{ route('branchs') }}">Minhas Filiais</a></li>
                                    <li><a href="{{ route('orders') }}">Meus Pedidos</a></li>
{{--                                    <li><a href="chat-representante.html">Fale com o seu representante</a></li>--}}
                                    <li><a href="javascript:void(0)" onclick="$('form#logout').submit()">Sair</a></li>
                                    <form id="logout" action="{{ route('logout') }}" method="post" style="display:none">@csrf</form>
                                </ul>
                            </li>
                        @elserole('seller')
                            <li class="menu-item area-cliente">
                                <a href="javascript:void(0)"><span class="icon-cliente-1"><span class="alert">1</span></span><span class="texto"> Área do Repres.</span></a>
                                <ul>
                                    <li>
                                        <form>
                                            <label for="cliente-topo">Cliente Selecionado</label>
                                                @foreach($header['cli']['portfolio'] as $portfolio)
                                                    @if($portfolio->codcli == $header['cli']['selected'])
                                                        <a href="{{ route('portfolio') }}" style="display: inline-flex;flex: 1;flex-direction: column;font-size: 0.8rem;color: #f39e1f;}">
                                                            <span>{{ $portfolio->cliente }}</span>
                                                            <span>{{ $portfolio->cgcent }}</span>
                                                        </a>
                                                    @endif
                                                @endforeach
                                            @if(count($header['cli']['branchs']) > 1)
                                                <label for="filial-topo">Filial do Cliente Selecionada</label>
                                                <select name="filial-topo" id="filial-topo">
                                                    @foreach($header['cli']['branchs'] as $sub)
                                                        <option value="{{ $sub->codcli }}" {{ ($sub->codcli == $header['cli']['selected']) ? 'selected' : '' }}>{{ $sub->cliente }} - {{ $sub->cgcent }}</option>
                                                    @endforeach
                                                </select>
                                            @endif
                                        </form>
                                    </li>
                                    <li><a href="{{ route('overview') }}">Visão Geral</a></li>
                                    <li><a href="{{ route('account') }}">Minha Conta</a></li>
                                    <li><a href="{{ route('portfolio') }}">Carteira de Clientes</a></li>
                                    <li><a href="{{ route('orders') }}">Pedidos de Clientes</a></li>
                                {{--<li><a href="representante_chat-clientes.html">Fale com os seus clientes</a></li>--}}
                                    <li><a href="javascript:void(0)" onclick="$('form#logout').submit()">Sair</a></li>
                                    <form id="logout" action="{{ route('logout') }}" method="post" style="display:none">@csrf</form>
                                </ul>
                            </li>
                        @endrole
                        <!-- PRAZO DESEJADO -->
                        <li class="menu-item prazo">
                            @if(isset($header['pp']))
                                <a href="javascript:void(0)">
                                    <span class="icon-prazo-1"></span>
                                    <span class="texto">Prazo<br><strong>{{ $header['pp']['pp_selected'][0]->numdias }}</strong> dias</span></a>
                                <ul>
                                    <li>
                                        <form>
                                            <label for="prazo-topo">Altere o <strong>Prazo Desejado</strong></label>
                                            <select name="prazo-topo" id="prazo-topo">
                                                @foreach($header['pp']['plans'] as $pp)
                                                    <option value="{{ $pp->codplpag }}" {{ ($pp->codplpag == $header['pp']['pp_selected'][0]->codplpag) ? 'selected' : '' }}>{{ $pp->descricao }}</option>
                                                @endforeach
                                            </select>
                                        </form>
                                    </li>
                                    <li>
                                        <span class="prazo-medio"><span class="pp-description" style="display:inline-flex;">{{ $header['pp']['pp_selected'][0]->desc_plan }}</span> - P. Médio: <span class="pp-numdias" style="display:inline-flex;">{{ $header['pp']['pp_selected'][0]->numdias }}</span> dias</span>
                                        <span class="valor-minimo">O valor mínimo de pedidos para o prazo escolhido é de <strong class="laranja">R$ <span class="pp-min" style="display: inline-flex;">{{ number_format($header['pp']['pp_selected'][0]->vlminpedido, '2', ',', '.') }}</span></strong></span>
                                    </li>
                                </ul>
                            @endif
                        </li>
                        {{-- Carrinho --}}
                        <li class="menu-item carrinho">
                            <a href="javascript:void(0)" class="cart-info"> <span class="icon-carrinho-1"><span class="alert">{{ ($header['qt'] == 0) ? 0 : $header['qt'] }}</span></span> <span class="texto"> <span class="preco">R$<strong>{{ $header['subtotal'] }}</strong></span></span></a>
                            <ul>
                                <li>
                                    <form>
                                        <table class="tabela-produtos carrinho">
                                            @if(count($header['cart_items']) > 0)
                                                <tbody>
                                                @foreach($header['cart_items'] as $item)
                                                    <tr>
                                                        <td width="70" class="imagem">
                                                            <img src="{{ asset('images/produtos/box_default.jpeg') }}" width="50px"/>
                                                        </td>
                                                        <td class="desc-prod">{{ $item->descricao }}</td>
                                                        <td class="col-qtd">
                                                            <div class="qtd">
                                                                <span class="qtd-btn minus">-</span>
                                                                <input data-id="{{$item->codprod}}" class="qtd" type="text" name="qtd" value="{{ $item->qt }}" autocomplete="off">
                                                                <span class="qtd-btn plus">+</span>
                                                            </div>
                                                        </td>
                                                        <td class="col-valor"><span class="preco">R$ {{ number_format($item->subtotal, '2', ',', '.') }}</span></td>
                                                        <td>
                                                            <span class="icon-fechar-small del"></span>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            @else
                                                <tbody class="empty">
                                                    <tr>
                                                        <td>Carrinho Vazio!</td>
                                                    </tr>
                                                </tbody>
                                            @endif

                                            <tfoot>
                                            <tr>
                                                <td colspan="2">Qtd: <span class="qt">{{ $header['qt'] }}</span></td>
                                                <td colspan="3" class="col-valor"><span class="preco">R$ <strong>{{ $header['subtotal'] }}</strong></span></td>
                                            </tr>
                                            <tr>
                                            <td colspan="2">Limite de Crédito: R$ {{ number_format(floatval($header['limite_credito'][0]->limcred), '2', ',', '.')}}</td>
                                                <td colspan="3" class="col-valor">
                                                    <a href="{{ route('cart') }}" class="bt-principal alignright">Ver Carrinho</a>
                                                </td>
                                            </tr>
                                            </tfoot>

                                        </table>
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @else
                        <li class="menu-item area-cliente login">
                            <a href="{{ route('login') }}"><span class="icon-cliente-1"></span><span class="texto">ENTRAR</span></a>
                        </li>
                    @endauth
                </ul>
            </div>

            @auth
                <div class="busca-form">
                    <form id="header_search_form">
                        <input type="text" name="primary_search" id="busca-topo" placeholder="Pesquise por Produtos, Marcas, EAN...">
                        <button type="submit" class="bt-busca icon-busca"><span>Pesquisar</span></button>
                        <div class="form-floater">
                            <table class="tabela-produtos busca-floater">
                                <tbody>
                                <tr>
                                    <td width="70" class="imagem">
                                        <img src="{{asset('images/produtos/produto-01.jpg')}}" alt="NOME DO PRODUTO" title="NOME DO PRODUTO" width="50px"/>
                                    </td>
                                    <td>AMOXIC 500MG 15 CP MEDLEY (C1)</td>
                                    <td class="col-qtd">
                                        <div class="qtd"><span class="qtd-btn">-</span><input type="number" name="qtd-1" value="1"/><span class="qtd-btn">+</span></div>
                                    </td>
                                    <td class="col-valor"><span class="preco">R$99.999,00</span></td>
                                    <td width="200">
                                        <button type="submit" class="bt-principal">Adicionar ao Carrinho</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="70" class="imagem">
                                        <img src="{{asset('images/produtos/produto-02.jpg')}}" alt="NOME DO PRODUTO" title="NOME DO PRODUTO" width="50px"/>
                                    </td>
                                    <td>AMOXIC 500MG 15 CP MEDLEY (C1)</td>
                                    <td class="col-qtd">
                                        <div class="qtd"><span class="qtd-btn">-</span><input type="number" name="qtd-1" value="1"/><span class="qtd-btn">+</span></div>
                                    </td>
                                    <td class="col-valor"><span class="preco">R$99.999,00</span></td>
                                    <td width="200">
                                        <button type="submit" class="bt-principal">Adicionar ao Carrinho</button>
                                    </td>
                                </tr>
                                <tbody>
                            </table>
                        </div>
                    </form>
                </div>
            @endauth
        </nav>
    </div>
@auth
    <!-- MENU SECUNDÁRIO -->
        <div class="menu-sec">
            <div class="wrapper">
                <ul>
                    <!-- OFERTA RELÂMPAGO -->
                    @if(isset($header['oferta'][0]))
                        <li class="oferta-relampago"><a href="javascript:void(0)">Oferta Relâmpago</a>
                            <ul class="produtos-list">
                                <li class="produto-item">
                                    <div class="miniatura">
                                        @if(strpos(get_headers('https://winthordanfe.s3.us-east-2.amazonaws.com/PRODUTO/'. $header['oferta'][0]->img)[0], '404') == 0)
                                            @php $img = 'https://winthordanfe.s3.us-east-2.amazonaws.com/PRODUTO/'.$header['oferta'][0]->img; @endphp
                                        @else
                                            @php $img = 'https://winthordanfe.s3.us-east-2.amazonaws.com/PRODUTO/caixa_padrao.jpeg'; @endphp
                                        @endif
                                        <a href="javascript:void(0)"><img src="{{ $img }}" width="300" height="300"/></a>
                                    </div>
                                    <div class="descricao">
                                        <form id="oferta">
                                            <div class="desconto hot valign-container">
                                                <div class="valign"><strong>{{ number_format($header['oferta'][0]->percdesc, '0',',','.') }}%</strong> <span>desconto</span></div>
                                            </div>
                                            <h3 class="nome-produto">
                                                <a href="javascript:void(0)">{{ $header['oferta'][0]->descricao }}</a>
                                            </h3>
                                            <div class="precos">
                                                <span class="preco-disabled">R$ {{ number_format($header['oferta'][0]->pvenda, '2',',','.') }}</span>
                                                <span class="preco">R$ {{ number_format($header['oferta'][0]->price, '2',',','.') }}</span>
                                            </div>
                                            <div class="qtd">
                                                <span class="qtd-btn minus">-</span>
                                                <input class="qtd" type="text" data-id="{{ $header['oferta'][0]->codprod }}" data-prm="{{ $header['oferta'][0]->codpromocaomed }}" name="qtd" value="0">
                                                <span class="qtd-btn plus">+</span>
                                            </div>
                                            <button type="submit" class="bt-principal">Adicionar ao Carrinho</button>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    @endif
                    <li class="produtos-az"><a href="{{route('products', ['list' => 'az'])}}">Produtos de A a Z</a></li>
{{--                    <li><a href="#">Medicamentos</a></li>--}}
{{--                    <li><a href="#">HBS</a></li>--}}
{{--                    <li><a href="produtos-loja-personalizada.html">Barba Forte</a></li>--}}
{{--                    <li><a href="#">Descontos Imperdíveis</a></li>--}}
                </ul>
            </div>
        </div>
    @endauth

    {{--MENU MOBILE - Menu para versões menores do que 990px--}}
    @auth
        <div class="overlay">
            <nav class="menu-mobile">
                <ul class="wrapper">
                    <li class="produtos-az"><a href="{{route('products', ['list' => 'az'])}}">Produtos de A a Z</a></li>
                    @role('customer')
                    <li class="area-cliente"><a href="javascript:void(0)">Área do Cliente <!-- <span class="alert">1</span> --></a>
                        <span class="toggle"></span>
                        <ul class="submenu-area-cliente">
                            <li>
                                <form>
                                    <label for="filial-topo">Filial Selecionada</label>
                                    <select name="filial-topo" id="filial-topo">
                                        @foreach($header['cli']['subsidiary'] as $sub)
                                            <option value="{{ $sub->codcli }}" {{ ($sub->codcli == $header['cli']['selected']) ? 'selected' : '' }}>{{ $sub->cliente }} - {{ $sub->cgcent }}</option>
                                        @endforeach
                                    </select>
                                </form>
                            </li>
                            <li><a href="{{ route('overview') }}">Visão Geral</a></li>
                            <li class="minha-conta"><a href="{{ route('account') }}">Minha Conta</a></li>
                            <li class="minhas-filiais"><a href="{{ route('branchs') }}">Minhas Filiais</a></li>
                            <li class="meus-pedidos"><a href="{{ route('orders') }}">Meus Pedidos</a></li>
                        </ul>
                    </li>
                    @elserole('seller')
                    <li class="area-cliente"><a href="#">Área do Representante <span class="alert">1</span></a><span class="toggle"></span>
                        <ul class="submenu-area-cliente">
                            <li>
                                <form>
                                    <label for="cliente-topo">Cliente Selecionado</label>
                                    @foreach($header['cli']['portfolio'] as $portfolio)
                                        @if($portfolio->codcli == $header['cli']['selected'])
                                            <a href="{{ route('portfolio') }}" style="display: inline-flex;flex: 1;flex-direction: column;font-size: 0.8rem;color: #f39e1f;}">
                                                <span>{{ $portfolio->cliente }}</span>
                                                <span>{{ $portfolio->cgcent }}</span>
                                            </a>
                                        @endif
                                    @endforeach
                                    @if(count($header['cli']['branchs']) > 1)
                                        <label for="filial-topo">Filial do Cliente Selecionada</label>
                                        <select name="filial-topo" id="filial-topo">
                                            @foreach($header['cli']['branchs'] as $sub)
                                                <option value="{{ $sub->codcli }}" {{ ($sub->codcli == $header['cli']['selected']) ? 'selected' : '' }}>{{ $sub->cliente }} - {{ $sub->cgcent }}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </form>
                            </li>
                            <li><a href="{{ route('overview') }}">Visão Geral</a></li>
                            <li class="minha-conta"><a href="{{ route('account') }}">Minha Conta</a></li>
                            <li class="minhas-filiais"><a href="{{ route('portfolio') }}">Carteira de Clientes</a></li>
                            <li class="meus-pedidos"><a href="{{ route('orders') }}">Pedidos de Clientes</a></li>
                        </ul>

                    </li>
                    @endrole
                    <li class="prazo-desejado"><a href="#">Prazo Desejado</a>
                        <span class="toggle"></span>
                        @if(isset($header['pp']))
                            <ul>
                                <li>
                                    <form>
                                        <label for="prazo-topo">Altere o <strong>Prazo Desejado</strong></label>
                                        <select name="prazo-topo" id="prazo-topo">
                                            @foreach($header['pp']['plans'] as $pp)
                                                <option value="{{ $pp->codplpag }}" {{ ($pp->codplpag == $header['pp']['pp_selected'][0]->codplpag) ? 'selected' : '' }}>{{ $pp->descricao }}</option>
                                            @endforeach
                                        </select>
                                    </form>
                                </li>
                                <li>
                                    <span class="prazo-medio"><span class="pp-description" style="display:inline-flex;">{{ $header['pp']['pp_selected'][0]->desc_plan }}</span> - P. Médio: <span class="pp-numdias" style="display:inline-flex;">{{ $header['pp']['pp_selected'][0]->numdias }}</span> dias</span>
                                    <span class="valor-minimo">O valor mínimo de pedidos para o prazo escolhido é de <strong class="laranja">R$ <span class="pp-min" style="display: inline-flex;">{{ number_format($header['pp']['pp_selected'][0]->vlminpedido, '2', ',', '.') }}</span></strong></span>
                                </li>
                            </ul>
                        @endif
                    </li>
                    <li class="carrinho-compras erro"><a href="{{ route('cart') }}">Carrinho de Compras <!--<span class="alert">2</span>--></a></li>
                    <li class="sair"><a href="javascript:void(0)" onclick="$('form#logout').submit()">Sair</a></li>
                    <form id="logout" action="{{ route('logout') }}" method="post" style="display:none">@csrf</form>
                </ul>
                <div class="menu-bottom">
                    <ul class="wrapper">
                        @if(isset($header['oferta'][0]))
                        <li class="oferta-relampago"><a href="javascript:void(0)">Oferta Relâmpago</a>
                                <ul class="produtos-list">
                                    <li class="produto-item">
                                        <div class="miniatura">
                                            @if(strpos(get_headers('https://winthordanfe.s3.us-east-2.amazonaws.com/PRODUTO/'. $header['oferta'][0]->img)[0], '404') == 0)
                                                @php $img = 'https://winthordanfe.s3.us-east-2.amazonaws.com/PRODUTO/'.$header['oferta'][0]->img; @endphp
                                            @else
                                                @php $img = 'https://winthordanfe.s3.us-east-2.amazonaws.com/PRODUTO/caixa_padrao.jpeg'; @endphp
                                            @endif
                                            <a href="javascript:void(0)"><img src="{{ $img }}" width="300" height="300"/></a>
                                        </div>
                                        <div class="descricao">
                                            <form id="oferta">
                                                <div class="desconto hot valign-container">
                                                    <div class="valign"><strong>{{ number_format($header['oferta'][0]->percdesc, '0',',','.') }}%</strong> <span>desconto</span></div>
                                                </div>
                                                <h3 class="nome-produto">
                                                    <a href="javascript:void(0)">{{ $header['oferta'][0]->descricao }}</a>
                                                </h3>
                                                <div class="precos">
                                                    <span class="preco-disabled">R$ {{ number_format($header['oferta'][0]->pvenda, '2',',','.') }}</span>
                                                    <span class="preco">R$ {{ number_format($header['oferta'][0]->price, '2',',','.') }}</span>
                                                </div>
                                                <div class="qtd">
                                                    <span class="qtd-btn minus">-</span>
                                                    <input class="qtd" type="text" data-id="{{ $header['oferta'][0]->codprod }}" name="qtd" value="0">
                                                    <span class="qtd-btn plus">+</span>
                                                </div>
                                                <button type="submit" class="bt-principal">Adicionar ao Carrinho</button>
                                            </form>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        @endif
                        <li class="busca-form">
                            <form id="header_search_form">
                                <input type="text" name="primary_search" id="busca-topo" placeholder="Pesquise por Produtos, Marcas, EAN...">
                                <button type="submit" class="bt-busca icon-busca"><span>Pesquisar</span></button>
                            </form>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    @else
        <div class="overlay">
            <nav class="menu-mobile">
                <ul class="wrapper">
                    <li><a href="{{ route('login') }}">Entrar</a></li>
                    <li><a href="{{ route('register') }}">Primeiro Acesso</a></li>
                    <li><a href="{{ route('client.request.register') }}">Solicitar Cadastro</a></li>
                    <li><a href="https://www.exfar.com.br/institucional">Institucional</a></li>
                    <li><a href="http://wiki.exfar.com.br/doku.php">Como Usar?</a></li>
                </ul>
            </nav>
        </div>
    @endauth
</header>

@yield('content')

@php
    $url_path_info = explode('/', url()->current());
@endphp

@if(isset($config['fullbanner']) && $config['fullbanner'])
    <div class="fullbanner">
        <a href="javascript:void(0)"><img src="{{ asset('/images/banners/banner_giovanna_baby_footer.jpeg') }}"/></a>
    </div>
@endif

@if(isset($config['midia']) && $config['midia'])
   <div class="midia">
         <div class="wrapper small"> 
             <div class="catalogo center"> 
                 <h4><a href="https://www.yumpu.com/pt/document/view/63575439/exfar-catalogo-06-2020" target="_blank">Catálogos</a></h4>
                 <div class="embed-responsive">
                     <iframe style="width:525px; height:374px;" src="https://www.yumpu.com/pt/embed/view/ejErDW058k9nUuAG" frameborder="0" allowfullscreen="true"  allowtransparency="true"></iframe>
                 </div>
             </div> 
             <div class="video center"> 
                 <h4><a href="https://www.youtube.com/channel/UCW4Ra34U2MCTg-51yqxGk5g" target="_blank">Vídeos</a></h4> 
                 <div class="embed-responsive"> 
                     <iframe width="560" height="315" src="https://www.youtube.com/embed/yLgeBj9wg6g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
                 </div> 
             </div> 
         </div> 
    </div>
@endif

<footer class="afterclear">
    {{-- <div class="newsletter">
        <div class="wrapper small">
            <div class="content">
                <span class="icon-mail"></span>
                 <div class="description">
                     <h3>Cadastre-se em nossa Newsletter</h3>
                     <p>Receba as promoções especiais da Exfar para a sua farmácia! em seu e-mail!:</p>
                 </div>
             </div>
             <form id="form-newsletter">
                 <input type="text" name="email_newsletter" id="email_newsletter" placeholder="E-mail">
                 <button type="submit"><span>Enviar</span></button>
             </form>
         </div>
     </div> --}}
    <div class="footer-info">
        <div class="wrapper">
            <div class="footer-content menu-footer">
                <ul>
                    @auth
                        <li><a href="http://wiki.exfar.com.br" target="_blank">Como usar?</a></li>
                        <li><a href="{{ route('products', ['list' => 'az']) }}">Produtos de A a Z</a></li>
                        <li><a href="{{ route('cart') }}">Carrinho de Compras</a></li>
                        <li><a href="{{ route('account') }}">Minha Conta</a></li>
                        <li><a href="{{ route('branchs') }}">Minhas Filiais</a></li>
                        <li><a href="{{ route('orders') }}">Meus Pedidos</a></li>
                        <li><a href="https://www.exfar.com.br/institucional" target="_blank">Institucional</a></li>
                    @else
                        <li><a href="http://wiki.exfar.com.br" target="_blank">Como usar?</a></li>
                        <li><a href="https://www.exfar.com.br/institucional" target="_blank">Institucional</a></li>
                    @endauth
                </ul>
            </div>
            <div class="footer-content televendas">
                <span class="icon-televendas"></span>
                <h3>SAC</h3>
                <h4><a href="tel:(85) 3017-9300">(85) 3017-9300 </a></h4>
                <h4><a href="tel:(85) 9 8154-2366">(85) 9 8154-2366 </a></h4>
                <p>Horário de Atendimento: Segunda a Sexta das 8h às 17h</p>
            </div>
{{--            <div class="footer-content central-atendimento">--}}
{{--                <span class="icon-central-atendimento"></span>--}}
{{--                <h3>Central de Atendimento</h3>--}}
{{--                <h4><a href="tel:0800 650 3036">0800 650 3036 </a></h4>--}}
{{--                <p>Horário de Atendimento: Segunda a Sexta das 8h às 18h</p>--}}
{{--            </div>--}}
            <div class="footer-content suporte-ti">
                <span class="icon-suporte-ti"></span>
                <h3>SUPORTE</h3>
                <h4><a href="tel:">(85) 9 9405-7255</a></h4>
                <p>Horário de Atendimento: Segunda a Sexta das 17h às 20h</p>
                <p>(Celular / WhatsApp)</p>
            </div>
        </div>
    </div>
    <div class="site-branding">
        <div class="wrapper">
            <h1 class="site-title">
                <a href="{{ route('/') }}"><span>Exfar Distribuidora</span></a>
            </h1>
{{--            <ul class="distribuidoras">--}}
{{--                <li class="distribuidora splog"><span></span></li>--}}
{{--                <li class="distribuidora medicar"><span></span></li>--}}
{{--                <li class="distribuidora vazmed"><span></span></li>--}}
{{--                <li class="distribuidora mglog"><span></span></li>--}}
{{--                <li class="distribuidora mundimed"><span></span></li>--}}
{{--                <li class="distribuidora canedo"><span></span></li>--}}
{{--            </ul>--}}
            <p class="center description">A Exfar Distribuidora de Medicamentos tem como principal objetivo levar saúde e promover qualidade de vida aos seus clientes devidamente cadastrados e com os alvarás e a documentação obrigatória em dia.</p>
        </div>
    </div>
</footer>

<script src="{{mix('js/app.js')}}"></script>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script>
  AOS.init({
    offset: 200,
    duration: 600,
    easing: 'ease-in-sine',
    delay: 100,
  });
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="{{mix('js/main.js')}}"></script>
@yield('js')
</body>
</html>
