<?php
/**
 * Taxonomy
 *
 * @author
 */

class Taxonomy {
	/**
	 * ID da Taxonomy
	 * @var string
	 */
	public $id;

	/**
	 * Nome da Taxonomy
	 * @var string
	 */
	public $name;

	/**
     * Nome, no plural, do custom post type
     * @var string
     */
    private $plural_name = null;

    /**
     * Opções padrões para criação da taxonomy
     * @var array
     */
    public $opts = array(
        'hierarchical'=> true,
        'rewrite' => array(
        	'with_front'   	=> false,
        	'hierarchical' 	=> true
        )
    );

    private $parent = null;

    public function __construct($id, $name)
    {
    	$this->id    	= $id;
        $this->setName($name);
    }

    /**
     * Modificar alguma opção da taxonomy
     * @param string $name Nome da opção
     * @param mixed $value Valor da opção
     */
    public function option($name, $value)
    {
        $this->opts[$name] = $value;

        if ($name == 'hierarchical' && $value === false)
        {
            $this->opts['rewrite']['hierarchical'] = false;
        }
        return $this;
    }

    /**
     * Modificar slug do custom post type
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->opts['rewrite']['slug'] = $slug;
        return $this;
    }

    /**
     * Modificar nome do custom post type
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;

        if (is_null($this->plural_name))
        {
            $this->plural_name = $this->name . 's';
        }
        return $this;
    }

    public function setParent(Taxonomy $parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * Modificar nome em plural do custom post type
     * @param string $name
     */
    public function setPluralName($plural_name)
    {
        $this->plural_name = $plural_name;
        return $this;
    }

    public function register($post_type)
    {
    	$this->opts['labels'] = array(
            'name'              => __( $this->plural_name ),
            'singular_name'     => __( $this->name),
            'search_items'      => __( 'Procurar ' . $this->name ),
            'all_items'         => __( 'Todas as ' . $this->plural_name ),
            'parent_item'       => __( $this->name . ' pai' ),
            'parent_item_colon' => __( $this->name . ' pai:' ),
            'edit_item'         => __( 'Editar ' . $this->name ),
            'update_item'       => __( 'Atualizar ' . $this->name ),
            'add_new_item'      => __( 'Adicionar ' . $this->name ),
            'new_item_name'     => __( 'Nova ' . $this->name ),
            'menu_name'         => __( $this->plural_name )
        );

    	register_taxonomy($this->id, $post_type, $this->opts);
    }
}
