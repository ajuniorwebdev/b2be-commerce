<?php
/**
 * Column
 *
 * @author
 */

class Column {
	/**
	 * Identificação da coluna
	 * @var string
	 */
	public $id;

	/**
	 * Título da coluna
	 * @var string
	 */
	public $title;

	/**
	 * WP Post
	 * @var object
	 */
	private $post = null;

	/**
	 * Evento de carregamento da coluna
	 * @var function
	 */
	public $onLoad = null;

	/**
	 * Construtor
	 * @param string $id
	 * @param string $title
	 * @return void
	 */
	public function __construct($id, $title)
	{
		global $post;

		$this->id = $id;
		$this->title = $title;
		$this->post = $post;
	}

	/**
	 * Carregar coluna
	 * @param string $column
	 */
	public function load($post)
	{
		$value = get_post_meta($post->ID, $this->id, true);

		if (!is_null($this->onLoad))
		{
			call_user_func($this->onLoad, $value, $this->post);
		}
		else
		{
			echo $value;
		}
	}
}
