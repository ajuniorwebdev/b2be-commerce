<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientRequest extends Model
{
    protected $fillable = [
        'id', 'cnpj', 'ie', 'razao_social', 'fantasia', 'rede', 'email_contato', 'email_cobranca', 'email_xml', 'telefone_fixo', 'telefone_celular', 'nome', 'cpf', 'email',
        'celular', 'comprador_med_nome', 'comprador_med_email', 'comprador_med_tel_fixo', 'comprador_med_tel_celular', 'comprador_per_nome', 'comprador_per_email', 'comprador_per_tel_fixo',
        'comprador_per_tel_celular', 'condicao_comercial', 'created_at', 'updated_at'
    ];

    protected $table = 'client_request';

}
