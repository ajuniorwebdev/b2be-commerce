<?php

namespace App\Http\Controllers;

use Exception;
use App\User;
use App\Cart;
use App\Client;
use App\Pedido;
use App\ProfileUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Validation\ValidationException as ValidationException;

class AccountController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = User::where(['id' => Auth::id()])->first();
        if($user->hasRole('customer')) {
            $current_user = Client::where(['codcli' => $user->codcli])->first();
        }
        else if($user->hasRole('seller')) {
            $current_user = ProfileUser::where(['codusur' => $user->codusur])->first();
        }

        return view('account.overview', compact('current_user'));
    }

    public function account()
    {
        $user = User::where(['id' => Auth::id()])->first();
        if($user->hasRole('customer')) {

            $client = Client::with(['seller'])
                ->where(['codcli' => $user->codcli])->first();

            return view('account.account', compact('client'));
        }
        else if($user->hasRole('seller')) {
            $profile_user = ProfileUser::where(['codusur' => $user->codusur])->first();
            return view('account.account-profileuser', compact('profile_user'));
        }
    }

    /**
     * Handle a change password by user.
     *
     * @param \Illuminate\Http\Request $request
     * @throws ValidationException
     */
    public function passwordChange(Request $request)
    {
        $this->validationPasswordChange($request->all())->validate();
        try {
            $user = User::where(['id' => Auth::id()])->first();
            if($user && Hash::check($request->input('current_password'), $user->password)) {
                User::where(['id' => Auth::id()])
                    ->update(['password' => Hash::make($request->input('password'))]);

                return response()
                    ->redirectToRoute('account')
                    ->with('success', ['msg' =>'Senha alterada com sucesso.']);
            }

            throw new Exception('Senha atual não confere');
        }
        catch(Exception $e) {
            report($e);

            throw ValidationException::withMessages([
                'current_password' => $e->getMessage(),
            ]);
        }
    }

    /**
     *
     * Method that validate a password change form
     *
     * @param $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validationPasswordChange($data)
    {
        return Validator::make($data, [
            'current_password' => ['required'],
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);
    }

    public function branchs()
    {
        return view('account.branch');
    }

    public function orders()
    {
        return view('account.orders');
    }

    public function ordersDetails($number_order)
    {
        $installments = Pedido::getInstallmentsOrder($number_order);
        $order_items = Pedido::getOrderItems($number_order);
        $invoice_order = Pedido::getInvoiceOrder($number_order);
        return view('account.orders-details', compact(['number_order', 'installments', 'order_items', 'invoice_order']));
    }

    public function ordersList(Request $request)
    {
        $user = User::where(['id' => Auth::id()])->first();
        parse_str($request->input('search'), $search);
        if($user->hasRole('customer')) {
            $orders = Pedido::getOrdersClient($user->codcli, $search);
            $page = $request->input('page');
            $perPage = 10;
            $offset = $perPage * ($page - 1);

            $orders = new LengthAwarePaginator(array_slice($orders, $offset, $perPage, true), count($orders), $perPage, $page, ['path' => $request->url(), 'query' => $request->query()]);
            $html = view('account.orders-list')->with(['orders' => $orders])->render();

            return response()
                ->json(['html' => $html, 'pagination' => (string)$orders->links('vendor.pagination.orders-list-link')], 200);
        }
        else if($user->hasRole('seller')) {
            $orders = Pedido::getOrdersSeller($user->codusur, $search);
            $page = $request->input('page');
            $perPage = 10;
            $offset = $perPage * ($page - 1);

            $orders = new LengthAwarePaginator(array_slice($orders, $offset, $perPage, true), count($orders), $perPage, $page, ['path' => $request->url(), 'query' => $request->query()]);
            $html = view('account.orders-list')->with(['orders' => $orders])->render();

            return response()
                ->json(['html' => $html, 'pagination' => (string)$orders->links('vendor.pagination.orders-list-link')], 200);
        }
    }

    public function ordersInstallments(Request $request, $number_order)
    {
        try {

            $installments = Pedido::getInstallmentsOrder($number_order);
            if(empty($installments)) {
                return response()->json(['html' => false, 'msg' => 'Ainda não há boletos gerados para esse pedido, tente mais tarde.'], 200);
            }

            $html = view('account.installment-list')->with(['installments' => $installments])->render();

            return response()
                ->json(['html' => $html], 200);
        }
        catch(Exception $e) {
            report($e);
            return response()->json(['msg' => 'Não foi possível carregar as parcelas do pedido, tente mais tarde!'], 401);
        }
    }

    public function portfolio()
    {
        $user = User::where(['id' => Auth::id()])->first();
        if($user->hasRole('customer')) {
            return redirect()
                ->route('branchs');
        }
        else {
            return view('account.portfolio');
        }
    }

    public function portfolioChange(Request $request)
    {
        try {
            $portfolio = $request->input('portfolio');
            $client = Client::where(['codcli' => $portfolio])->first();
            $data = [
                'codplpag' => $client->codplpag,
                'codcli' => $client->codcli
            ];
            $cart_id = Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->first()->id;
            if(Cart::where(['id' => $cart_id])->update($data)) {
                return response()->json(['success' => true], 200);
            }
        }
        catch(Exception $e) {
            report($e);
            return response()->json(['msg' => 'Ocorreu um problema para selecionar outro cliente, tente novamente mais tarde!'], 401);
        }
    }

    public function portfolioList(Request $request)
    {
        try {
            parse_str($request->input('search'), $search);
            $user = User::where(['id' => Auth::id()])->first();
            $portfolios = ProfileUser::portfolio($user->codusur, $search);
            $page = $request->input('page');
            $perPage = 10;
            $offset = ($page * $perPage) - $perPage;

            $portfolios = new LengthAwarePaginator(array_slice($portfolios, $offset, $perPage, true), count($portfolios), $perPage, $page, ['path' => $request->url(), 'query' => $request->query()]);
            $html = view('account.portfolio-list')->with(['portfolios' => $portfolios])->render();

            return response()
                ->json(['html' => $html, 'pagination' => (string)$portfolios->links('vendor.pagination.portfolio-list-link')], 200);
        }
        catch(Exception $e) {
            report($e);
            return response()->json(['msg' => 'Não foi possível carregar a sua carteira de clientes, contate o administrador'], 401);
        }
    }
}

