$(document).ready(() => {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })

    $('form#header_search_form').on('submit', (e) => {
        e.preventDefault()
        let input = $('form#header_search_form input[name="primary_search"]')
        if(input.val().length > 0) {
            let keyword = input.val()
            input.val('')
            document.cookie = 'search_action='+keyword+'; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/'
            window.location.href = '/produtos/az'
        }
    })

    $('li.menu-item.carrinho form').on('submit', (e) => {
        e.preventDefault()
    })

    $('table.tabela-produtos.carrinho').on('click', (e) => {
        if($(e.target).hasClass('minus')) {
            let old_qtd = $(e.target).parent().find('input.qtd').val()
            if(parseInt(old_qtd) > 0) {
                $(e.target).parent().find('input.qtd').val(parseInt(old_qtd) - 1)
                if($(e.target).parent().find('input.qtd').val() === 0) {
                    let input = $(e.target).parent().find('input.qtd')
                    input.val(0)
                    Main.cart_header(input)
                    return
                }
                Main.cart_header($(e.target).parent().find('input.qtd'))
            }
            else {
                let input = $(e.target).parent().find('input.qtd')
                input.val(0)
                Main.cart_header(input)
            }
        }

        if($(e.target).hasClass('plus')) {
            let old_qtd = $(e.target).parent().find('input.qtd').val()
            $(e.target).parent().find('input.qtd').val(parseInt(old_qtd) + 1)
            Main.cart_header($(e.target).parent().find('input.qtd'))
        }

        if($(e.target).hasClass('del')) {
            let input = $(e.target).parent().parent().find('input.qtd')
            input.val(0)
            Main.cart_header(input)
        }
    })

    $('li.oferta-relampago form#oferta').on('click', (e) => {
        if($(e.target).hasClass('minus')) {
            let old_qtd = $(e.target).parent().find('input.qtd').val()
            if(parseInt(old_qtd) > 0) {
                $(e.target).parent().find('input.qtd').val(parseInt(old_qtd) - 1)
                return
            }
            else {
                return
            }
        }

        if($(e.target).hasClass('plus')) {
            let old_qtd = $(e.target).parent().find('input.qtd').val()
            $(e.target).parent().find('input.qtd').val(parseInt(old_qtd) + 1)
        }

    })

    $('form#oferta').on('submit', (e) => {
        e.preventDefault()
        if(parseInt($(e.target).parent().find('div.qtd input.qtd').val()) > 0) {
            Main.cart_header($(e.target).parent().find('div.qtd input.qtd'), false)
            $(e.target).parent().find('div.qtd input.qtd').val(0)
        }
    })

    $('li.produto-item.vitr form#prod-vtr').on('click', (e) => {
        if($(e.target).hasClass('minus')) {
            let old_qtd = $(e.target).parent().find('input.qtd').val()
            if(parseInt(old_qtd) > 0) {
                $(e.target).parent().find('input.qtd').val(parseInt(old_qtd) - 1)
                return
            }
            else {
                return
            }
        }

        if($(e.target).hasClass('plus')) {
            let old_qtd = $(e.target).parent().find('input.qtd').val()
            $(e.target).parent().find('input.qtd').val(parseInt(old_qtd) + 1)
        }

    })

    $('form#prod-vtr').on('submit', (e) => {
        e.preventDefault()
        if(parseInt($(e.target).parent().find('div.qtd input.qtd').val()) > 0) {
            Main.cart_header($(e.target).parent().find('div.qtd input.qtd'), false)
            $(e.target).parent().find('div.qtd input.qtd').val(0)
        }
    })

    $('li.produto-item.vitrr form#prod-vtrr').on('click', (e) => {
        if($(e.target).hasClass('minus')) {
            let old_qtd = $(e.target).parent().find('input.qtd').val()
            if(parseInt(old_qtd) > 0) {
                $(e.target).parent().find('input.qtd').val(parseInt(old_qtd) - 1)
                return
            }
            else {
                return
            }
        }

        if($(e.target).hasClass('plus')) {
            let old_qtd = $(e.target).parent().find('input.qtd').val()
            $(e.target).parent().find('input.qtd').val(parseInt(old_qtd) + 1)
        }

    })

    $('form#prod-vtrr').on('submit', (e) => {
        e.preventDefault()
        if(parseInt($(e.target).parent().find('div.qtd input.qtd').val()) > 0) {
            Main.cart_header($(e.target).parent().find('div.qtd input.qtd'), false)
            $(e.target).parent().find('div.qtd input.qtd').val(0)
        }
    })

    $('li.menu-item.prazo select#prazo-topo').on('change', (e) => {
        $.ajax({
            url: '/prazo/change',
            method: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'plan': $(e.target).val()
            },
            success: (res) => {
                let r = res
                $('li.menu-item.prazo span.texto strong').text(r.res[0].numdias)
                $('li.menu-item.prazo ul li span.prazo-medio span.pp-description').text(r.res[0].desc_plan)
                $('li.menu-item.prazo ul li span.prazo-medio span.pp-numdias').text(r.res[0].numdias)
                $('li.menu-item.prazo ul li span.valor-minimo span.pp-min').text(r.res[0].vlminpedido)
                location.reload()
            },
            error: (err) => {
                console.log(err)
            }
        })
    })

    // solicitação de alteração da filial cliente
    $('li.menu-item select#filial-topo').on('change', (e) => {
        $.ajax({
            url: '/branchs/change',
            method: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Accept': 'application/json'
            },
            data: {
                codcli: $(e.target).val()
            },
            success: (res) => {
                Swal.fire({
                    icon: 'success',
                    title: 'Sucesso!',
                    text: res.msg,
                    showCancelButton: false,
                    showConfirmButton: true
                })
            },
            error: (err) => {
                Swal.fire({
                    icon: 'error',
                    title: 'Atenção!',
                    text: err.responseJSON.msg,
                    showCancelButton: true,
                    showConfirmButton: false
                })
            }
        })
    })

    // $('a#cliente-topo').on('click', (e) => {
    //     $.ajax({
    //         url: '/portfolio/change',
    //         method: 'post',
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
    //             'Accept': 'application/json'
    //         },
    //         data: {
    //             portfolio: $(e.target).attr('data-id')
    //         },
    //         success: () => {
    //             location.reload()
    //         },
    //         error: (err) => {
    //             Swal.fire({
    //                 icon: 'error',
    //                 title: 'Atenção!',
    //                 text: err.responseJson.msg,
    //                 showCancelButton: true,
    //                 showConfirmButton: false
    //             })
    //         }
    //     })
    // })

    if($('div#banner-principal').length > 0) {
        $('div#banner-principal').on('click', (e) => {
            $.ajax({
                url: '/banners/action',
                method: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'Accept': 'application/json'
                },
                data: {
                    id: $(e.target).attr('data-id')
                },
                success: (res) => {
                    if(res.banner.call_to_action == 'S') {
                        document.cookie = 'search_action='+res.banner.filtro+'; expires=Fri, 31 Dec 9999 23:59:59 GMT'
                        window.location.href = '/produtos/az'
                    }
                    else {
                        window.location.href = '/produtos/az'
                    }
                },
                error: (err) => {
                    Swal.fire({
                        icon: 'error',
                        title: 'Atenção!',
                        text: err.responseJSON.msg,
                        showConfirmButton: false,
                        showCancelButton: true
                    })
                }
            })

        })
    }

    if($('div.fullbanner').length > 0) {
        $('div.fullbanner a').on('click', (e) => {

            document.cookie = 'search_action=GIOVANNA BABY; expires=Fri, 31 Dec 9999 23:59:59 GMT'
            window.location.href = '/produtos/az'

            // $.ajax({
            //     url: '/banners/action',
            //     method: 'post',
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            //         'Accept': 'application/json'
            //     },
            //     data: {
            //         id: $(e.target).attr('data-id')
            //     },
            //     success: (res) => {
            //         if(res.banner.call_to_action == 'S') {
            //             document.cookie = 'search_action=; expires=Fri, 31 Dec 9999 23:59:59 GMT'
            //             window.location.href = '/produtos/az'
            //         }
            //         else {
            //             window.location.href = '/produtos/az'
            //         }
            //     },
            //     error: (err) => {
            //         Swal.fire({
            //             icon: 'error',
            //             title: 'Atenção!',
            //             text: err.responseJSON.msg,
            //             showConfirmButton: false,
            //             showCancelButton: true
            //         })
            //     }
            // })

        })
    }

    $('form#form-newsletter input[name="email_newsletter"]').on('keypress', (e) => {
        if(e.keyCode === 13) {
            e.preventDefault()
            if($(e.target).val().length > 0) {
                Main.send_newsletter($(e.target).val())
                return
            }
            Swal.fire({
                icon: 'error',
                title: 'Atenção!',
                text: 'Você precisa informar um e-mail.',
                showConfirmButton: true,
                showCancelButton: false
            })
        }
    })

    $('form#form-newsletter').on('submit', (e) => {
        e.preventDefault()
        if($('input[name="email_newsletter"]').val().length > 0) {
            Main.send_newsletter($('input[name="email_newsletter"]').val());
            return
        }
        Swal.fire({
            icon: 'error',
            title: 'Atenção!',
            text: 'Você precisa informar um e-mail.',
            showConfirmButton: true,
            showCancelButton: false
        })
    })

})

const Main = {
    cart_header: (e, up = true) => {
        $.ajax({
            url: '/cart/store',
            method: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                id: $(e).attr('data-id'),
                prm: $(e).attr('data-prm'),
                q: $(e).val()
            },
            success: (res) => {
                if(up) {
                    if (parseInt($(e).val()) === 0) {
                        $(e).parent().parent().parent().remove()
                    }
                    $('table.tabela-produtos.carrinho tbody').remove()
                    $('table.tabela-produtos.carrinho').append(res.cart_items_html)
                    $(e).parent().parent().parent().find('td').eq(-2).find('span.preco').text('').text(res.product_price)
                    $('table.tabela-produtos.carrinho').find('tfoot tr td span.preco strong').text('').text(' ' + res.subtotal)
                    $('table.tabela-produtos.carrinho').find('tfoot tr td span.qt').text('').text(' ' + res.qt)
                    $('.mobile-menu-control.ferramentas-topo li.carrinho span.icon-carrinho-1 span.alert').text('').text(' '+res.qt)
                    $('li.carrinho a.cart-info span.icon-carrinho-1 span.alert').text('').text(' ' + res.qt)
                    $('li.carrinho a.cart-info span.texto span.preco strong').text('').text(' ' + res.subtotal)
                    $('table.tabela-produtos.carrinho tbody input.qtd').on('change', (e) => {
                        Main.cart_header($(e.target))
                    })
                    if (window.location.pathname == '/produtos/az') {
                        Products.list(window.location.href, 1, true, $('form.search').serialize())
                    }
                    else if(window.location.pathname == '/carrinho') {
                        Cart.list(1, true)
                    }
                }
                else {
                    $('table.tabela-produtos.carrinho tbody').remove()
                    $('table.tabela-produtos.carrinho').append(res.cart_items_html)
                    $('table.tabela-produtos.carrinho').find('tfoot tr td span.preco strong').text('').text(' ' + res.subtotal)
                    $('table.tabela-produtos.carrinho').find('tfoot tr td span.qt').text('').text(' ' + res.qt)
                    $('.mobile-menu-control.ferramentas-topo li.carrinho span.icon-carrinho-1 span.alert').text('').text(' '+res.qt)
                    $('li.carrinho a.cart-info span.icon-carrinho-1 span.alert').text('').text(' ' + res.qt)
                    $('li.carrinho a.cart-info span.texto span.preco strong').text('').text(' ' + res.subtotal)
                    $('table.tabela-produtos.carrinho tbody input.qtd').on('change', (e) => {
                        Main.cart_header($(e.target))
                    })
                }
            },
            error: (err) => {
                return false
            }
        })
    },
    send_newsletter: (email) => {
        $.ajax({
            url: '/newsletter/store',
            method: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Accept': 'application/json'
            },
            data: {
                email: email
            },
            success: (res) => {
                Swal.fire({
                    icon: 'success',
                    title: 'Obrigado!',
                    text: res.msg,
                    showConfirmButton: true,
                    showCancelButton: false
                })
                $('input[name="email_newsletter"]').val('')
            },
            error: (err) => {

                if(err.responseJSON.msg !== undefined) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Atenção!',
                        text: err.responseJSON.msg,
                        showConfirmButton: false,
                        showCancelButton: true
                    })
                }

                if(err.responseJSON.errors !== undefined) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Atenção!',
                        text: err.responseJSON.errors.email,
                        showConfirmButton: false,
                        showCancelButton: true
                    })
                }

            }
        })
    }
}