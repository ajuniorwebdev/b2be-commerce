@extends('layouts.app')

@section('content')
    <div class="page-container">
        <div class="wrapper tiny">
            <div class="login-boxes">
                <div class="login-box">
                    <div class="login">
                        <h1>Primeiro Acesso</h1>
                        <form method="post" action="{{ route('register') }}">
                            @csrf
                            <ol class="col-group">
                                <li class="col-1 radio-group">
                                    <input type="radio" name="profile" id="client_profile" value="1" checked>
                                    <label for="client_profile">Cliente</label>
                                    <input type="radio" name="profile" id="seller_profile" value="2">
                                    <label for="seller_profile">Representante</label>
                                </li>
                                <div class="client_form">
                                    <li class="col-1 @error('cnpj') erro @enderror">
                                        <label for="cnpj">CNPJ</label>
                                        <input type="text" id="cnpj" name="cnpj" placeholder="Informe seu CNPJ" autocomplete="off" value="{{ old('cnpj') }}">
                                        @error('cnpj')
                                            <span class="msg-erro">{{ $message }}</span>
                                        @enderror
                                    </li>
                                    {{-- <li class="col-1 @error('client_code') erro @enderror">
                                        <label for="client_code">Código</label>
                                    <input type="text" id="client_code" name="client_code" placeholder="Informe seu código de cliente" autocomplete="off" value="{{ old('client_code') }}">
                                        @error('client_code')
                                            <span class="msg-erro">{{ $message }}</span>
                                        @enderror
                                    </li> --}}
                                </div>
                                <div class="seller_form" style="display:none">
                                    <li class="col-1 @error('seller_code') erro @enderror">
                                        <label for="seller_code">Código</label>
                                        <input type="text" id="seller_code" name="seller_code" placeholder="Informe seu código de representante" value="{{ old('seller_code') }}">
                                        @error('seller_code')
                                            <span class="msg-erro">{{ $message }}</span>
                                        @enderror
                                    </li>
                                </div>
                                <li class="col-1 @error('email') erro @enderror">
                                    <label for="email">E-mail</label>
                                    <input type="text" id="email" name="email" placeholder="Informe seu e-mail" value="{{ old('email') }}">
                                    @error('email')
                                        <span class="msg-erro">{{ $message }}</span>
                                    @enderror
                                </li>
                                <li class="col-1 @error('password') erro @enderror">
                                    <label for="password">Nova senha</label>
                                    <input type="password" id="password" name="password" placeholder="Cadastre uma nova senha" autocomplete="off">
                                    @error('password')
                                        <span class="msg-erro">{{ $message }}</span>
                                    @enderror
                                </li>
                                <li class="col-1">
                                    <label for="password-confirm">Confirmar nova senha</label>
                                    <input type="password" id="password-confirm" name="password_confirmation" placeholder="Confirme sua nova senha" autocomplete="off">
                                </li>
                                <li class="col-1">
                                    <button class="bt-principal alignright" type="submit">Enviar</button>
                                </li>
                            </ol>
                        </form>
                    </div>
                </div>
                <div class="login-box">
                    <div class="login">
                        <h3>Já possui o acesso?</h3>
                        <p>você já tenha o acesso ao e-commerce <strong>Exfar Distribuidora</strong>? Então basta entrar em nosso sistema</p>
                        <a href="{{ route('login') }}" class="bt-sec">Entrar no Sistema</a>
                    </div>
                    <div class="solicite-acesso">
                        <h3>Solicite o Cadastro</h3>
                        <p>Ainda não é nosso cliente? Preencha a ficha de cadastro e aguarde nosso contato.</p>
                        <a href="{{ route('client.request.register') }}" class="bt-sec">Solicitar Cadastro</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{mix('js/login.js')}}"></script>
    @if(!is_null(old('profile')))
        @if(old('profile') == '1')
            <script>
                $(document).ready(() => {
                    $('input[name="profile"]#client_profile').prop('checked', true).change()
                    $('input[name="cnpj"]').val(`{{old('cnpj')}}`)
                })
            </script>
        @else
            <script>
                $(document).ready(() => {
                    $('input[name="profile"]#seller_profile').prop('checked', true).change()
                    $('input[name="seller_code"]').val(`{{old('seller_code')}}`)
                })
            </script>
        @endif
    @endif

    @error('failed')
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Atenção!',
                text: '{{ $message }}',
            })
        </script>
    @enderror
@endsection
