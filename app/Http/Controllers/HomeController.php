<?php

namespace App\Http\Controllers;

use App\Cart;
use Exception;
use App\Banner;
use App\Destaque;
use App\Newsletter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $banners = Banner::with(['status', 'bannerImg', 'bannerTipo'])
            ->where(['status_id' => 1, 'banner_hierarchy' => 1])
            ->orderBy('position', 'asc')
            ->get();
//        $banners = [];

        $banners_sec = Banner::with(['status', 'bannerImg', 'bannerTipo'])
            ->where(['status_id' => 1, 'banner_hierarchy' => 2])
            ->orderBy('position', 'asc')
            ->get();
//        $banners_sec = [];

        $codplpag = false;
        if(Auth::check()) {
            $codplpag = Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->first()->codplpag;
        }

        $destaques_vitrine = Destaque::destaque($codplpag);
//        $destaques_vitrine = [];
        $destaques_randon = Destaque::destaque_randon($codplpag);
//        $destaques_randon = [];

        return view('home', compact(['banners', 'banners_sec', 'destaques_vitrine', 'destaques_randon']));
    }

    public function bannersAction(Request $request)
    {
        try{
            $banner = Banner::with(['status', 'bannerImg', 'bannerTipo'])
                ->where(['status_id' => 1, 'banner_hierarchy' => 1, 'id' => $request->input('id')])
                ->orderBy('position', 'asc')
                ->first();
            return response()->json(compact('banner'), 200);
        }
        catch(Exception $e) {
            report($e);
            return response()->json(['msg' => 'Não possível executar essa ação, tente novamente mais tarde.'], 401);
        }
    }

    public function newsletterStore(Request $request)
    {
        $this->validationNewsletter($request)->validate();
        try {

            if(Newsletter::where(['email' => $request->input('email')])->exists()) {
                Newsletter::where(['email' => $request->input('email')])
                    ->update([
                        'updated_at' => now()
                    ]);
                return response()->json(['msg' => 'Seu e-mail já foi cadastrado, em breve você receberá nossas ofertas especiais.'], 200);
            }

            $data = [
                'email' => $request->input('email'),
                'created_at' => now(),
                'update_at' => null,
                'deleted_at' => null
            ];

            if(Newsletter::create($data)) {
                return response()->json(['msg' => 'Seu e-mail já foi cadastrado, em breve você receberá nossas ofertas especiais.'], 200);
            }

        }
        catch(Exception $e) {
            report($e);
            return response()->json(['msg' => 'Não foi possível cadastrar seu e-mail, tente novamente mais tarde!'], 401);
        }
    }

    protected function validationNewsletter(Request $request)
    {
        return Validator::make($request->all(), [
            'email' => 'required|email'
        ]);
    }
}
