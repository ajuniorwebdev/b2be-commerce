<?php

namespace App;

use App\Status;
use App\BannerImg;
use App\BannerTipo;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = [
        'company_branch_codigo', 'description', 'img_banner_id', 'status_id', 'tipo_banner_id', 'position', 'call_to_action', 'filtro', 'created_at', 'updated_at'
    ];

    protected $table = 'banners';

    public function status() {
        return $this->belongsTo('App\Status', 'status_id', 'id');
    }

    public function bannerImg() {
        return $this->belongsTo('App\BannerImg', 'img_banner_id', 'id');
    }

    public function bannerTipo() {
        return $this->belongsTo('App\BannerTipo', 'tipo_banner_id', 'id');
    }
}
