<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Cart extends Model
{
    protected $fillable = ['user_id','codplpag','codcli','items_qt','subtotal','dt_created','dt_updated','dt_deleted','dt_sended'];
    protected $table = 'cart';

    public static function getCartItems()
    {
        $cart_id = Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->first()->id;
        $sql = "
            select
                pr.codauxiliar,
                pr.codprod,
                pr.descricao,
                ci.qt,
                ci.subtotal
            from cart_items ci 
            inner join product pr on ci.product_id = pr.codprod
            where ci.cart_id = {$cart_id}
        ";
        return DB::select($sql);
    }

    public static function getSubtotalCart()
    {
        $cart_id = Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->first()->id;
        $sql = "select ifnull(sum(subtotal),0.00) as subtotal from cart_items where cart_id = {$cart_id}";
        return DB::select($sql);
    }

    public static function getQuantityItemsCart()
    {
        $cart_id = Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->first()->id;
        $sql = "select ifnull(sum(qt), 0) as qt from cart_items where cart_id = {$cart_id}";
        return DB::select($sql);
    }

}
