@extends('layouts.app')

@section('content')
    <div class="page-container ">
        <div class="wrapper tiny">
            <div class="page-title">
                <h1>Solicite o Cadastro</h1>
            </div>
            <div class="page-content">
                <p>Ainda não é nosso cliente? Solicite a ficha de cadastro para acessar o e-commerce Exfar Distribuidora</p>
                <div class="page-content-form">
                    <form id="client_request" action="{{ route('client.request.send') }}" method="post" enctype="multipart/form-data">
                        @csrf

                        @error('failed')
                            <span class="msg-erro big">Por favor, verifique os campos destacados abaixo:</span>
                        @enderror

                        <fieldset id="dados-cliente">
                            <legend>Dados do Cliente</legend>
                            <ol class="col-group">
                                <li class="col-2 @error('cnpj') erro @enderror">
                                    <label for="cnpj" style="color: #d82f0feb">CPNJ *</label>
                                    <input type="text" name="cnpj" id="cnpj" value="{{ old('cnpj') }}" placeholder="00.000.000/0000-00" autocomplete="off">
{{--                                    <input type="text" name="cnpj" id="cnpj" value="99.999.999/9999-99" placeholder="00.000.000/0000-00" autocomplete="off">--}}
                                    @error('cnpj')
                                        <span class="msg-erro">{{ $message }}</span>
                                    @enderror
                                </li>
                                <li class="col-2 @error('inscricao_estadual') erro @enderror">
                                    <label for="inscricao_estadual" style="color: #d82f0feb">Inscrição Estadual *</label>
                                    <input type="number" name="inscricao_estadual" id="inscricao_estadual" value="{{ old('inscricao_estadual') }}" placeholder="0" autocomplete="off">
{{--                                    <input type="number" name="inscricao_estadual" id="inscricao_estadual" value="999999" placeholder="0" autocomplete="off">--}}
                                    @error('inscricao_estadual')
                                        <span class="msg-erro">{{ $message }}</span>
                                    @enderror
                                </li>
                                <li class="col-1">
                                    <label for="razao_social" >Razão Social</label>
                                    <input type="text" name="razao_social" id="razao_social" value="{{ old('razao_social') }}" placeholder="Insira a Razão Social da sua farmácia" autocomplete="off">
                                </li>
                                <li class="col-2">
                                    <label for="nome_fantasia" >Nome Fantasia</label>
                                    <input type="text" name="nome_fantasia" id="nome_fantasia" value="{{ old('nome_fantasia') }}" placeholder="Insira o Nome Fantasia da sua farmácia" autocomplete="off">
                                </li>
                                <li class="col-2">
                                    <label for="nome_rede" >Nome da Rede</label>
                                    <input type="text" name="nome_rede" id="nome_rede" value="{{ old('nome_rede') }}" placeholder="Insira o nome da Rede da sua farmácia" autocomplete="off">
                                </li>
                                <li class="col-1 @error('email_contato') erro @enderror">
                                    <label for="email_contato" style="color: #d82f0feb">E-mail Contato *</label>
                                    <input type="email" name="email_contato" id="email_contato" value="{{ old('email_contato') }}" placeholder="email@email.com" autocomplete="off">
{{--                                    <input type="email" name="email_contato" id="email_contato" value="email@email.com" placeholder="email@email.com" autocomplete="off">--}}
                                    @error('email_contato')
                                        <span class="msg-erro">{{ $message }}</span>
                                    @enderror
                                </li>
                                <li class="col-2">
                                    <label for="email_cobranca" >E-mail Cobrança</label>
                                    <input type="email" name="email_cobranca" id="email_cobranca" value="{{ old('email_cobranca') }}" placeholder="email@email.com" autocomplete="off">
                                </li>
                                <li class="col-2">
                                    <label for="email_xml" >E-mail XML</label>
                                    <input type="email" name="email_xml" id="email_xml" value="{{ old('email_xml') }}" placeholder="email@email.com" autocomplete="off">
                                </li>
                                <li class="col-2 @error('telefone_celular') erro @enderror">
                                    <label for="telefone_celular" style="color: #d82f0feb">Celular *</label>
                                    <input type="text" name="telefone_celular" id="telefone_celular" value="{{ old('telefone_celular') }}" placeholder="(00) 00000-0000" autocomplete="off">
{{--                                    <input type="text" name="telefone_celular" id="telefone_celular" value="(14) 99999-9999" placeholder="(00) 00000-0000" autocomplete="off">--}}
                                    @error('telefone_celular')
                                        <span class="msg-erro">{{ $message }}</span>
                                    @enderror
                                </li>
                                <li class="col-2">
                                    <label for="telefone_fixo">Telefone</label>
                                    <input type="text" name="telefone_fixo" id="telefone_fixo" value="{{ old('telefone_fixo') }}" placeholder="(00) 0000-0000" autocomplete="off">
                                </li>
                            </ol>
                        </fieldset>
                        <fieldset id="dados_proprietarios">
                            <legend>Dados do Proprietário (Opcional)</legend>
                            <div class="dp_1">
                                <h5>Proprietário</h5>
                                <ol class="col-group">
                                    <li class="col-1">
                                        <label for="nome_proprietario">Nome do Proprietário</label>
                                        <input type="text" name="nome_proprietario" id="nome_proprietario" value="{{ old('nome_proprietario') }}" placeholder="Insira o Nome Completo do Proprietário da farmácia" autocomplete="off">
                                    </li>
                                    <li class="col-3">
                                        <label for="cpf_proprietario" >CPF Proprietário</label>
                                        <input type="text" name="cpf_proprietario" id="cpf_proprietario" value="{{ old('cpf_proprietario') }}" placeholder="000.000.000-00" autocomplete="off">
                                    </li>
                                    <li class="col-3">
                                        <label for="email_proprietario" >E-mail</label>
                                        <input type="email" name="email_proprietario" id="email_proprietario" value="{{ old('email_proprietario') }}" placeholder="email@email.com" autocomplete="off">
                                    </li>
                                    <li class="col-3">
                                        <label for="telefone_proprietario">Celular</label>
                                        <input type="text" name="telefone_proprietario" id="telefone_proprietario" value="{{ old('telefone_proprietario') }}" placeholder="(00) 00000-0000" autocomplete="off">
                                    </li>
{{--                                    <li class="col-1">--}}
{{--                                        <a href="javascript:void(0)" class="add_dp"><small>Adicionar mais um proprietario</small></a> &nbsp;&nbsp; <a href="javascript:void(0)" class="remove_dp vermelho disabled"><small>Remover este proprietário</small></a>--}}
{{--                                    </li>--}}
                                </ol>
                            </div>
                        </fieldset>
                        <fieldset id="dados_compradores">
                            <legend>Dados dos Compradores (Opcional)</legend>
                            <h5>Medicamentos</h5>
                            <ol class="col-group">
                                <li class="col-1">
                                    <label for="nome_comprador_medicamentos">Nome do Comprador de Medicamentos</label>
                                    <input type="text" name="nome_comprador_medicamentos" id="nome_comprador_medicamentos" value="{{ old('nome_comprador_medicamentos') }}" placeholder="Insira o Nome Completo do Comprador de Medicamentos" autocomplete="off">
                                </li>
                                <li class="col-3">
                                    <label for="email_comprador_medicamentos" >E-mail</label>
                                    <input type="email" name="email_comprador_medicamentos" id="email_comprador_medicamentos" value="{{ old('email_comprador_medicamentos') }}" placeholder="email@email.com" autocomplete="off">
                                </li>
                                <li class="col-3">
                                    <label for="telefone_fixo_comprador_medicamentos" >Telefone Fixo</label>
                                    <input type="text" name="telefone_fixo_comprador_medicamentos" id="telefone_fixo_comprador_medicamentos" value="{{ old('telefone_fixo_comprador_medicamentos') }}" placeholder="(00) 0000-0000" autocomplete="off">
                                </li>
                                <li class="col-3">
                                    <label for="telefone_celular_comprador_medicamentos" >Telefone Celular</label>
                                    <input type="text" name="telefone_celular_comprador_medicamentos" id="telefone_celular_comprador_medicamentos" value="{{ old('telefone_celular_comprador_medicamentos') }}" placeholder="(00)00000-0000" autocomplete="off">
                                </li>
                            </ol>
                            <h5>Perfumaria</h5>
                            <ol class="col-group">
                                <li class="col-1">
                                    <label for="nome_comprador_perfumaria">Nome do Comprador de Perfumaria</label>
                                    <input type="text" name="nome_comprador_perfumaria" id="nome_comprador_perfumaria" value="{{ old('nome_comprador_perfumaria') }}" placeholder="Insira o Nome Completo do Comprador de Perfumaria" autocomplete="off">
                                </li>
                                <li class="col-3">
                                    <label for="email_comprador_perfumaria" >E-mail</label>
                                    <input type="email" name="email_comprador_perfumaria" id="email_comprador_perfumaria" value="{{ old('email_comprador_perfumaria') }}" placeholder="email@email.com" autocomplete="off">
                                </li>
                                <li class="col-3">
                                    <label for="telefone_fixo_comprador_perfumaria" >Telefone Fixo</label>
                                    <input type="text" name="telefone_fixo_comprador_perfumaria" id="telefone_fixo_comprador_perfumaria" value="{{ old('telefone_fixo_comprador_perfumaria') }}" placeholder="(00) 0000-0000" autocomplete="off">
                                </li>
                                <li class="col-3">
                                    <label for="telefone_celular_comprador_perfumaria" >Telefone Celular</label>
                                    <input type="text" name="telefone_celular_comprador_perfumaria" id="telefone_celular_comprador_perfumaria" value="{{ old('telefone_celular_comprador_perfumaria') }}" placeholder="(00) 00000-0000" autocomplete="off">
                                </li>
                            </ol>
                        </fieldset>
                        <fieldset id="dados_limite_sugerido">
                            <legend>Condição Comercial (Opcional)</legend>
                            <ol class="col-group">
                                <li class="col-2">
                                    <label for="limite_sugerido">Limite Sugerido</label>
                                    <input type="text" name="limite_sugerido" id="limite_sugerido" value="{{ old('limite_sugerido') }}" maxlength="13" placeholder="R$ 0.000,00" autocomplete="off">
                                </li>
                            </ol>
                        </fieldset>
                        <fieldset id="enviar-documentos">
                            <legend>Documentos (Opcional)</legend>
                            <span class="msg-erro big">Atenção: Aceitaremos apenas arquivos com boa qualidade e legibilidade, estes serão impressos e arquivos na distribuidora.</span>
                            <p><strong>Medicamento:</strong> Contrato Social, Sintegra ou Cartão CPNJ, Autorização De Funcionamento Da Empresa (AFE), Autorização Especial/Controlados (AE), Certificado De Regularidade Tecnica (CRT) E Alvara Sanitario.</p>
                            <p><strong>Perfumaria e Outros:</strong> Contrato Social, Sintegra ou Cartão CNPJ, Autorização de Funcionamento da Empresa (AFE) e Alvará Sanitario.</p>
                            <ol class="col-group">
                                <li class="col-1">
                                    <label for="limite-sugerido">Envie os arquivos</label>
                                    <div id="dragdrop-area" class="dragdrop-area dm-uploader">
                                        Arraste seus arquivos para cá
                                        <br>ou<br>
                                        <div class="btn bt-sec">
                                            <span>Selecione os arquivos</span>
                                            <input type="file" title="Click to add Files" multiple>
                                        </div>
                                        <small>Formatos aceitos: Pdf, Jpg, Png</small>
                                    </div>
                                </li>
                            </ol>
                            <ul class="list-unstyled" id="files" style="display: none">
                                <li class="text-muted text-center empty">Nenhum arquivo importado.</li>
                            </ul>
                            <div style="display:none;" id="documents">
                                <input type="text" name="documents">
                            </div>
                        </fieldset>

                        <fieldset>
                            <ol class="col-group">
                                <li class="col-4-5">
                                    <small style="color: #d82f0feb">* Campos Obrigatórios</small>
                                </li>
                                <li class="col-5">
                                    <!-- O Botão foi alterado por um link simples apenas para simular a navegação do sistema -->
                                    <!-- <button type="submit" name="enviar" class="bt-principal alignright">Enviar</button> -->
                                    <button type="submit" class="bt-principal alignright">Enviar</button>
                                </li>
                            </ol>
                        </fieldset>
                    </form>
                </div>
                <div class="login-boxes">
                    <div class="login-box">
                        <div class="login">
                            <h3>Já possui o acesso?</h3>
                            <p>você já tenha o acesso ao e-commerce <strong>Exfar Distribuidora</strong>? Então basta entrar em nosso sistema</p>
                            <a href="{{ route('login') }}" class="bt-sec">Entrar no Sistema</a>
                        </div>
                    </div>
                    <div class="login-box">
                        <div class="primeiro-acesso">
                            <h3>Primeiro Acesso</h3>
                            <p>Você é nosso cliente mas ainda não Ainda não tem acesso ao e-commerce <strong>Exfar Distribuidora</strong>?</p>
                            <a href="{{ route('register') }}" class="bt-sec">Primeiro Acesso</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('js')
    <script src="{{mix('js/client.request.js')}}"></script>
    <script type="text/html" id="files-template">
        <li class="media">
            <div class="media-body mb-1">
                <p class="mb-2">
                    <strong>%%filename%%</strong> - Status: <span class="text-muted">Waiting</span>
                </p>
                <div class="progress mb-2">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary"
                         role="progressbar"
                         style="width: 0%"
                         aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                    </div>
                </div>
            </div>
        </li>
    </script>
    @if(session('error'))
        <script>
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Opss!!',
                text: '{{session('error')['msg']}}',
                showConfirmButton: true
            })
        </script>
    @endif

    @error('cnpj')
        @if(old('cnpj') != '')
            <script>
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Atenção!!',
                    text: 'Você já fez sua solicitação, aguarde até obter um retorno ou entre em contato para saber em qual etapa esta o seu cadastro.',
                    showConfirmButton: true
                })
            </script>
        @endif
    @enderror
@endsection