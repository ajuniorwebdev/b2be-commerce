<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientBranchRequest extends Model
{
    protected $fillable = [
        'codcli', 'cgcent', 'codcliprinc', 'action_sign', 'status', 'created_at', 'updated_at'
    ];

    protected $table = 'client_branch_request';
}
