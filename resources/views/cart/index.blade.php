@extends('layouts.app')

@section('content')
    <div class="produtos-content table-view">
        <h1 class="center">Carrinho de Compras</h1>

        <div class="wrappper-cart items">
            <div class="table-wrapper">
                <form id="order" action="{{ route('order.store') }}" method="post">
                    @csrf
                    <table id="cart-items">
                        <thead>
                        <tr>
                            <th>Marca</th>
                            <th>EAN</th>
                            <th>Produto</th>
                            <th class="center">Preço</th>
                            <th class="center hidden-mobile">Imposto Unit.</th>
                            <th class="center hidden-mobile">Desconto</th>
                            <th class="center hidden-mobile">Preço c/ Desc.</th>
                            <th class="center ">Preço c/ Desc. + Impostos</th>
                            <th class="center" width="100">Qtd.</th>
                            <th class="center ">Subtotal</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="13" style="text-align: center;">
                                <div class="loader" style="text-align: center; height: 100px; line-height: 140px;">
                                    <svg width="48" height="48" viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg" version="1.1">
                                        <path d="M 150,0 a 150,150 0 0,1 106.066,256.066 l -35.355,-35.355 a -100,-100 0 0,0 -70.711,-170.711 z" fill="#0071bb">
                                            <animateTransform attributeName="transform" attributeType="XML" type="rotate" from="0 150 150" to="360 150 150" begin="0s" dur=".5s" fill="freeze" repeatCount="indefinite"></animateTransform>
                                        </path>
                                    </svg>
                                </div>
                                <h4>Aguarde um instante...</h4>
                            </td>
                        </tr>
                        </tbody>

                    </table>
                </form>
            </div>
        </div>
        <div class="produtos-tools wrappper-cart afterclear">
            <hr>
            <a href="{{route('/')}}">← Continuar comprando</a>
            <a href="javascript:void(0)" onclick="$('form#order').submit()" class="bt-principal alignright">Finalizar Compra</a>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{mix('js/cart.js')}}"></script>
    @if(session('error'))
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Opss!',
                text: "{{ session('error')['msg'] }}",
                showConfirmButton: true
            })
        </script>
    @enderror
@endsection
