<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerTipo extends Model
{
    protected $fillable = [
        'descricao', 'created_at', 'updated_at'
    ];

    protected $table = 'banners_tipos';
}
