<?php
/**
 * Custom Post Type
 *
 * @author
 */

class CustomPostType {
    /**
     * Identificador do custom post type
     * @var string
     */
    public $id;

    /**
     * Nome do custom post type
     * @var string
     */
    private $name;

    /**
     * Nome, no plural, do custom post type
     * @var string
     */
    private $plural_name = null;

    /**
     * Opções padrões para criação do custom post type
     * @var array
     */
    private $opts = array(
        'public'        => true,
        'has_archive'   => true,
        'supports'      => array('title', 'editor', 'thumbnail'),
        'labels'        => array()
    );

    private $meta_boxes = array();
    private $columns    = array();
    private $taxonomies = array();
    private $pages      = array();

    private $placeholder_title = null;


    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->setName($name);
        $this->setSlug($id);

        // Coluna padrões
        $this->columns = array(
            'title' => new Column('title', 'Título')
        );
    }

    /**
     * Modificar alguma opção do custom post type
     * @param string $name Nome da opção
     * @param mixed $value Valor da opção
     */
    public function option($name, $value)
    {
        $this->opts[$name] = $value;
        return $this;
    }

    /**
     * Modificar alguma label do custom post type
     * @param string $name Nome da label
     * @param mixed $value Valor da label
     */
    public function label($name, $value)
    {
        $this->opts['labels'][$name] = $value;
        return $this;
    }

    /**
     * Adicionar suporte ao custom post type
     * @param array|string $value
     * @return void
     */
    public function addSupport($value)
    {
        if (is_array($value))
        {
            $this->opts['supports'] += $value;
        }
        else
        {
            $this->opts['supports'][] = $value;
        }
        return $this;
    }

    /**
     * Remover suporte em um custom post type
     * @param array|string $value
     * @return void
     */
    public function removeSupport($value)
    {
        $array = (is_string($value) ? array($value) : $value);

        foreach ($array as $item)
        {
            $key = array_search($item, $this->opts['supports']);
            if ($key)
            {
                unset($this->opts['supports'][$key]);
            }
        }

        return $this;
    }

    /**
     * Modificar slug do custom post type
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->option('rewrite', array('slug' => $slug));
        return $this;
    }

    /**
     * Modificar nome do custom post type
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;

        if (is_null($this->plural_name))
        {
            $this->plural_name = $this->name . 's';
        }
        return $this;
    }

    /**
     * Modificar o placeholder da input de título
     */
    public function setPlaceholderTitle($value)
    {
        $this->placeholder_title = $value;
        return $this;
    }

    /**
     * Modificar nome em plural do custom post type
     * @param string $name
     */
    public function setPluralName($plural_name)
    {
        $this->plural_name = $plural_name;
        return $this;
    }

    public function addMetaBox($id, $title, $context, $priority)
    {
        $metabox = new MetaBox($id, $title, $context, $priority);
        $this->meta_boxes[] = $metabox;

        return $metabox;
    }

    public function add($is_page = false)
    {
        add_theme_support( 'post-thumbnails' );

        if (!$is_page) {
            add_action('init', array(&$this, 'register'));
        }

        add_action('add_meta_boxes', array(&$this, 'addMetaBoxes'));

        // if (current_user_can('edit_post'))
        // {
            add_action('save_post', array(&$this, 'saveMetaBoxes'));
        // }

        if (count($this->columns) > 0)
        {
            add_filter('manage_edit-' . $this->id . '_columns', array(&$this, 'addColumns'));
            add_action('manage_' . $this->id . '_posts_custom_column', array(&$this, 'showColumns'));
        }

        if (!is_null($this->placeholder_title))
        {
            add_filter( 'enter_title_here', array(&$this, 'changePlaceholderTitle') );
        }

        foreach ($this->pages as $page)
        {
            $page->add();
        }
    }

    public function changePlaceholderTitle($input)
    {
        global $post_type;

        if ( is_admin() && $post_type == $this->id )
            return $this->placeholder_title;

        return $input;
    }

    public function addMetaBoxes()
    {
        foreach ($this->meta_boxes as $meta_box)
        {
            $meta_box->add($this->id);
        }
    }

    public function saveMetaBoxes($post_id)
    {
        foreach ($this->meta_boxes as $meta_box)
        {
            $meta_box->save($post_id);
        }
    }

    public function register()
    {
        // Labels padrões
        $default_labels = array(
            'name'          => $this->plural_name,
            'singular_name' => $this->name,
            'add_new'       => 'Adicionar ' . $this->name,
            'add_new_item'  => 'Adicionar ' . $this->name,
            'new_item'      => 'Adicionar ' . $this->name,
            'edit_item'     => 'Editar ' . $this->name,
            'search_items'  => 'Procurar ' . $this->name
        );

        $this->opts['labels'] = array_merge($default_labels, $this->opts['labels']);

        register_post_type($this->id, $this->opts);

        // Registrar taxonomies
        $this->registerTaxonomies();
    }

    public function addParent(CustomPostType $post_type_parent, MetaBox $metabox, $multiple = false)
    {
        if ($post_type_parent !== $this)
        {
            // Salvar função anterior antes de reescreve-la
            $oldOnLoad = $metabox->onLoad;

            $metabox->onLoad = function($value, $id, $post) use ($post_type_parent, $metabox, $oldOnLoad, $multiple)
            {
                $parents = get_posts(
                    array(
                        'post_type'   => $post_type_parent->id,
                        'orderby'     => 'date',
                        'order'       => 'DESC',
                        'numberposts' => -1
                    )
                );

                if (!empty($parents))
                {
                    // Enable multiple selection
                    $selected = function($parent_id, $value, $multiple) {
                        if ($multiple) {
                            $selected = in_array($parent_id, $value) ? ' selected' : '';
                        } else {
                            $selected = $parent_id == $value ? ' selected' : '';
                        }

                        return $selected;
                    };

                    if ($multiple == true) {
                        echo '<select data-placeholder="Clique para selecionar os itens" multiple name="' . $id . '[]" class="select" style="width: 100%">';
                    } else {
                        echo '<select data-placeholder="Clique para selecionar um item" name="' . $id. '" class="select" style="width: 100%">';
                    }

                    foreach ($parents as $parent)
                    {
                        $value_select = null;

                        if (is_null($oldOnLoad))
                            $value_select = $parent->post_title;
                        else
                            $value_select = call_user_func($oldOnLoad, $value, $id, $parent);

                        $value = get_post_meta($post->ID, $id, true);

                        echo '<option value="' . $parent->ID . '"' . $selected($parent->ID, $value, $multiple) . '>' . $value_select . '</option>';
                    }

                    echo '</select>';
                }
                else
                {
                    echo 'Nada para exibir aqui!';
                }
            };
        }
    }

    public function addColumns()
    {
        $array_columns = array();

        foreach ($this->columns as $column)
        {
            $array_columns[$column->id] = $column->title;
        }

        return $array_columns;
    }

    public function showColumns($id)
    {
        if (isset($this->columns[$id]))
        {
            global $post;

            $this->columns[$id]->load($post);
        }
    }

    public function addColumn($id, $title)
    {
        if (!isset($this->columns[$id]))
        {
            $new_column = new Column($id, $title);
            $this->columns[$id] = $new_column;

            return $new_column;
        }
    }

    public function removeColumn($column)
    {
        if (isset($this->columns[$column]))
        {
            unset($this->columns[$column]);
        }
    }

    public function addTaxonomy($id, $title)
    {
        if (!isset($this->taxonomies[$id]))
        {
            $new_taxonomy = new Taxonomy($id, $title);
            $this->taxonomies[$id] = $new_taxonomy;

            return $new_taxonomy;
        }
    }

    public function removeTaxonomy($id)
    {
        if (isset($this->taxonomies[$id]))
        {
            unset($this->taxonomies[$id]);
        }
    }

    private function registerTaxonomies()
    {
        foreach ($this->taxonomies as $taxonomy)
        {
            $taxonomy->register($this->id);
        }
    }

    public function addPageSettings($id, $name)
    {
        $this->pages[$id] = new PageSetting($id, $name, 'edit.php?post_type=' . $this->id);
        return $this->pages[$id];
    }
}
