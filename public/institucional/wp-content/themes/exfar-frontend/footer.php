    <?php       
        $general = get_option('general');
        $phone2 = $general['phone2'];
        $logradouro = $general['logradouro'];
        $complemento = $general['complemento'];
        $email = $general['email'];
        $link = $general['linkSegundaVia'];
    ?>
    
    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="footer-section col-md-3">
                    <div class="footer-section-logo"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/logo-exfar-white.png" alt="Logo"></div>
                </div>
                <div class="footer-section col-md-4">
                    <div class="footer-section-header">Contato</div>    
                    <p><?php echo $logradouro; ?></p>
                    <p><?php echo $complemento; ?></p>
                    <p><?php echo $phone2; ?></p>
                    <p><?php echo $email; ?></p>
                </div>
                <div class="footer-section col-md-4">
                    <div class="footer-section-header">Mapa do site</div>
                    <p><a href="<?php echo home_url('/');?>">Home</a></p>
                    <p><a href="<?php echo home_url('/sobre');?>">Sobre</a></p>
                    <p><a href="<?php echo home_url('/contato');?>">Contato</a></p>
                    <p><a href="<?php echo $link;?>" target="_blank">2ª Via Boleto</a></p>
                </div>
                <div class="footer-section col-md-1">
                </div>
            </div>
            <div class="row subfooter">
                <p>Todos os direitos reservados a EXFAR LTDA.</p>
                <a href="http://www.harpiadev.com.br" target="_blank">
                    <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/logo-footer-white.png" alt="Harpia Desenvolvimento">
                </a>
            </div>
        </div>
    </footer>

    <!-- <script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script> -->

    <?php wp_footer(); ?>

    <!-- New Section -->
    <!-- <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
    <script src="node_modules/swiper/dist/js/swiper.min.js"></script>
    <script src="assets/js/scripts.js"></script> -->
</body>

</html>