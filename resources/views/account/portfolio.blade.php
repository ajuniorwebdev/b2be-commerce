@extends('layouts.app')

@section('content')
    <div class="area-cliente-container">
        <div class="lateral">
            <ul>
                <li class="area-cliente"><a href="{{ route('overview') }}">Visão Geral</a></li>
                <li class="minha-conta"><span class="icon-cliente-1"></span><a href="{{ route('account') }}">Minha Conta</a></li>
                <li class="minhas-filiais active"><span class="icon-filiais-1"></span><a href="@role('customer') {{ route('branchs') }} @elserole('seller') {{ route('portfolio') }} @endrole">@role('customer') Minhas Filiais @elserole('seller') Carteira de Clientes @endrole</a></li>
                <li class="meus-pedidos"><span class="icon-pedidos-1"></span><a href="{{ route('orders') }}">Meus Pedidos</a></li>
                {{-- <li class="chat-representante"><span class="icon-chat-1"></span><a href="chat-representante.html">Fale com o seu representante</a></li>--}}
            </ul>
        </div>
        <div class="content">
            <div class="wrapper fullscreen" style="margin: 0; max-width: 100%">
                <ul class="mobile-only area-cliente-atalhos">
                    <li class="minha-conta aos-init aos-animate" data-aos="fade-up" data-aos-delay="0"><a href="{{ route('account') }}"><span class="icon-cliente-1"></span>Minha Conta</a></li>
                    <li class="minhas-filiais aos-init aos-animate" data-aos="fade-up" data-aos-delay="100"><a href="@role('customer') {{ route('branchs') }} @elserole('seller') {{ route('portfolio') }} @endrole"><span class="icon-filiais-1"></span>@role('customer') Minhas Filiais @elserole('seller') Carteira de Clientes @endrole</a></li>
                    <li class="meus-pedidos aos-init aos-animate" data-aos="fade-up" data-aos-delay="200"><a href="{{ route('orders') }}"><span class="icon-pedidos-1"></span>Pedidos de Clientes</a></li>
                    {{--<li class="chat-representante aos-init aos-animate" data-aos="fade-up" data-aos-delay="300"><a href="representante_chat-clientes.html"><span class="icon-chat-1"></span>Fale com os seus Clientes</a></li>--}}
                </ul>

                <div class="page-title">
                    <h1>Carteira de Clientes</h1>
                </div>
                <div class="produtos-tools afterclear" style="max-width: 100%">
                    <div class="busca-form">
                        <form class="search">
                            <input type="text" name="primary_search" id="busca-topo" placeholder="Pesquise por Cód Cliente, Razão Social, Nome Fantasia, Nome da Rede...">
                            <button type="submit" class="bt-busca icon-busca"><span>Pesquisar</span></button>
{{--                            <span class="toggle-busca-avancada icon-config"></span>--}}
{{--                            <div class="busca-avancada">--}}
{{--                                <h5>Busca Avançada</h5>--}}
{{--                                <ol class="col-group">--}}
{{--                                    <li class="col-4">--}}
{{--                                        <label for="cod-cliente">Cód. Cliente</label>--}}
{{--                                        <input type="text" name="cod-cliente">--}}
{{--                                    </li>--}}
{{--                                    <li class="col-4">--}}
{{--                                        <label for="razao-social">Razão Sociai</label>--}}
{{--                                        <input type="text" name="razao-social">--}}
{{--                                    </li>--}}
{{--                                    <li class="col-4">--}}
{{--                                        <label for="nome-fantasia">Nome Fantasia</label>--}}
{{--                                        <input type="text" name="nome-fantasia">--}}
{{--                                    </li>--}}
{{--                                    <li class="col-4">--}}
{{--                                        <label for="nome-rede">Nome da Rede</label>--}}
{{--                                        <input type="text" name="nome-rede">--}}
{{--                                    </li>--}}

{{--                                    <li class="col-1">--}}
{{--                                        <a href="#" class="toggle">Fechar</a>--}}
{{--                                        <button type="submit" class="bt-principal alignright">Pesquisar</button>--}}
{{--                                    </li>--}}
{{--                                </ol>--}}
{{--                            </div>--}}
                        </form>
                    </div>
                </div>
                <div class="page-content">
                    <div class="pedidos afterclear">
                        <div class="table-wrapper">
                            <table id="portfolio">
                                <thead>
                                <tr>
                                    <th width="150">CNPJ</th>
                                    <th>Nome Fantasia</th>
                                    <th>Bairro</th>
                                    <th>Cidade</th>
                                    <th>Lim. Crédito</th>
                                    <th>Qtd Titulos Aberto</th>
                                    <th>Vr Total Aberto</th>
                                    <th>Qtd Titulos Venc.</th>
                                    <th>Vr Total Venc.</th>
                                    <th>Dt Alv. Func.</th>
                                    <th>Dt Alv. Sanit.</th>
                                    <th>Dt Alv. Func. AFE</th>
                                    <th>Dt Alv. CRF</th>
                                    <th class="center">Ação</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="15" style="text-align: center;">
                                        <div class="loader" style="text-align: center; height: 100px; line-height: 140px;">
                                            <svg width="48" height="48" viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg" version="1.1">
                                                <path d="M 150,0 a 150,150 0 0,1 106.066,256.066 l -35.355,-35.355 a -100,-100 0 0,0 -70.711,-170.711 z" fill="#0071bb">
                                                    <animateTransform attributeName="transform" attributeType="XML" type="rotate" from="0 150 150" to="360 150 150" begin="0s" dur=".5s" fill="freeze" repeatCount="indefinite"></animateTransform>
                                                </path>
                                            </svg>
                                        </div>
                                        <h4>Aguarde um instante...</h4>
                                    </td>
                                </tr>
                                </tbody>

{{--                                <tr>--}}
{{--                                    <td><a href="representante_carteira-clientes-view.html">00000</a></td>--}}
{{--                                    <td><a href="representante_carteira-clientes-view.html">Lorem ipsum dolor sit amet</a></td>--}}
{{--                                    <td>DrogaMed </td>--}}
{{--                                    <td></td>--}}
{{--                                    <td class="center">3</td>--}}
{{--                                </tr>--}}

                            </table>
                            <hr>
                            <div class="pagination-container">
                                <ul class="pagination alignright">
                                    <li class="prev disabled"><a href="#">&lt;</a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li class="next"><a href="#">&gt;</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if($config['fullbanner_account'])
                <div class="fullbanner">
                    <a href="#"><img src="{{ asset('/images/banners/'.$config['banner_fix']->bannerImg->img) }}"/></a>
                </div>
            @endif

        </div>
    </div>
@endsection

@section('js')
    <script src="{{ mix('/js/account.js') }}"></script>
@endsection
