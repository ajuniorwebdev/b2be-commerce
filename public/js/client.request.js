$(document).ready(() => {
    // mask field's
    $('input[name="cnpj"]').mask('00.000.000/0000-00')
    $('input[name="inscricao_estadual"]').mask('000000000000')
    $('input[name="telefone_fixo"]').mask('(00) 0000-0000')
    $('input[name="telefone_celular"]').mask('(00) 00000-0000')
    $('input[name="cpf_proprietario"]').mask('000.000.000-00')
    $('input[name="telefone_proprietario"]').mask('(00) 00000-0000')
    $('input[name="telefone_fixo_comprador_medicamentos"]').mask('(00) 0000-0000')
    $('input[name="telefone_celular_comprador_medicamentos"]').mask('(00) 00000-0000')
    $('input[name="telefone_fixo_comprador_perfumaria"]').mask('(00) 0000-0000')
    $('input[name="telefone_celular_comprador_perfumaria"]').mask('(00) 00000-0000')
    $('input[name="limite_sugerido"]').mask('#.##0,00', {reverse: true})

    $('div#dragdrop-area').dmUploader({
        url: '/client/request/files',
        maxFileSize: 3000000,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        extFilter: ["jpg", "pdf", "png", "jpeg"],
        extraData: function() {
            return {
                'cnpj': $('input[name="cnpj"]').val()
            }
        },
        onDragEnter: function(){
            // Happens when dragging something over the DnD area
            this.addClass('active');
        },
        onDragLeave: function(){
            // Happens when dragging something OUT of the DnD area
            this.removeClass('active');
        },
        onInit: function(){
            // Plugin is ready to use
            console.log('plugin de upload iniciado')
        },
        onComplete: function() {
            // All files in the queue are processed (success or error)
            console.log('All pending tranfers finished')
        },
        onNewFile: function(id, file){
            if(!$('input#cnpj').val()) {
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Preencha o formulário primeiro e depois insira seus documentos.',
                    showConfirmButton: true
                })
                return false;
            }
            // When a new file is added using the file selector or the DnD area
            $('ul.list-unstyled').css({display: 'block'})
            ClientRequest.ui_multi_add_file(id, file)
            $('input[name="documents"]').val('1')
        },
        onBeforeUpload: function(id){
            // about tho start uploading a file
            ClientRequest.ui_multi_update_file_progress(id, 0, '', true);
            ClientRequest.ui_multi_update_file_status(id, 'uploading', 'Uploading...');
        },
        onUploadProgress: function(id, percent){
            // Updating file progress
            ClientRequest.ui_multi_update_file_progress(id, percent);
        },
        onUploadSuccess: function(id, data){
            // A file was successfully uploaded
            ClientRequest.ui_multi_update_file_status(id, 'success', 'Upload Complete');
            ClientRequest.ui_multi_update_file_progress(id, 100, 'success', false);
        },
        onUploadError: function(id, xhr, status, message){
            // Happens when an upload error happens
            ClientRequest.ui_multi_update_file_status(id, 'danger', message);
            ClientRequest.ui_multi_update_file_progress(id, 0, 'danger', false);
        },
        onFallbackMode: function(){
            // When the browser doesn't support this plugin :(
        },
        onFileSizeError: function(file){
            console.log('File \'' + file.name + '\' cannot be added: size excess limit', 'danger');
        },
        onFileTypeError: function(file){
            // ui_single_update_status(this, 'File type is not an image', 'danger');
            console.log('File \'' + file.name + '\' cannot be added: must be an image (type error)', 'danger');
        },
        onFileExtError: function(file){
            // ui_single_update_status(this, 'File extension not allowed', 'danger');
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Opss!',
                text: 'Extensão de arquivo não permitida, salve seu arquivo na extensão correta e tente novamente.',
                showCancelButton: true,
                showConfirmButton: false,
            })
            //console.log('File \'' + file.name + '\' cannot be added: must be an image (extension error)', 'danger');
        }
    })

    $('form#client_request').on('keypress', (e) => {
        if(e.keyCode == 13) {
            e.preventDefault();
        }
    })
})

let ClientRequest = {
    ui_multi_add_file: (id, file) => {
        let template = $('#files-template').text()
        template = template.replace('%%filename%%', file.name)

        template = $(template)
        template.prop('id', 'uploaderFile' + id)
        template.data('file-id', id)

        $('#files').find('li.empty').fadeOut() // remove the 'no files yet'
        $('#files').prepend(template)
    },

    // Changes the status messages on our list
    ui_multi_update_file_status: (id, status, message) => {
        $('#uploaderFile' + id).find('span').html(message).prop('class', 'status text-' + status)
    },

    ui_multi_update_file_progress: (id, percent, color, active) => {
        color = (typeof color === 'undefined' ? false : color)
        active = (typeof active === 'undefined' ? true : active)

        let bar = $('#uploaderFile' + id).find('div.progress-bar')

        bar.width(percent + '%').attr('aria-valuenow', percent)
        bar.toggleClass('progress-bar-striped progress-bar-animated', active)

        if (percent === 0){
            bar.html('')
        } else {
            bar.html(percent + '%')
        }

        if (color !== false){
            bar.removeClass('bg-success bg-info bg-warning bg-danger')
            bar.addClass('bg-' + color)
        }
    },

    // Toggles the disabled status of Star/Cancel buttons on one particual file
    ui_multi_update_file_controls: (id, start, cancel, wasError) => {
        wasError = (typeof wasError === 'undefined' ? false : wasError)

        $('#uploaderFile' + id).find('button.start').prop('disabled', !start)
        $('#uploaderFile' + id).find('button.cancel').prop('disabled', !cancel)

        if (!start && !cancel) {
            $('#uploaderFile' + id).find('.controls').fadeOut()
        } else {
            $('#uploaderFile' + id).find('.controls').fadeIn()
        }

        if (wasError) {
            $('#uploaderFile' + id).find('button.start').html('Retry')
        }
    }
}