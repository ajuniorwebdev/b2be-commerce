<?php

namespace App\Mail;

use App\ClientRequest;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RequestAcess extends Mailable
{
    use Queueable, SerializesModels;

    protected $request_access;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ClientRequest $request_access)
    {
        $this->request_access = $request_access;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $client = ClientRequest::where('id', $this->request_access->id)->first();

        return $this->subject('Solicitação de Cadastro E-Commerce')
            ->view('vendor.mail.request.request-access')
            ->with([
                'cnpj' => $client->cnpj,
                'ie' => $client->ie,
                'razao_social' => $client->razao_social,
                'fantasia' => $client->nome_fantasia,
                'rede' => $client->nome_rede,
                'email_contato' => $client->email_contato,
                'email_cobranca' => $client->email_cobranca,
                'email_xml' => $client->email_xml,
                'telefone_fixo' => $client->telefone_fixo,
                'telefone_celular' => $client->telefone_celular,
                'nome' => $client->nome_proprietario,
                'cpf' => $client->cpf_proprietario,
                'email' => $client->email_proprietario,
                'celular' => $client->telefone_proprietario,
                'comprador_med_nome' => $client->nome_comprador_medicamentos,
                'comprador_med_email' => $client->email_comprador_medicamentos,
                'comprador_med_tel_fixo' => $client->telefone_fixo_comprador_medicamentos,
                'comprador_med_tel_celular' => $client->telefone_celular_comprador_medicamentos,
                'comprador_per_nome' => $client->nome_comprador_perfumaria,
                'comprador_per_email' => $client->email_comprador_perfumaria,
                'comprador_per_tel_fixo' => $client->telefone_fixo_comprador_perfumaria,
                'comprador_per_tel_celular' => $client->telefone_celular_comprador_perfumaria,
                'condicao_comercial' => str_replace(',', '.',str_replace('.','',$client->limite_sugerido))
            ]);
    }
}
