<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class PaymentPlan extends Model
{
    protected $table = 'payment_plan';

    public static function average()
    {
        $codcli = Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->first()->codcli;
        $sql = "
            select 
                codplpag,
                descricao,
                numdias,
                vlminpedido,
                status
            from payment_plan
            where tipoprazo = 'N'
              and (numdias >= 0 and numdias <= (
                select
                    pp.numdias
                from client cl
                inner join payment_plan pp on cl.codplpag = pp.codplpag
                where codcli = {$codcli}
              ))
            order by numdias asc
        ";
        return DB::select($sql);
    }

    public static function averageSelected($plan = false)
    {
        $cart = Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->first();
        $plan = ($plan) ? $plan : $cart->codplpag;

        $sql = "
            select 
                codplpag,
                descricao,
                numdias,
                vlminpedido,
                status,
                case
                    when prazo12 > 0 then concat(prazo1, '/', prazo2, '/', prazo3, '/', prazo4, '/', prazo5, '/', prazo6, '/', prazo7, '/', prazo8, '/', prazo9, '/', prazo10, '/', prazo11, '/', prazo12)
                    when prazo11 > 0 then concat(prazo1, '/', prazo2, '/', prazo3, '/', prazo4, '/', prazo5, '/', prazo6, '/', prazo7, '/', prazo8, '/', prazo9, '/', prazo10, '/', prazo11)
                    when prazo10 > 0 then concat(prazo1, '/', prazo2, '/', prazo3, '/', prazo4, '/', prazo5, '/', prazo6, '/', prazo7, '/', prazo8, '/', prazo9, '/', prazo10)
                    when prazo9 > 0 then concat(prazo1, '/', prazo2, '/', prazo3, '/', prazo4, '/', prazo5, '/', prazo6, '/', prazo7, '/', prazo8, '/', prazo9)
                    when prazo8 > 0 then concat(prazo1, '/', prazo2, '/', prazo3, '/', prazo4, '/', prazo5, '/', prazo6, '/', prazo7, '/', prazo8)
                    when prazo7 > 0 then concat(prazo1, '/', prazo2, '/', prazo3, '/', prazo4, '/', prazo5, '/', prazo6, '/', prazo7)
                    when prazo6 > 0 then concat(prazo1, '/', prazo2, '/', prazo3, '/', prazo4, '/', prazo5, '/', prazo6)
                    when prazo5 > 0 then concat(prazo1, '/', prazo2, '/', prazo3, '/', prazo4, '/', prazo5)
                    when prazo4 > 0 then concat(prazo1, '/', prazo2, '/', prazo3, '/', prazo4)
                    when prazo3 > 0 then concat(prazo1, '/', prazo2, '/', prazo3)
                    when prazo2 > 0 then concat(prazo1, '/', prazo2)
                    when prazo1 > 0 then prazo1
                    else prazo1
                end as desc_plan
            from payment_plan
            where tipoprazo = 'N'
              and (numdias >= 0 and numdias <= (
                select
                    pp.numdias
                from client cl
                inner join payment_plan pp on cl.codplpag = pp.codplpag
                where codcli = {$cart->codcli}
              ))
              and codplpag = {$plan}
            order by numdias asc
        ";
        return DB::select($sql);
    }
}
