<?php

namespace App;

use App\Product;
use App\Discount;
use App\DestaqueTipo;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Destaque extends Model
{
    protected $fillable = [
        'codpromocaomed', 'codprod', 'codfilial', 'numregiao', 'img', 'status_id', 'tipo_destaque_id', 'created_at', 'updated_at'
    ];

    protected $table = 'destaques';

    public function destaque_tipo()
    {
        return $this->belongsTo('App\DestaqueTipo', 'tipo_destaque_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product', 'codprod', 'codprod');
    }

    public function discount()
    {
        return $this->hasMany('App\Discount', ['codpromocaomed', 'codprod', 'codfilial'], ['codpromocaomed', 'codprod', 'codfilial']);
    }

    public static function oferta()
    {
        $sql = "
            select * from (
                select
                    bra.codmarca,
                    bra.marca descricao_marca,
                    pro.codprod,
                    pro.descricao,
                    tax.perdescrepasse impostos,
                    pri.pvenda,
                    ((pri.pvenda - (pri.pvenda * (dis.percdesc/100))) + ((pri.pvenda - (pri.pvenda * (dis.percdesc/100))) * (tax.perdescrepasse/100))) price,
                    dis.codpromocaomed,
                    dis.percdesc,
                    dest.img
                from product pro
                inner join brand bra on pro.codmarca = bra.codmarca
                inner join stock stc on pro.codprod = stc.codprod and stc.codfilial = 2
                inner join price pri on pro.codprod = pri.codprod and pri.numregiao = 1000
                left join tax on pri.codst = tax.codst
                left join (
                    select des.codprod, des.codmarca, des.codpromocaomed, max(ifnull(des.percdesc,0)) percdesc
                    from discount des
                    where des.tipopoliticapromocaomed = 'D'
                    and des.tipofv = 'PE'
                    group by des.codprod, des.codmarca, des.codpromocaomed
                ) dis on pro.codprod = dis.codprod or pro.codmarca = dis.codmarca
                inner join destaques dest on dis.codprod = dest.codprod and dis.codpromocaomed = dest.codpromocaomed and tipo_destaque_id = 3
                where stc.qtest > 0
                and dest.status_id = 1
                group by
                    bra.codmarca,
                    bra.marca,
                    pro.codprod,
                    pro.codauxiliar,
                    tax.perdescrepasse,
                    stc.qtest,
                    pri.pvenda,
                    dis.codpromocaomed,
                    dis.percdesc,
                    dest.img
            ) destaque_oferta;
        ";

        $results = DB::select($sql);
        return $results;
    }

    public static function destaque($codplpag)
    {
        $sql = "
            select
                bra.codmarca,
                bra.marca descricao_marca,
                pro.codprod,
                concat(pro.descricao, ' - ', bra.marca) descricao,
                pro.codauxiliar,
                tax.perdescrepasse impostos,
                stc.qtest,
                pri.pvenda,
                ((pri.pvenda - (pri.pvenda * (dis.percdesc/100))) + ((pri.pvenda - (pri.pvenda * (dis.percdesc/100))) * (tax.perdescrepasse/100))) price,
                dis.codpromocaomed,
                dis.percdesc,
                case
                   when stc.qtest < 50 then 'C'
                   when stc.qtest >= 50 and stc.qtest < 150 then 'M'
                   when stc.qtest >= 150 then 'E'
                end stock_volume,
                dest.img
            from product pro
            inner join brand bra on pro.codmarca = bra.codmarca
            inner join stock stc on pro.codprod = stc.codprod
            inner join price pri on pro.codprod = pri.codprod
            left join tax on pri.codst = tax.codst
            left join (
                select des.codprod, des.codmarca, des.codpromocaomed, max(ifnull(des.percdesc,0)) percdesc
                from discount des
                where des.tipopoliticapromocaomed = 'D'
                  and des.tipofv = 'PE'
                group by des.codprod, des.codmarca, des.codpromocaomed
            ) dis on pro.codprod = dis.codprod or pro.codmarca = dis.codmarca
            inner join destaques dest on dis.codprod = dest.codprod and dis.codpromocaomed = dest.codpromocaomed
            where stc.qtest > 0
              and stc.codfilial = 2
              and pri.numregiao = 1000
              and dest.status_id = 1
              and tipo_destaque_id = 1
            group by
              bra.codmarca,
              bra.marca,
              pro.codprod,
              pro.codauxiliar,
              tax.perdescrepasse,
              stc.qtest,
              pri.pvenda,
              dis.codpromocaomed,
              dis.percdesc,
              dest.img;
        ";

        return DB::select($sql);
    }

    public static function destaque_randon($codplpag)
    {
        $sql = "
        select * from (
            select
                bra.codmarca,
                bra.marca descricao_marca,
                pro.codprod,
                concat(pro.descricao, ' - ', bra.marca) descricao,
                pro.codauxiliar,
                tax.perdescrepasse impostos,
                stc.qtest,
                pri.pvenda,
                ((pri.pvenda - (pri.pvenda * (dis.percdesc/100))) + ((pri.pvenda - (pri.pvenda * (dis.percdesc/100))) * (tax.perdescrepasse/100))) price,
                dis.codpromocaomed,
                dis.percdesc,
                case
                   when stc.qtest < 50 then 'C'
                   when stc.qtest >= 50 and stc.qtest < 150 then 'M'
                   when stc.qtest >= 150 then 'E'
                end stock_volume,
                dest.img
            from product pro
            inner join brand bra on pro.codmarca = bra.codmarca
            inner join stock stc on pro.codprod = stc.codprod
            inner join price pri on pro.codprod = pri.codprod
            left join tax on pri.codst = tax.codst
            left join (
                select des.codprod, des.codmarca, des.codpromocaomed, max(ifnull(des.percdesc,0)) percdesc
                from discount des
                where des.tipopoliticapromocaomed = 'D'
                  and des.tipofv = 'PE'
                group by des.codprod, des.codmarca, des.codpromocaomed
            ) dis on pro.codprod = dis.codprod or pro.codmarca = dis.codmarca
            inner join destaques dest on dis.codprod = dest.codprod and dis.codpromocaomed = dest.codpromocaomed
            where stc.qtest > 0
              and stc.codfilial = 2
              and pri.numregiao = 1000
              and dest.status_id = 1
              and tipo_destaque_id = 2
            group by
                bra.codmarca,
                bra.marca,
                pro.codprod,
                pro.codauxiliar,
                tax.perdescrepasse,
                stc.qtest,
                pri.pvenda,
                dis.codpromocaomed,
                dis.percdesc,
                dest.img
        ) destaque_random;
        ";

        return DB::select($sql);
    }
}
