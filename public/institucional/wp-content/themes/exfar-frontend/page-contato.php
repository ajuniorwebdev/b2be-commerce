<?php get_header(); 
    $general = get_option('general');
    $mapa = $general['mapa'];
    $phone = $general['phone1'];

?>

<main id="contact">
    <section class="contact-header">
        <div class="contact-header-shadow">
            <h2>CONTATO</h2>
        </div>
    </section>
    <section class="contact-form">
        <div class="container">
            <div class="contact-form-headers animaleft">
                <h4>FALE CONOSCO</h4>
                <p>Número de Televendas: <span><b> <?php echo $phone ?></b></span></p>
            </div>

            <div class="contact-form-content">
                <div class="row">
                    <div class="col-md-5 animaleft">
                        <?php echo do_shortcode('[contact-form-7 id="12" title="Contato"]');?>
                    </div>
                    <div class="col-md-6 offset-md-1 animaright">
                        <iframe id="map"
                            src="<?php echo $mapa; ?>"
                            width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>