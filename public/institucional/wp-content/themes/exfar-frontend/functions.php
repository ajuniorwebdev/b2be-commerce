<?php
include 'framework/start.php';

// add scripts in page
function my_scripts() {
	// deregistering old jquery
	wp_deregister_script('jquery');

	// styles
	// wp_register_style('fontawesome', get_template_directory_uri() . '/node_modules/font-awesome/css/font-awesome.min.css');
    // wp_enqueue_style('fontawesome' );

	wp_register_style('bootstrap', get_template_directory_uri() . '/node_modules/bootstrap/dist/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap' );

    wp_register_style('swiper', get_template_directory_uri() . '/node_modules/swiper/dist/css/swiper.min.css');
    wp_enqueue_style('swiper' );

    wp_register_style('style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('style' );

	// chamar o jquery
	wp_register_script('jquery', get_stylesheet_directory_uri() . '/node_modules/jquery/dist/jquery.min.js', null, null, true);
	wp_enqueue_script('jquery');
    
     // popper
	wp_register_script('popper.js', get_stylesheet_directory_uri() . '/node_modules/popper.js/dist/umd/popper.min.js', null, null, true);
	wp_enqueue_script('popper.js');

    // bootstrap
	wp_register_script('bootstrap.js', get_stylesheet_directory_uri() . '/node_modules/bootstrap/dist/js/bootstrap.min.js', null, null, true);
	wp_enqueue_script('bootstrap.js');

	// chamar swiper
	wp_register_script('swiper.js', get_stylesheet_directory_uri() . '/node_modules/swiper/dist/js/swiper.min.js', null, null, true);
	wp_enqueue_script('swiper.js');

	// chamar mask
	wp_register_script('mask.js', get_stylesheet_directory_uri() . '/node_modules/jquery-mask-plugin/dist/jquery.mask.min.js', null, null, true);
	wp_enqueue_script('mask.js');

	// chamar mask
	wp_register_script('scrollreveal.js', get_stylesheet_directory_uri() . '/assets/js/scrollreveal.min.js', null, null, true);
	wp_enqueue_script('scrollreveal.js');

	// chamar o script
	wp_register_script('script', get_stylesheet_directory_uri() . '/assets/js/scripts.js', null, null, true);
	wp_enqueue_script('script');

	



	// Define a variável ajaxurl
    $script  = '<script>';
    $script .= 'var ajaxurl = "' . admin_url('admin-ajax.php') . '";';
    $script .= '</script>';
    echo $script;

}



add_action( 'wp_enqueue_scripts', 'my_scripts' );
add_filter('show_admin_bar', '__return_false');
add_theme_support('post-thumbnails');


function post_banner () {

	$labels = array(
	'name' => 'Banners',
	'name_singular' => 'Banner',
	'add_new_item' => 'Adicionar novo banner'
	);

	$supports = array(
		'title',
		'thumbnail'
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'menu_icon' => 'dashicons-media-code',
		'supports' => $supports
	);

	register_post_type('banner', $args);

}


function post_fornecedor () {

	$labels = array(
	'name' => 'Fornecedores',
	'name_singular' => 'Fornecedor',
	'add_new_item' => 'Adicionar novo fornecedor'
	);

	$supports = array(
		'title',
		'thumbnail'
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'menu_icon' => 'dashicons-media-code',
		'supports' => $supports
	);

	register_post_type('fornecedor', $args);

}

add_action('init','post_banner');
add_action('init','post_fornecedor');

?>