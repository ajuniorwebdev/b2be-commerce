<?php

namespace App;

use App\Cart;
use App\Seller;
use App\ClientUser;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'client';

    public function seller()
    {
        return $this->belongsTo('App\Seller', 'codusur1', 'codusur');
    }

    public static function subsidiary(Request $request, $search)
    {
        $where = null;
        if(!empty($search['primary_search'])) {
            $words = explode(' ', $search['primary_search']);
            $words_matched = null;
            foreach ($words as $word) {
                $words_matched .= "+" . $word . "* ";
            }
            $where .= " AND MATCH(cliente, cgcent, fantasia, estent) AGAINST('{$words_matched}' IN BOOLEAN MODE)";
        }

        $codcli = Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->first()->codcli;
        $sql = "
            select
                codcli,
                cliente,
                cgcent,
                fantasia,
                estent,
                codcliprinc
            from client
            where codcliprinc = (
                select codcliprinc from client where codcli = {$codcli}
            )
            {$where}
            ;
        ";

        return DB::select($sql);
    }

    public static function subsidiaryHeader()
    {
        $codcli = Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->first()->codcli;
        $sql = "
            select
                codcli,
                cliente,
                cgcent,
                fantasia,
                estent,
                codcliprinc
            from client
            where codcliprinc = (
                select codcliprinc from client where codcli = {$codcli}
            );
        ";

        return DB::select($sql);
    }

    public static function subsidiary_branchs($codcli)
    {
        $sql = "
            select
                codcli,
                cliente,
                cgcent,
                fantasia,
                estent,
                codcliprinc
            from client
            where codcliprinc = {$codcli}
        ";
        return DB::select($sql);
    }

    public static function portfolio($codusur)
    {
        $sql = "
            select
                cli.codcli,
                cli.cliente,
                cli.cgcent,
                cli.fantasia,
                cli.estent,
                cli.codcliprinc
            from client_user cliu
            left join client cli on cliu.codcli = cli.codcli 
            where cliu.codusur = {$codusur}
        ";
        return DB::select($sql);
    }

    public static function getLimiteCredito($codcli)
    {
        $sql = "
            select
                limcred
            from client cli
            where codcli = {$codcli}
        ";
        return DB::select($sql);
    }

}
