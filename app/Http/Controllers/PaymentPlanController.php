<?php

namespace App\Http\Controllers;

use App\Cart;
use App\PaymentPlan;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentPlanController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function change(Request $request)
    {
        try {
            $average = PaymentPlan::averageSelected($request->input('plan'));
            $id = Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->first()->id;
            Cart::where(['id' => $id])
                ->update([
                    'codplpag' => $request->input('plan')
                ]);

            return response()
                ->json([
                    'msg' => 'Prazo alterado com sucesso!',
                    'res' => $average
                ], 200);
        }
        catch(Exception $e) {
            report($e);
            return response()
                ->json(['err' => 'Não foi possível selecionar outro prazo, contate o administrador!'], 401);
        }
    }

}
