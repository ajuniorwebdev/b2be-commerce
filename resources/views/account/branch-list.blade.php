<tbody>
@if(count($branchs) > 0)
    @foreach($branchs as $br)
        <tr data-id="{{ $br->codcli }}">
            <td>{{ $br->cgcent }}</td>
            <td>{{ $br->cliente }}</td>
            <td>{{ $br->fantasia }}</td>
            <td>{{ $br->estent }}</td>
            <td><a href="javascript:void(0)" class="{{ $br->codcli == $br->codcliprinc ? 'btn-princ' : 'btn-remove' }}">{{ $br->codcli == $br->codcliprinc ? 'Principal' : 'Excluir' }}</a></td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="12" style="text-align: center;">
            <h4>Nenhuma filial encontrada</h4>
        </td>
    </tr>
@endif
</tbody>

