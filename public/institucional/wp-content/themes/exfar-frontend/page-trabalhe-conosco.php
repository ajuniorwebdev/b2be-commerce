<?php get_header(); ?>

<main id="work">
    <section class="work-header">
        <div class="work-header-shadow">
            <h2>TRABALHE CONOSCO</h2>
        </div>
    </section>
    <section class="work-form">
        <div class="container">
            <div class="work-form-headers animaleft">
                <h4>TRABALHE CONOSCO</h4>
                <p>Preencha os campos abaixo e anexe seu curriculo.</p>
            </div>

            <div class="work-form-content animaleft">
                <?php echo do_shortcode('[contact-form-7 id="48" title="Trabalhe conosco"]');?>
            </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>