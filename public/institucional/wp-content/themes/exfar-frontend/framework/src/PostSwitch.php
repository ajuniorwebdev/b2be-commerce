<?php

/**
 * Created by PhpStorm.
 * User: Alison
 * Date: 12/08/2015
 * Time: 16:16
 */
class PostSwitch
{

    public function getPostFromOtherSite($otherSiteId, $args, $postMeta, $key)
    {
        global $wp_query;

        if($key == null){
            $key = 'publish_dee';
        }

        switch_to_blog($otherSiteId);

        if(isset($args['meta_query'])){
            // Adicionando a condi��o de publicado na rede = 2 (Id do site atual)
            $args['meta_query'][] = [
                'key'     => $key,
                'value'   => serialize(strval(2)),
                'compare' => 'LIKE'
            ];
        }

        $wp_query = new WP_Query($args);
        $wp_query = $this->addPostMetaToPost($wp_query, $postMeta);

        restore_current_blog();

        return $wp_query;
    }

    /**
     * Adicionando propriedades customizadas ao objeto para utilizar na exibi��o do evento
     * � utilizado dentro do switch_to_blog(), pois n�o � poss�vel utilizar fora do contexto.
     *
     * @param $wpQuery
     * @param $postMeta
     * @return mixed
     */
    private function addPostMetaToPost($wpQuery, $postMeta)
    {
        if(is_array($wpQuery->posts) && count($wpQuery->posts) > 0)
        {

            foreach($wpQuery->posts as $key => $post) {

                foreach($postMeta as $newKey => $meta) {
                    if($meta['name'] == 'post_thumbnail'){
                        $image_id  = get_post_thumbnail_id($post->ID);
                        $image = wp_get_attachment_url($image_id);
                        $wpQuery->posts[$key]->$meta['name'] = $image;
                    }else{
                        $wpQuery->posts[$key]->$meta['name'] = get_post_meta($post->ID, $meta['name'], true);
                    }
                }

                $wpQuery->posts[$key]->external = 1;

            }

        }

        return $wpQuery;
    }

    public function getPost($args)
    {
        global $wp_query;
        $wp_query = new WP_Query($args);

        // Adicionando o date_event ao objeto para utilizar APENAS como par�metro de ordena��o
        if(is_array($wp_query->posts) && count($wp_query->posts) > 0){
            foreach($wp_query->posts as $key => $post){

                $data = get_post_meta($post->ID, 'date_event', true);
                $wp_query->posts[$key]->date_event = $data;

            }
        }

        return $wp_query;

    }

    public function dateCompare($a, $b)
    {
        $t1 = strtotime($a->date_event);
        $t2 = strtotime($b->date_event);
        return $t1 - $t2;
    }

    public function getMergedEvents($args, $blogId, $inverse=false, $postMeta=null, $key=null)
    {
        $list = new WP_Query($args);

        $list->post_count = ($this->getPostFromOtherSite($blogId, $args, $postMeta, $key)->post_count + $this->getPost($args)->post_count);

        $list->posts = array_merge($this->getPostFromOtherSite($blogId, $args, $postMeta, $key)->posts, $this->getPost($args)->posts);
        usort( $list->posts, array($this, 'dateCompare'));


        if($inverse){
            $list->posts = array_reverse($list->posts);
        }

        return $list;
    }

}