<?php
# Inclusão das classes
include 'src/Column.php';
include 'src/MetaBox.php';
include 'src/Taxonomy.php';
include 'src/FieldPageSetting.php';
include 'src/SectionPageSetting.php';
include 'src/PageSetting.php';
include 'src/CustomPostType.php';
include 'src/Field.php';
include 'src/GeneralSetting.php';

# Inclusao dos modulos
include 'modules/settings.php';