@extends('layouts.app')

@section('content')
    <div id="branchs" class="area-cliente-container">
        <div class="lateral">
            <ul>
                <li class="area-cliente"><a href="{{ route('overview') }}">Visão Geral</a></li>
                <li class="minha-conta"><span class="icon-cliente-1"></span><a href="{{ route('account') }}">Minha Conta</a></li>
                <li class="minhas-filiais active"><span class="icon-filiais-1"></span><a href="@role('customer') {{ route('branchs') }} @elserole('seller') {{ route('portfolio') }} @endrole">@role('customer') Minhas Filiais @elserole('seller') Carteira de Clientes @endrole</a></li>
                <li class="meus-pedidos"><span class="icon-pedidos-1"></span><a href="{{ route('orders') }}">Meus Pedidos</a></li>
            {{--<li class="chat-representante"><span class="icon-chat-1"></span><a href="chat-representante.html">Fale com o seu representante</a></li>--}}
            </ul>
        </div>
        <div class="content">
            <div class="wrapper small">
                <ul class="mobile-only area-cliente-atalhos">
                    <li class="minha-conta" data-aos="fade-up" data-aos-delay="0"><a href="{{ route('account') }}"><span class="icon-cliente-1"></span>Minha Conta</a></li>
                    <li class="minhas-filiais" data-aos="fade-up" data-aos-delay="100"><a href="@role('customer') {{ route('branchs') }} @elserole('seller') {{ route('portfolio') }} @endrole"><span class="icon-filiais-1"></span>@role('customer') Minhas Filiais @elserole('seller') Carteira de Clientes @endrole</a></li>
                    <li class="meus-pedidos" data-aos="fade-up" data-aos-delay="200"><a href="{{ route('orders') }}"><span class="icon-pedidos-1"></span>Meus Pedidos</a></li>
                {{--<li class="chat-representante" data-aos="fade-up" data-aos-delay="300"><a href="chat-representante.html" ><span class="icon-chat-1"></span>Fale com o seu representante</a></li>--}}
                </ul>
                <div class="page-title">
                    <h1>Minhas Filiais</h1>
                </div>
                <div class="produtos-tools afterclear">
                    <div class="busca-form">
                        <form class="search">
                            <input type="text" name="primary_search" id="busca-topo" placeholder="Pesquise por CNPJ, Razão Social, Nome Fantasia...">
                            <button type="submit" class="bt-busca icon-busca"><span>Pesquisar</span></button>
{{--                            <span class="toggle-busca-avancada icon-config"></span>--}}
{{--                            <div class="busca-avancada">--}}
{{--                                <h5>Busca Avançada</h5>--}}
{{--                                <ol class="col-group">--}}
{{--                                    <li class="col-3">--}}
{{--                                        <label for="cnpj">CNPJ</label>--}}
{{--                                        <input type="text" name="cnpj">--}}
{{--                                    </li>--}}
{{--                                    <li class="col-3">--}}
{{--                                        <label for="razao-social">Razão Sociai</label>--}}
{{--                                        <input type="text" name="razao-social">--}}
{{--                                    </li>--}}
{{--                                    <li class="col-3">--}}
{{--                                        <label for="nome-fantasia">Nome Fantasia</label>--}}
{{--                                        <input type="text" name="nome-fantasia">--}}
{{--                                    </li>--}}

{{--                                    <li class="col-1">--}}
{{--                                        <a href="#" class="toggle">Fechar</a>--}}
{{--                                        <button type="submit" class="bt-principal alignright">Pesquisar</button>--}}
{{--                                    </li>--}}
{{--                                </ol>--}}
{{--                            </div>--}}
                        </form>
                    </div>
                </div>
                <div class="page-content">
                    <div class="pedidos afterclear">
                        <div class="table-wrapper">
                            <table>
                                <thead>
                                <tr>
                                    <th>CNPJ</th>
                                    <th>Razão Social</th>
                                    <th>Nome Fantasia</th>
                                    <th>UF</th>
                                    <th>Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="12" style="text-align: center;">
                                        <div class="loader" style="text-align: center; height: 100px; line-height: 140px;">
                                            <svg width="48" height="48" viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg" version="1.1">
                                                <path d="M 150,0 a 150,150 0 0,1 106.066,256.066 l -35.355,-35.355 a -100,-100 0 0,0 -70.711,-170.711 z" fill="#0071bb">
                                                    <animateTransform attributeName="transform" attributeType="XML" type="rotate" from="0 150 150" to="360 150 150" begin="0s" dur=".5s" fill="freeze" repeatCount="indefinite"></animateTransform>
                                                </path>
                                            </svg>
                                        </div>
                                        <h4>Aguarde um instante...</h4>
                                    </td>
                                </tr>
                                </tbody>

                            </table>
                        </div>
                    {{--<a href="meus-pedidos.html" class="bt-principal alignright" >Ver todos os Pedidos</a>--}}
                    </div>
                    <div class="pedidos afterclear" style="padding-top: 20px;">
                        <h4>Adicionar nova Filial</h4>
                        <p>Se caso não souber o seu codigo de cliente, consulte umas das notas fiscal entregue por nós anteriormente ou entre em contato com o SAC</p>
                        <form id="branchs" method="post" action="{{ route('branchs.store') }}">
                            @csrf
                            <input type="hidden" name="act" value="I">
                            <ol class="col-group"> 
                                <li class="col-2 @error('cgcent') erro @enderror">
                                    <label for="cnpj">CNPJ da Filial *</label>
                                    <input type="text" name="cgcent" id="cnpj" placeholder="00.000.000/0001-00">
                                    @error('cgcent')
                                        <span class="msg-erro">Campo Obrigatório</span>
                                    @enderror
                                </li>
                                <li class="col-2 @error('codcli') erro @enderror">
                                    <label for="codigo-cliente" >Código de Cliente da Distribuidora *</label>
                                    <input type="text" name="codcli" id="codigo-cliente" placeholder="0">
                                    @error('codcli')
                                        <span class="msg-erro">Campo Obrigatório</span>
                                    @enderror
                                </li>
                                <li class="col-4-5">
                                    <small>* Campos Obrigatórios</small>
                                </li>
                                <li class="col-5">
                                    <button type="submit" name="enviar" class="bt-principal alignright">Enviar</button>
                                </li>
                            </ol>
                        </form>
                    </div>
                </div>
            </div>
            @if($config['fullbanner_account'])
                <div class="fullbanner">
                    <a href="#"><img src="{{ asset('/images/banners/'.$config['banner_fix']->bannerImg->img) }}"/></a>
                </div>
            @endif
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ mix('js/account.js') }}"></script>
    @if(session('success'))
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Sucesso!',
                text: '{{ session('success')['msg'] }}',
                showConfirmButton: true
            })
        </script>
    @endif

    @if(session('err'))
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Atenção!',
                text: '{{ session('err')['msg'] }}',
                showCancelButton: true,
                showConfirmButton: false
            })
        </script>   
    @endif
@endsection
