<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Cart;
use App\Client;
use App\ClientUser;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

  /**
   * Handle a login request to the application.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
   *
   * @throws \Illuminate\Validation\ValidationException
   */
  public function login(Request $request)
  {
    $this->validateLogin($request);

    // If the class is using the ThrottlesLogins trait, we can automatically throttle
    // the login attempts for this application. We'll key this by the username and
    // the IP address of the client making these requests into this application.
    if (method_exists($this, 'hasTooManyLoginAttempts') && $this->hasTooManyLoginAttempts($request)) {
      $this->fireLockoutEvent($request);
      return $this->sendLockoutResponse($request);
    }

    if ($this->attemptLogin($request)) {
        $this->setCart();
        return $this->sendLoginResponse($request);
    }

    // If the login attempt was unsuccessful we will increment the number of attempts
    // to login and redirect the user back to the login form. Of course, when this
    // user surpasses their maximum number of attempts they will get locked out.
    $this->incrementLoginAttempts($request);

    return $this->sendFailedLoginResponse($request);
  }

  /**
   * Validate the user login request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return void
   *
   * @throws \Illuminate\Validation\ValidationException
   */
  protected function validateLogin(Request $request)
  {
    $request->validate([
      $this->username() => 'required|string',
      'password' => 'required|string',
    ], [
      $this->username() . '.required' => 'O campo '. $this->username() .' é obrigatório.',
      'password.required' => 'O campo senha é obrigatório'
    ]);
  }

  /**
   * Get the failed login response instance.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Symfony\Component\HttpFoundation\Response
   *
   * @throws \Illuminate\Validation\ValidationException
   */
  protected function sendFailedLoginResponse(Request $request)
  {
    throw ValidationException::withMessages([
      'failed' => [trans('auth.failed')],
    ]);
  }

  public function setCart()
  {
      // check if param table is filledt
      if(!Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->exists()) {
          try {
              // get id of user
              $data['user_id'] = Auth::id();
              // check if user is a customer or seller
              $user = User::where(['id' => Auth::id()])->first();

              if($user->hasRole('customer')) {
                  // customer
                  $data['codcli'] = $user->codcli;
              }
              else if($user->hasRole('seller')) {
                  // seller
                  $data['codcli'] = ClientUser::where(['codusur' => $user->codusur])
                      ->orderBy('codcli', 'asc')
                      ->first()
                      ->codcli;
              }
              // get payment_plan of client
              $data['codplpag'] = Client::where(['codcli' => $data['codcli']])->select('codplpag')->first()->codplpag;

              $data['items_qt'] = 0;
              $data['subtotal'] = 0.00;
              $data['created_at'] = now();
              $data['updated_at'] = null;

              if(Cart::create($data)) {
                  return true;
              }
          }
          catch(Exception $e) {
              report($e);
              return true;
          }
      }
  }

}
