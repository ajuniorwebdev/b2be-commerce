<?php

namespace App\Http\Controllers;

use App\Cart;
use App\CartItem;
use App\Client;
use App\Product;

use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('cart.index');
    }

    public function cartList(Request $request)
    {
        try {

            $cart_items = CartItem::list();
            $empty = false;
            if(count($cart_items) <= 0) {
                $empty = true;
            }
            $page = $request->input('page');
            $perPage = 999;
            $offset = ($page * $perPage) - $perPage;

            $cart_items = new LengthAwarePaginator(array_slice($cart_items, $offset, $perPage, true), count($cart_items), $perPage, $page, ['path' => $request->url(), 'query' => $request->query()]);
            $subtotal = Cart::getSubtotalCart();

            $html = view('cart.list')->with(['cart_items' => $cart_items, 'subtotal' => $subtotal])->render();

            return response()
                ->json(['html' => $html, 'empty' => $empty], 200);
        }
        catch(Exception $e) {
            report($e);
            return response()
                ->json('Não foi possível carregar a listagem de items no carrinho, contate o administrador do sistema!', 401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->storeValidator($request->all())->validate();
        try {
            // check cart already exists
            $cart_id = $this->cartExists()->id;

            // check if product already exist
            if(CartItem::where(['cart_id' => $cart_id, 'product_id' => $request->input('id')])->exists()) {
                // check if quantity is 0
                if(intval($request->input('q')) <= 0) {
                    $id = CartItem::where(['cart_id' => $cart_id, 'product_id' => $request->input('id')])->select(['id'])->first()->id;
                    if($this->destroy($id)) {
                        $subtotal = Cart::getSubtotalCart();
                        $subtotal = number_format($subtotal[0]->subtotal, '2', ',', '.');
                        $qt = Cart::getQuantityItemsCart()[0]->qt;
                        $cart_items = view('cart.list_header')->with(['cart_items' => Cart::getCartItems()])->render();
                        return response()
                            ->json(['product_price' => 'R$ 0,00', 'subtotal' => $subtotal, 'qt' => $qt, 'cart_items_html' => $cart_items], 200);
                    }
                }
                // if exist, call update method passing Request with parameters
                $id = CartItem::where(['cart_id' => $cart_id, 'product_id' => $request->input('id')])->select(['id'])->first()->id;
                if($this->update($request, $id)) {
                    $product_price = CartItem::where(['id' => $id])->select(['subtotal'])->first()->subtotal;
                    $product_price = 'R$ '.number_format($product_price, '2', ',', '.');
                    $subtotal = Cart::getSubtotalCart();
                    $subtotal = number_format($subtotal[0]->subtotal, '2', ',', '.');
                    $qt = Cart::getQuantityItemsCart()[0]->qt;
                    $cart_items = view('cart.list_header')->with(['cart_items' => Cart::getCartItems()])->render();
                    return response()
                        ->json(['product_price' => $product_price, 'subtotal' => $subtotal, 'qt' => $qt, 'cart_items_html' => $cart_items], 200);
                }
            }

            // if not get financial information in product list method
            $request->request->add(['codeproduct_search' => $request->input('id')]);
            if($request->has('prm')) {
                $request->request->add(['codpromocaomed' => $request->input('prm')]);
            }
            $product = Product::az($request, 'az', []);
            // insert into cart and cart itens
            $price_discount = $product[0]->preco_tabela - (($product[0]->preco_tabela * $product[0]->desconto)/100);
            $qt = $request->input('q');
            $tax = ($product[0]->impostos/100);

            $data = [
                'cart_id' => $cart_id,
                'product_id' => $product[0]->codprod,
                'product_ean' => $product[0]->codauxiliar,
                'product_description' => $product[0]->descricao_produto,
                'price' => $product[0]->preco_tabela,
                'discount' => $product[0]->desconto,
                'tax' => $product[0]->impostos,
                'qt' => $qt,
                'price_discount' => $price_discount,
                'price_tax' => ($product[0]->preco_tabela + (($product[0]->preco_tabela/100) * $tax)),
                'price_discount_tax' => ($price_discount + ($price_discount * $tax)),
                'subtotal' => (($price_discount * $qt) + (($price_discount * $qt) * $tax)),
                'created_at' => now(),
                'updated_at' => null
            ];
            if($cart_item = CartItem::create($data)) {
                $product_price = 'R$ '.number_format(CartItem::where(['id' => $cart_item->id])->select(['subtotal'])->first()->subtotal, '2', ',', '.');
                $subtotal = number_format(Cart::getSubtotalCart()[0]->subtotal, '2', ',', '.');
                $qt = Cart::getQuantityItemsCart()[0]->qt;
                $cart_items = view('cart.list_header')->with(['cart_items' => Cart::getCartItems()])->render();
                return response()
                    ->json(['product_price' => $product_price, 'subtotal' => $subtotal, 'qt' => $qt, 'cart_items_html' => $cart_items], 200);
            }
        }
        catch(Exception $e) {
            report($e);
        }
    }

    /**
     * Validation of cart input products
     *
     * @param $data
     * @return mixed
     */
    protected function storeValidator($data)
    {
        return Validator::make($data, [
            'id' => ['required'],
            'q' => ['required', 'integer']
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        // find current information about product and financial data
        $request->request->add(['codeproduct_search' => $request->input('id')]);
        $product = Product::az($request, 'az', []);

        // create data array with information for update
        $price_discount = $product[0]->preco_tabela - (($product[0]->preco_tabela * $product[0]->desconto)/100);
        $qt = $request->input('q');
        $tax = ($product[0]->impostos/100);

        $data = [
            'price' => $product[0]->preco_tabela,
            'discount' => $product[0]->desconto,
            'tax' => $product[0]->impostos,
            'qt' => $request->input('q'),
            'price_discount' => $price_discount,
            'price_tax' => ($product[0]->preco_tabela + (($product[0]->preco_tabela/100) * $tax)),
            'price_discount_tax' => ($price_discount + ($price_discount * $tax)),
            'subtotal' => (($price_discount * $qt) + (($price_discount * $qt) * $tax)),
            'updated_at' => now()
        ];
        // update all fields in cart item
        if(CartItem::where(['id' => $id])->update($data)) {
            return true;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            return CartItem::destroy($id);
        }
        catch(Exception $e) {
            report($e);
            return true;
        }
    }

    /**
     *
     */
    public function cartExists()
    {
        // check if param table is filled
        if(!Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->exists()) {
            try {
                // get id of user
                $data['user_id'] = Auth::id();
                // check if user is a customer or seller
                $user = User::where(['id' => Auth::id()])->first();
                if($user->codcli) {
                    // customer
                    $data['codcli'] = $user->codcli;
                }
                else if($user->codusur) {
                    // seller
                    $data['codcli'] = Client::where(['codusur' => $user->codusur])
                        ->orderBy('codcli', 'asc')
                        ->first();
                }
                // get payment_plan of client
                $data['codplpag'] = Client::where(['codcli' => $data['codcli']])->select('codplpag')->first();

                $data['items_qt'] = 0;
                $data['subtotal'] = 0.00;
                $data['dt_created'] = now();

                return Cart::create($data);
            }
            catch(Exception $e) {
                report($e);
                return true;
            }
        }
        else {
            return Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->select('id')->first();
        }
    }

}
