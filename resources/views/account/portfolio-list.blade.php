<tbody>
@foreach($portfolios as $portfolio)
    <tr @if($portfolio->bloqueio == 'S') style="background-color: #ff33335c !important;" @endif>
        <td><a href="javascript:void(0)">{{ $portfolio->cgcent }}</a></td>
        <td>{{ $portfolio->fantasia }}</td>
        <td>{{ $portfolio->bairroent }}</td>
        <td>{{ $portfolio->municent }}</td>
        <td>R${{ number_format($portfolio->limcred, '2', ',', '.') }}</td>
        <td>{{ $portfolio->qtd_aberto }}</td>
        <td>R${{ number_format($portfolio->vr_aberto, '2', ',', '.') }}</td>
        <td>{{ $portfolio->qtd_vencido }}</td>
        <td>R${{ number_format($portfolio->vr_vencido, '2', ',', '.') }}</td>
        <td>{{ $portfolio->dtvencalvarafunc }}</td>
        <td>{{ $portfolio->dtvencalvarasanit }}</td>
        <td>{{ $portfolio->dtvencalvarafuncafe }}</td>
        <td>{{ $portfolio->dtvenccrf }}</td>
        <td class="center">
            <a id="cliente-topo" data-id="{{ $portfolio->codcli }}" href="javascript:void(0)">Selecionar</a>
        </td>
    </tr>
@endforeach
</tbody>
