<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerImg extends Model
{
    protected $fillable = [
        'description', 'img', 'created_at', 'updated_at'
    ];

    protected $table = 'banners_img';
}
