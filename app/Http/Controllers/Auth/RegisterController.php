<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Exception;
use App\Client;
use App\ProfileUser;
use App\ClientRequest;
use App\Mail\RequestAcess;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws ValidationException
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $profile = ($request->input('profile') === '1') ? 'customer' : 'seller';

        try {
            $user->assignRole($profile);

            return $request->wantsJson()
                ? new Response('', 201)
                : redirect('/login')->with('register_success', ['msg' =>'Usuário criado com sucesso!']);
        }
        catch(Exception $e) {
            report($e);

            $this->rollback($user);
            throw ValidationException::withMessages([
                'failed' => 'Não foi possível fazer o primeiro acesso.',
            ]);
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if ($data['profile'] === '1') {
            return Validator::make($data, [
                'cnpj' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
        } else {
            return Validator::make($data, [
                'seller_code' => ['required', 'integer'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     * @throws ValidationException
     */
    protected function create(array $data)
    {
        if ($data['profile'] === '1') {
            // client
            $client = Client::where(['cgcent' => $data['cnpj']])
                ->select('cliente', 'codcli', 'cgcent')
                ->first();

            if (!$client) {
                throw ValidationException::withMessages([
                    'failed' => 'CNPJ não consta em nossa base. Tente novamente.',
                ]);
            }

            return $user = User::create([
                'name' => $client->cliente,
                'email' => $data['email'],
                'codcli' => $client->codcli,
                'password' => Hash::make($data['password']),
            ]);

        }
        else {
            // seller
            $seller = ProfileUser::where(['codusur' => $data['seller_code']])
                ->select('nome')
                ->first();

            if (!$seller) {
                throw ValidationException::withMessages([
                    'failed' => 'Código incorreto.',
                ]);
            }

            return $user = User::create([
                'name' => $seller->nome,
                'email' => $data['email'],
                'codusur' => $data['seller_code'],
                'password' => Hash::make($data['password']),
            ]);

        }

    }

    protected function rollback($user) {

        try {
            User::destroy($user->id);
        }
        catch(Exception $e) {
            report($e);
            return true;
        }

    }

    public function showClientRequest()
    {
        return view('auth.client_request_register');
    }

    public function sendClientInformation(Request $request)
    {
        $this->validationClientRequest($request->all())->validate();
        try {
            $data = [
                'cnpj' => $request->input('cnpj'), // obrigatório
                'ie' => $request->input('inscricao_estadual'), // obrigatório
                'razao_social' => $request->filled('razao_social') ? $request->input('razao_social') : null, // não obrigatório
                'fantasia' => $request->filled('nome_fantasia') ? $request->input('nome_fantasia') : null, // não obrigatório
                'rede' => $request->filled('nome_rede') ? $request->input('nome_rede') : null, // não obrigatório
                'email_contato' => $request->input('email_contato'), // obrigatório
                'email_cobranca' => $request->filled('email_cobranca') ? $request->input('email_cobranca') : null, // não obrigatório
                'email_xml' => $request->filled('email_xml') ? $request->input('email_xml') : null, // não obrigatório
                'telefone_fixo' => $request->filled('telefone_fixo') ? $request->input('telefone_fixo') : null, // não obrigatório
                'telefone_celular' => $request->input('telefone_celular'), // obrigatório
                'nome' => $request->filled('nome_proprietario') ? $request->input('nome_proprietario') : null, // não obrigatório
                'cpf' => $request->filled('cpf_proprietario') ? $request->input('cpf_proprietario') : null, // não obrigatório
                'email' => $request->filled('email_proprietario') ? $request->input('email_proprietario') : null, // não obrigatório
                'celular' => $request->filled('telefone_proprietario') ? $request->input('telefone_proprietario') : null, // não obrigatório
                'comprador_med_nome' => ($request->filled('nome_comprador_medicamentos')) ? $request->input('nome_comprador_medicamentos') : null,// não obrigatório
                'comprador_med_email' => ($request->filled('email_comprador_medicamentos')) ? $request->input('email_comprador_medicamentos') : null,// não obrigatório
                'comprador_med_tel_fixo' => ($request->filled('telefone_fixo_comprador_medicamentos')) ? $request->input('telefone_fixo_comprador_medicamentos') : null,// não obrigatório
                'comprador_med_tel_celular' => ($request->filled('telefone_celular_comprador_medicamentos')) ? $request->input('telefone_celular_comprador_medicamentos') : null,// não obrigatório
                'comprador_per_nome' => ($request->filled('nome_comprador_perfumaria')) ? $request->input('nome_comprador_perfumaria') : null,// não obrigatório
                'comprador_per_email' => ($request->filled('email_comprador_perfumaria')) ? $request->input('email_comprador_perfumaria') : null,// não obrigatório
                'comprador_per_tel_fixo' => ($request->filled('telefone_fixo_comprador_perfumaria')) ? $request->input('telefone_fixo_comprador_perfumaria') : null,// não obrigatório
                'comprador_per_tel_celular' => ($request->filled('telefone_celular_comprador_perfumaria')) ? $request->input('telefone_celular_comprador_perfumaria') : null,// não obrigatório
                'condicao_comercial' => ($request->filled('limite_sugerido')) ? str_replace(',', '.',str_replace('.','',$request->input('limite_sugerido'))) : 0, // não obrigatório,
                'created_at' => now(),
                'updated_at' => now()
            ];

            if(Mail::to('sac@exfar.com.br')->bcc(['a.juniorwebdev@gmail.com', 'simoes.ul@gmail.com'])->send(new RequestAcess(ClientRequest::create($data)))) {
                throw new Exception('error');
            }

            $token = Hash::make($request->input('cnpj'));
            $token = str_replace('/', '.', $token);
            return response()
                ->redirectToRoute('client.request.success', ['token' => $token])
                ->with('success');
        }
        catch(Exception $e) {
            report($e);

            return response()
                ->redirectToRoute('client.request.register')
                ->with('error', ['msg' => 'Não foi possível fazer sua solicitação nesse momento, por favor tente mais tarde!']);
        }
    }

    protected function validationClientRequest($data) {
        return Validator::make($data, [
            'cnpj' => ['required', 'unique:client_request'],
            'inscricao_estadual' => ['required'],
            'email_contato' => ['required', 'email'],
            'telefone_celular' => ['required'],
        ],[
            'required' => 'O campo :attribute é obrigatório.'
        ]);
    }

    public function uploadFiles(Request $request) {

        try {
            $cnpj = str_replace(['.','/','-'], '',$request->input('cnpj'));
            if(!Storage::exists('/documents/'.$cnpj)) {
                Storage::makeDirectory('/documents/'.$cnpj);
            }

            $request->file('file')->store('/documents/'.$cnpj);

            return true;
        }
        catch(Exception $e) {
            report($e);

            return response()
                ->redirectToRoute('client.request.register')
                ->with('error', ['msg' => 'Não foi possível carregar este arquivo, tente novamente mais tarde!']);
        }

    }

    public function clientRequestSuccess() {
        return view('auth.client_request_success');
    }
}
