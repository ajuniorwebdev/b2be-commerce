@extends('layouts.app')

@section('content')
    <div class="area-cliente-container">
        <div class="lateral">
            <ul>
                <li class="area-cliente"><a href="{{ route('overview') }}">Visão Geral</a></li>
                <li class="minha-conta"><span class="icon-cliente-1"></span><a href="{{ route('account') }}">Minha Conta</a></li>
                <li class="minhas-filiais"><span class="icon-filiais-1"></span><a href="@role('customer') {{ route('branchs') }} @elserole('seller') {{ route('portfolio') }} @endrole">@role('customer') Minhas Filiais @elserole('seller') Carteira de Clientes @endrole</a></li>
                <li class="meus-pedidos active"><span class="icon-pedidos-1"></span><a href="{{ route('orders') }}">Meus Pedidos</a></li>
{{--                <li class="chat-representante"><span class="icon-chat-1"></span><a href="chat-representante.html">Fale com o seu representante</a></li>--}}
            </ul>
        </div>
        <div class="content">
            <div class="wrapper">
                <ul class="mobile-only area-cliente-atalhos">
                    <li class="minha-conta aos-init aos-animate" data-aos="fade-up" data-aos-delay="0"><a href="{{ route('account') }}"><span class="icon-cliente-1"></span>Minha Conta</a></li>
                    <li class="minhas-filiais aos-init aos-animate" data-aos="fade-up" data-aos-delay="100"><a href="@role('customer') {{ route('branchs') }} @elserole('seller') {{ route('portfolio') }} @endrole"><span class="icon-filiais-1"></span>@role('customer') Minhas Filiais @elserole('seller') Carteira de Clientes @endrole</a></li>
                    <li class="meus-pedidos aos-init aos-animate" data-aos="fade-up" data-aos-delay="200"><a href="{{ route('orders') }}"><span class="icon-pedidos-1"></span>Meus Pedidos</a></li>
{{--                    <li class="chat-representante aos-init aos-animate" data-aos="fade-up" data-aos-delay="300"><a href="chat-representante.html"><span class="icon-chat-1"></span>Fale com o seu representante</a></li>--}}
                </ul>
                <div class="page-title center" style="margin-bottom: 40px;">
                    <h1>Pedido Nº {{ $number_order }}</h1>
                </div>
                <div class="page-content">
                    <div class="pedidos afterclear">
                        <div class="table-wrapper">
                            <ul class="status">
                                @if($order_items[0]->order_status_id == 2)
                                    <li class="ok"><span>1</span>Pedido Enviado</li>
                                    <li class="standby"><span>2</span>Processamento e Validação</li>
                                    <li class="standby"><span>3</span>Faturamento</li>
                                    <li class="standby"><span>4</span>Separação</li>
                                    <li class="standby"><span>5</span>Pedido Finalizado</li>
                                @elseif($order_items[0]->order_status_id <> 2 && ($order_items[0]->order_status_id == 12 && isset($order_items[0]->dt_sended)))
                                    <li class="ok"><span>1</span>Pedido Enviado</li>
                                    <li class="ok"><span>2</span>Processamento e Validação</li>
                                    <li class="standby"><span>3</span>Faturamento</li>
                                    <li class="standby"><span>4</span>Separação</li>
                                    <li class="standby"><span>5</span>Pedido Finalizado</li>
                                @elseif($order_items[0]->order_status_id <> 2 && $order_items[0]->order_status_id <> 12 && $order_items[0]->order_status_id == 4)
                                    <li class="ok"><span>1</span>Pedido Enviado</li>
                                    <li class="ok"><span>2</span>Processamento e Validação</li>
                                    <li class="{{ ($order_items[0]->posicao_atual == 'C') ? 'erro' : 'ok' }}"><span>3</span>{{ ($order_items[0]->posicao_atual == 'C') ? 'Cancelado' : 'Faturamento' }}</li>
                                    <li class="standby"><span>4</span>Separação</li>
                                    <li class="standby"><span>5</span>Pedido Finalizado</li>
                                @elseif($order_items[0]->order_status_id <> 2 && $order_items[0]->order_status_id <> 12 && $order_items[0]->order_status_id <> 4 && $order_items[0]->order_status_id == 7)
                                    <li class="ok"><span>1</span>Pedido Enviado</li>
                                    <li class="ok"><span>2</span>Processamento e Validação</li>
                                    <li class="ok"><span>3</span>Faturamento</li>
                                    <li class="ok"><span>4</span>Separação</li>
                                    <li class="ok"><span>5</span>Pedido Finalizado</li>
                                @endif
                            </ul>
                        </div>
                        <ul class="info-pedido">
                            <li><strong class="left">Data do Pedido: </strong><span class="right">{{ $order_items[0]->created_at }}</span></li>
                            <li><strong class="left">Origem da Venda: </strong><span class="right">E-Commerce</span></li>
                            <li><strong class="left">Filial: </strong><span class="right" style="width: 60%">{{ $order_items[0]->fantasia }}</span></li>
                            @php $total = explode(',', number_format($order_items[0]->total_erp, '2', ',', '.')); @endphp
                            <li><strong class="left">Valor: </strong><span class="right preco">R$ <strong>{{ $total[0] }}</strong>,{{$total[1]}}</span></li>
                        </ul>
                        <ul class="log-pedido">
                            <li><strong class="left erro">Status: </strong><span class="status-pedido right erro">{{ $order_items[0]->status_name }}</span></li>
                            @if($order_items[0]->created_at) <li><strong class="left">{{ $order_items[0]->created_at }}: </strong><span class="right"> Pedido Criado com sucesso.</span></li> @endif
                            @if($order_items[0]->dt_sended) <li><strong class="left">{{ $order_items[0]->dt_sended }}: </strong><span class="right"> Pedido Enviado com sucesso. Aguardando processamento e validação.</span></li> @endif
                            @if($order_items[0]->updated_at > 0) <li><strong class="left">{{ $order_items[0]->updated_at }}: </strong><span class="right"> Processamento e validação concluídos.</span></li> @endif
                            @if($order_items[0]->dt_cancelado > 0)<li><strong class="left">{{ $order_items[0]->dt_cancelado }}: </strong><span class="right"> Pedido Cancelado.</span></li>@endif
                            @if($order_items[0]->dt_inicio_separacao > 0) <li><strong class="left">{{ $order_items[0]->dt_inicio_separacao }}: </strong><span class="right"> Pedido em separação.</span></li> @endif
                            @if($order_items[0]->dt_faturado > 0) <li><strong class="left">{{ $order_items[0]->dt_faturado }}: </strong><span class="right"> Pedido Faturado.</span></li> @endif
                        </ul>
                        <hr>
                        <h3 class="center">Produtos deste Pedido</h3>
                        <div class="table-wrapper">
                            <form>
                                <table>
                                    <thead>
                                    <tr>
                                        <th>Marca</th>
                                        <th>EAN</th>
                                        <th>Produto</th>
                                        <th class="center">Preço</th>
                                        <th class="center">Imposto Unit.</th>
                                        <th class="center">Desconto</th>
                                        <th class="center">Preço Liquido</th>
                                        <th class="center">Qtd Solic.</th>
                                        <th class="center">Qtd Faturada.</th>
                                        <th class="center ">Total</th>
                                        <th class="center">Qtd Não Faturada</th>
                                        <th>Motivo</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <td colspan="13"><small>Total: R$</small>{{ $total[0] }}<small>,{{ $total[1] }}</small></td>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($order_items as $osi)
                                        <tr>
                                            <td>{{ $osi->marca }}</td>
                                            <td>{{ $osi->ean }}</td>
                                            <td>{{ $osi->description }}</td>
                                            <td class="center ">R$ {{ number_format($osi->preco_bruto, '2', ',', '.') }}</td>
                                            <td class="center">R$ {{ number_format($osi->vlrepasse, '2', ',', '.') }}</td>
                                            <td class="center">{{ number_format($osi->percdesc, '2', ',', '.') }}%</td>
                                            <td class="center ">R${{ number_format($osi->preco_liquido, '2', ',', '.') }}</td>
                                            <td class="center">{{ $osi->qt_solicitada }}</td>
                                            <td class="center">{{ $osi->qtd_faturado }}</td>
                                            <td class="center">R${{ number_format(($osi->precocontabil * $osi->qtd_faturado), '2', ',', '.') }}</td>
                                            <td class="center ">{{ $osi->qtd_naofaturado }}</td>
                                            <td class="center ">{{ utf8_decode($osi->motivoatend) }}</td>
                                        </tr>
                                    @endforeach

                                    </tbody>

                                </table>
                            </form>
                        </div>
                    </div>
                    <hr>
                    <div class="pedidos">
                        <h4 class="center">Faturas e Nota Fiscal</h4>
                        <div class="faturas-hidden open">
                            <div class="table-wrapper">
                                <table>
                                    <thead>
                                        <th>Nº Parcela</th>
                                        <th>Data de Vencimento</th>
                                        <th>Status</th>
                                        <th class="center">Valor</th>
                                        <th>Data de Pagamento</th>
                                        <th class="center"></th>
                                    </thead>
                                    <tbody>
                                        @foreach($installments as $installment)
                                            <tr >
                                                <td>{{ $installment->prest }}</td>
                                                <td>{{ $installment->dtvenc }}</td>
                                                <td>{{ $installment->status_boleto }}</td>
                                                <td class="center">R$ {{ number_format($installment->valor, '2', ',', '.') }}</td>
                                                <td class="center">{{ $installment->dtpag }}</td>
                                                <td class="center">
                                                    @if(empty($installment->dtpag))
                                                        <a target="_blank" href="https://winthordanfe.s3.us-east-2.amazonaws.com/BOLETO/{{ $installment->numtransvenda . '-' . $installment->duplic }}.pdf">Imprimir Boleto</a>
                                                    @else
                                                        <a href="javascript:void(0)" class="disabled">Imprimir Boleto</a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="meus-pedidos-footer">
                        @if(isset($invoice_order[0]))
                            <a class="bt-principal alignright" target="_blank" href="https://winthordanfe.s3.us-east-2.amazonaws.com/DANFE/{{ $invoice_order[0]->chavenfe }}.pdf">Imprimir Danfe</a>
                            <a class="bt-principal alignright" target="_blank" href="https://winthordanfe.s3.us-east-2.amazonaws.com/XML/{{ $invoice_order[0]->chavenfe }}-nfe-proc.xml">Baixar Nota Fiscal (xml)</a>
                        @else
                            <a class="bt-principal alignright disabled" href="javascript:void(0)">Imprimir Danfe</a>
                            <a class="bt-principal alignright disabled" href="javascript:void(0)">Baixar Nota Fiscal (xml)</a>
                        @endif
                        <a href="{{ route('orders') }}">← Voltar para a lista de pedidos</a>
                    </div>

                </div>
            </div>
            @if($config['fullbanner_account'])
                <div class="fullbanner">
                    <a href="#"><img src="{{ asset('/images/banners/'.$config['banner_fix']->bannerImg->img) }}"/></a>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ mix('js/account.js') }}"></script>
    @if(session('success'))
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Sucesso!',
                text: '{{ session('success')['msg'] }}',
                showConfirmButton: true
            })
        </script>
    @endif
@endsection
