<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\Cart;
use App\Client;
use App\Product;
use App\Destaque;
use App\PaymentPlan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;


class HandleHeaderInfomation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $header['oferta'] = Destaque::oferta();
        if(Auth::check()) {
            $user = User::where(['id' => Auth::id()])->first();

            $header['cart_items'] = Cart::getCartItems();
            $header['subtotal'] = number_format(Cart::getSubtotalCart()[0]->subtotal, '2', ',', '.');
            $header['qt'] = Cart::getQuantityItemsCart()[0]->qt;

            $header['pp']['plans'] = PaymentPlan::average();
            $header['pp']['pp_selected'] = PaymentPlan::averageSelected();

            $header['limite_credito'] = Client::getLimiteCredito(Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->first()->codcli);

            if($user->hasRole('customer')) {
                $header['cli']['subsidiary'] = Client::subsidiaryHeader();
                $header['cli']['selected'] = Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->first()->codcli;
            }
            else if($user->hasRole('seller')) {
                $header['cli']['portfolio'] = Client::portfolio($user->codusur);
                $header['cli']['selected'] = Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->first()->codcli;
                $header['cli']['branchs'] = Client::subsidiary_branchs($header['cli']['selected']);
            }
        }

        View::share('header', $header);
        return $next($request);
    }
}
