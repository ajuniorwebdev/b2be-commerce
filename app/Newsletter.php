<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $fillable = [
        'email', 'created_at', 'updated_at', 'delete_at'
    ];

    protected $table = 'newsletter_email_list';

}
