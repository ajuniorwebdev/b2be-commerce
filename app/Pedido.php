<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $fillable = [
        'user_id','codcli','number_order','option_payments_id','codplpag','buy_by_seller','codusur','order_status_id','items_qt',
        'total','number_order_erp','numpedvan','created_at','updated_at','dt_canceled','dt_sended','referencia','status_creditcard_payment_id',
    ];

    protected $table = 'pedidos';

    public static function getOrdersClient($client, $search)
    {
        $where = null;
        if (!empty($search['primary_search'])) {
            $words = explode(' ', $search['primary_search']);
            $words_matched = null;
            foreach ($words as $word) {
                $words_matched .= "+" . str_replace('-', '',$word) . "* ";
            }
            $where .= " AND MATCH(ped.fulltext_pedido) AGAINST('{$words_matched}' IN BOOLEAN MODE) ";
        }

        $order_by = 'dt_order desc';
        $sql = "
            select distinct
                ped.id,
                ped.number_order,
                ped.numpedvan,
                inv.numnota invoice,
                case
                    when ped.dt_sended is null
                    then date_format(ped.created_at, '%d/%m/%Y %H:%i:%s')
                    else date_format(ped.dt_sended, '%d/%m/%Y %H:%i:%s')
                end dt_order,
                cli.fantasia,
                os.status_name,
                case
                    when os.id = 4 then ped.valorliquido
                    else ped.total
                end total
            from pedidos ped
            inner join order_status os on ped.order_status_id = os.id
            inner join client cli on ped.codcli = cli.codcli
            left join invoice inv on replace(ped.numped, '', null) = replace(inv.numped, 0, null)
            where codcliprinc = {$client} 
            {$where}
            order by ped.id desc
        ";

        return DB::select($sql);
    }

    public static function getOrdersSeller($codusur, $search)
    {
        $where = null;
        if (!empty($search['primary_search'])) {
            $words = explode(' ', $search['primary_search']);
            $words_matched = null;
            foreach ($words as $word) {
                $words_matched .= "+" . str_replace('-', '',$word) . "* ";
            }
            $where .= " AND MATCH(ped.fulltext_pedido) AGAINST('{$words_matched}' IN BOOLEAN MODE) ";
        }

        $order_by = 'dt_order desc';
        $sql = "
            select distinct
                ped.id,
                ped.number_order,
                ped.numpedvan,
                inv.numnota invoice,
                case
                    when ped.dt_sended is null
                    then date_format(ped.created_at, '%d/%m/%Y %H:%i:%s')
                    else date_format(ped.dt_sended, '%d/%m/%Y %H:%i:%s')
                end dt_order,
                cli.fantasia,
                os.status_name,
                case
                    when os.id = 4 then ped.valorliquido
                    else ped.total
                end total
            from pedidos ped
            inner join order_status os on ped.order_status_id = os.id
            inner join client cli on ped.codcli = cli.codcli
            inner join client_user cliu on cli.codcli and cliu.codcli
            left join invoice inv on ped.numped = inv.numped
            where cliu.codusur = {$codusur} 
            {$where}
            order by ped.id desc
        ";

        return DB::select($sql);
    }

    public static function getInstallmentsOrder($number_order)
    {

        $sql = "
            select distinct
                ins.duplic,
                concat(ins.prest, '/', (select max(prest) from installment where duplic = ins.duplic)) prest,
                date_format(str_to_date(ins.dtvenc, '%d-%M-%y'), '%d/%m/%Y') dtvenc,
                ins.valor,
                date_format(str_to_date(ins.dtpag, '%d-%M-%y'), '%d/%m/%Y') dtpag,
                ins.status,
                case
                    when ins.vpago <= 0 then 'Aberto'
                    when ins.vpago = ins.valor then 'Pago'
                end status_boleto,
                ins.numtransvenda
            from pedidos ped
            inner join installment ins on replace(ped.numped, '', null) = ins.numped
            where number_order = '{$number_order}';
        ";

        return DB::select($sql);
    }

    public static function getOrderItems($number_order)
    {

        $sql = "
            select
                date_format(ped.dt_processado, '%d/%m/%Y %H:%i:%s') dt_processado,
                cli.fantasia,
                case when ped.total_erp = '' then 0.00 else coalesce(ped.total_erp, 0.00) end total_erp,
                case when ped.order_status_id = 4 and ped.posicao_atual = 'C' then 'Pedido Cancelado' else os.status_name end status_name,
                ped.order_status_id,
                date_format(ped.created_at, '%d/%m/%Y %H:%i:%s') created_at,
                date_format(ped.updated_at, '%d/%m/%Y %H:%i:%s') updated_at,
                date_format(ped.dt_sended, '%d/%m/%Y %H:%i:%s') dt_sended,
                date_format(ped.dt_cancelado, '%d/%m/%Y %H:%i:%s') dt_cancelado,
                date_format(ped.dt_emissao_mapa, '%d/%m/%Y %H:%i:%s') dt_emissao_mapa,
                date_format(str_to_date(ped.dt_inicio_separacao, '%d/%m/%Y %H:%i:%s'), '%d/%m/%Y %H:%i:%s') dt_inicio_separacao,
                date_format(ped.dt_conferencia, '%d/%m/%Y %H:%i:%s') dt_conferencia,
                date_format(ped.dt_montando_carga, '%d/%m/%Y %H:%i:%s') dt_montando_carga,
                date_format(str_to_date(ped.dt_faturado, '%d/%m/%Y %H:%i:%s'), '%d/%m/%Y %H:%i:%s') dt_faturado,
                ped.posicao_atual,
                ped.observacao,
                br.marca,
                pedi.codprod,
                pedi.ean,
                pedi.description,
                pedi.qt qt_solicitada,
                pedi.numpedvan,
                ifnull(pedi.qtd_faturado, 0) qtd_faturado,
                ifnull(pedi.qtd_naofaturado, 0) qtd_naofaturado,
                ifnull(pedi.preco_bruto, 0.00) preco_bruto,
                pedi.percdesc,
                ifnull(pedi.preco_liquido, 0.00) preco_liquido,
                ifnull(pedi.vlrepasse, 0.00) vlrepasse,
                ifnull(pedi.precocontabil, 0.00) precocontabil,
                pedi.motivoatend
            from pedidos ped
            inner join pedidos_items pedi on ped.id = pedi.pedidos_id
            inner join client cli on ped.codcli = cli.codcli
            inner join order_status os on ped.order_status_id = os.id
            inner join product pro on pedi.codprod = pro.codprod
            inner join brand br on pro.codmarca = br.codmarca
            where ped.number_order = '{$number_order}'
        ";

        return DB::select($sql);
    }

    public static function getInvoiceOrder($number_order)
    {
        $sql = "
            select distinct
                inv.chavenfe
            from invoice inv
            inner join pedidos ped on inv.numped = ped.numped
            where ped.number_order = '{$number_order}' 
        ";
        return DB::select($sql);
    }
}
