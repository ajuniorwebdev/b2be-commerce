$(document).ready(() => {

    if(document.cookie.split('; ').some((item) => item.trim().startsWith('search_action='))) {
        $('form.search input[name="primary_search"]').val(document.cookie.split('; ').find(row => row.startsWith('search_action')).split('=')[1])
        document.cookie = 'search_action=; expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/'
    }

    Products.list(window.location.href,1, false)

    $('form.search').on('submit', (e) => {
        e.preventDefault()
        if(e.keyCode == 13 || e.keyCode == 9) {
            Products.list(window.location.href, 1, true, $('form.search').serialize())
        }
    })

    $('form.search button.btn-search').on('click', () => {
        if($('div.busca-avancada.open')[0] != undefined) {
            $('div.busca-avancada').removeClass('open')
        }
        Products.list(window.location.href, 1, true, $('form.search').serialize())
    })

    $('input[name="primary_search"]').on('keyup', (e) => {
        let loader = ($('div.loading').length > 0) ? false : true
        if($(e.target).val().length >= 3) {
            Products.list(window.location.href, 1, loader, $('form.search').serialize())
        }
        if($(e.target).val().length == 0) {
            Products.list(window.location.href, 1, loader, $('form.search').serialize())
        }
    })

    $('table#products').on('click', (e) => {
        if($(e.target).hasClass('minus')) {
            let old_qtd = $(e.target).parent().find('input.qtd').val()
            if(parseInt(old_qtd) > 0) {
                $(e.target).parent().find('input.qtd').val(parseInt(old_qtd) - 1)
                $(e.target).parent().find('input.qtd').trigger('change')
                if($(e.target).parent().find('input.qtd').val() == 0) {
                    $(e.target).parent().parent().parent().removeClass('hot')
                    return
                }
                return
            }
            else {
                $(e.target).parent().parent().parent().removeClass('hot')
                return
            }
        }

        if($(e.target).hasClass('plus')) {
            let old_qtd = $(e.target).parent().find('input.qtd').val()
            $(e.target).parent().find('input.qtd').val(parseInt(old_qtd) + 1)
            $(e.target).parent().find('input.qtd').trigger('change')
            if(!$(e.target).parent().parent().parent().hasClass('hot')) {
                $(e.target).parent().parent().parent().addClass('hot')
            }
            return
        }
    })

    $('select#prod_filter_1').on('change', (e) => {
        $('select#prod_filter_2').val($(e.target).val())
        Products.list(window.location.href, 1, true, $('form.search').serialize(), $(e.target).val())
    })

    $('select#prod_filter_2').on('change', (e) => {
        $('select#prod_filter_1').val($(e.target).val())
        Products.list(window.location.href, 1, true, $('form.search').serialize(), $(e.target).val())
    })
})

const Products = {
    list: (list, page, loader, search, order) => {

        /*scrolling up to page*/
        window.scrollTo(0, 0);

        let list_url = list.split('/')
        list_url = list_url[list_url.length - 1]

        search = (!search) ? $('form.search').serialize() : search
        order = (!order) ? $('select#prod_filter_1').val() : order

        $.ajax({
            url: '/products/'+list_url,
            method: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                list: list,
                page: page,
                search: search,
                order: order
            },
            beforeSend: () => {
                if(loader) {
                    $('div.wrappper.fullscreen').prepend(`
                        <div class="loading"></div>
                        <div class="loader-container" style="position: absolute; z-index: 999999; width: 100%; margin: 2% auto;text-align: center;">
                            <div class="loader" style="text-align: center; height: 100px; line-height: 140px">
                                <svg width="48" height="48" viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg" version="1.1">
                                    <path d="M 150,0 a 150,150 0 0,1 106.066,256.066 l -35.355,-35.355 a -100,-100 0 0,0 -70.711,-170.711 z" fill="#0071bb">
                                        <animateTransform attributeName="transform" attributeType="XML" type="rotate" from="0 150 150" to="360 150 150" begin="0s" dur=".5s" fill="freeze" repeatCount="indefinite"></animateTransform>
                                    </path>
                                </svg>
                            </div>
                            <h4>Aguarde um instante...</h4>
                        </div>
                    `)
                }
            },
            complete: () => {
                if(loader) {
                    $('div.wrappper.fullscreen div.loading').remove()
                    $('div.wrappper.fullscreen div.loader-container').remove()
                }
            },
            success: (res) => {
                $('div.table-wrapper table tbody').remove()
                $('div.table-wrapper table tfoot').remove()
                $('div.table-wrapper table').append(res.html)
                $('div.pagination-container ul').remove()
                $('div.pagination-container').append(res.pagination)
                // listenner
                $('table#products input.qtd').mask('000000')
                $('table#products input.qtd').on('change', (e) => {
                    if($(e.target).val() > 0) {
                        if(!$(e.target).parent().parent().parent().hasClass('hot')) {
                            $(e.target).parent().parent().parent().addClass('hot')
                        }
                    }
                    else {
                        $(e.target).val(0)
                        $(e.target).parent().parent().parent().removeClass('hot')
                    }

                    Products.cart(e.target);

                })
                $('table#products input.qtd').on('focus', (e) => {
                    if($(e.target).val() == 0) {
                        $(e.target).val('')
                    }
                })
            },
            error: (err) => {
                console.log(err)
            }
        })
    },
    cart: (e) => {
        $.ajax({
            url: '/cart/store',
            method: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $(e).attr('data-id'),
                'q': $(e).val()
            },
            success: (res) => {
                $(e).parent().parent().parent().find('td').eq(-2).text('').text(res.product_price)
                $('table#products').find('tfoot tr td small span').text('').text(' ' + res.subtotal)
                $('table.tabela-produtos.carrinho tbody').remove()
                $('table.tabela-produtos.carrinho').append(res.cart_items_html)
                $('table.tabela-produtos.carrinho').find('tfoot tr td span.preco strong').text('').text(' '+res.subtotal)
                $('table.tabela-produtos.carrinho').find('tfoot tr td span.qt').text('').text(' '+res.qt)
                $('.mobile-menu-control.ferramentas-topo li.carrinho span.icon-carrinho-1 span.alert').text('').text(' '+res.qt)
                $('li.carrinho a.cart-info span.icon-carrinho-1 span.alert').text('').text(' '+res.qt)
                $('li.carrinho a.cart-info span.texto span.preco strong').text('').text(' '+res.subtotal)
                $('table.tabela-produtos.carrinho tbody tr td span.del').on('click', (e) => {
                    let input = $(e.target).parent().parent().find('input.qtd')
                    input.val(0)
                    Main.cart_header(input)
                })
                $('table.tabela-produtos.carrinho tbody input.qtd').on('change', (e) => {
                    Main.cart_header($(e.target))
                })
            },
            error: (error) => {
                console.log(error)
            }
        })
    }
}
