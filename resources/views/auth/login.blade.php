@extends('layouts.app')

@section('content')

    <div class="page-content">
        <div class="wrapper tiny">
            <div class="login-boxes">
                <div class="login-box">
                    <div class="login">
                        <h1>Entrar</h1>
                        <form method="post" action="{{ route('login') }}">
                            @csrf
                            <ol class="col-group">
                                <li class="col-1 @error('email') erro @enderror">
                                    <label for="email">E-mail</label>
                                    <input id="email" name="email" placeholder="Informe seu e-mail" type="email" value="{{ old('email') }}" autocomplete="off">
                                    @error('email')
                                        <span class="msg-erro">{{ $message }}</span>
                                    @enderror
                                </li>
                                <li class="col-1 @error('password') erro @enderror">
                                    <label for="password">Senha</label>
                                    <input id="password" name="password" placeholder="Informe sua senha" type="password" autocomplete="off">
                                    @error('password')
                                        <span class="msg-erro">{{ $message }}</span>
                                    @enderror
                                </li>
                                <li class="col-1">
                                    <a href="{{ route('password.request') }}">Esqueci minha senha</a>
                                    <!-- O Botão foi alterado por um link simples apenas para simular a navegação do sistema -->
                                    <!-- <button class="bt-principal alignright" type="submit">Entrar</button> -->
                                    <button class="bt-principal alignright" type="submit">Entrar</button>
                                </li>
                            </ol>
                        </form>
                    </div>
                </div>

                <div class="login-box">
                    <div class="primeiro-acesso">
                        <h3>Primeiro Acesso</h3>
                        <p>Você é nosso cliente mas ainda não tem acesso ao e-commerce <strong>Exfar Distribuidora</strong>?</p>
                        <a href="{{ route('register') }}" class="bt-sec">Primeiro Acesso</a>
                    </div>
                    <div class="solicite-acesso">
                        <h3>Solicite o Cadastro</h3>
                        <p>Ainda não é nosso cliente? Preencha a ficha de cadastro e aguarde nosso contato.</p>
                        <a href="{{ route('client.request.register') }}" class="bt-sec">Solicitar Cadastro</a>
                    </div>
                </div>

            </div>
        </div>

    </div>

@endsection

@section('js')
    <script src="{{mix('js/login.js')}}"></script>
    @if(session('register_success'))
        <script>
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: '{{ session('register_success')['msg'] }}',
                showConfirmButton: false,
                timer: 2500
            })
        </script>
    @endif

    @if(session('status'))
        <script>
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: '{{ session('status') }}'
            })
        </script>
    @endif
@endsection
