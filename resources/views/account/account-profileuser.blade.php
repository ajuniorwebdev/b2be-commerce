@extends('layouts.app')

@section('content')
    <div class="area-cliente-container">
        <div class="lateral">
            <ul>
                <li class="area-cliente"><a href="{{ route('overview') }}">Visão Geral</a></li>
                <li class="minha-conta active"><span class="icon-cliente-1"></span><a href="{{ route('account') }}">Minha Conta</a></li>
                <li class="minhas-filiais"><span class="icon-filiais-1"></span><a href="@role('customer') {{ route('branchs') }} @elserole('seller') {{ route('portfolio') }} @endrole">@role('customer') Minhas Filiais @elserole('seller') Carteira de Clientes @endrole</a></li>
                <li class="meus-pedidos"><span class="icon-pedidos-1"></span><a href="{{ route('orders') }}">Meus Pedidos</a></li>
{{--                <li class="chat-representante"><span class="icon-chat-1"></span><a href="chat-representante.html">Fale com o seu representante</a></li>--}}
            </ul>
        </div>
        <div class="content">
            <div class="wrapper small">
                <ul class="mobile-only area-cliente-atalhos">
                    <li class="minha-conta" data-aos="fade-up" data-aos-delay="0"><a href="{{ route('account') }}"><span class="icon-cliente-1"></span>Minha Conta</a></li>
                    <li class="minhas-filiais" data-aos="fade-up" data-aos-delay="100"><a href="@role('customer') {{ route('branchs') }} @elserole('seller') {{ route('portfolio') }} @endrole"><span class="icon-filiais-1"></span>@role('customer') Minhas Filiais @elserole('seller') Carteira de Clientes @endrole</a></li>
                    <li class="meus-pedidos" data-aos="fade-up" data-aos-delay="200"><a href="{{ route('orders') }}"><span class="icon-pedidos-1"></span>Meus Pedidos</a></li>
{{--                    <li class="chat-representante" data-aos="fade-up" data-aos-delay="300"><a href="chat-representante.html" ><span class="icon-chat-1"></span>Fale com o seu representante</a></li>--}}
                </ul>
                <div class="page-title">
                    <h1>Minha Conta</h1>
                </div>
                <div class="page-content">
                    <form id="profile-user">
                        <ol class="col-group">
                            <li class="col-2">
                                <label for="cnpj">CPNJ</label>
                                <input type="text" id="cnpj" placeholder="00.000.000/0001-00" value="{{ $profile_user->cgc }}" disabled>
                                <span class="msg-erro">Campo Obrigatório</span>
                            </li>
                            <li class="col-2">
                                <label for="cpf">CPF</label>
                                <input type="text" id="cpf" placeholder="0" value="{{ $profile_user->cpf }}" disabled>
                                <span class="msg-erro">Campo Obrigatório</span>
                            </li>
                            <li class="col-1">
                                <label for="nome">Nome</label>
                                <input type="text" id="nome" value="{{ $profile_user->nome }}" disabled>
                                <span class="msg-erro">Campo Obrigatório</span>
                            </li>
                            <li class="col-2">
                                <label for="codigo">Código</label>
                                <input type="text" id="codigo" value="{{ $profile_user->codusur }}" disabled>
                                <span class="msg-erro">E-mail inválido</span>
                            </li>
                            <li class="col-2">
                                <label for="email-contato">E-mail Contato</label>
                                <input type="email" id="email-contato" value="{{ $profile_user->email }}" disabled>
                                <span class="msg-erro">E-mail inválido</span>
                            </li>
{{--                            <li class="col-5">--}}
{{--                                <button type="submit" name="enviar" class="bt-principal alignright">Enviar</button>--}}
{{--                            </li>--}}
                        </ol>
                    </form>

                    <hr/>

                    <form action="{{ route('account.password') }}" method="post">
                        @csrf
                        <ol class="col-group">
                            <li class="col-1">
                                <h3>Alterar Senha</h3>
                            </li>
                            <li class="col-3 @error('current_password') erro @enderror">
                                <label for="senha-atual" >Senha Atual *</label>
                                <input type="password" name="current_password" id="senha-atual" placeholder="Senha Atual">
                                @error('current_password')
                                    <span class="msg-erro">{{ $message }}</span>
                                @enderror
                            </li>
                            <li class="col-3 @error('password') erro @enderror">
                                <label for="nova-senha" >Nova Senha *</label>
                                <input type="password" name="password" id="nova-senha" placeholder="Nova Senha">
                                @error('password')
                                    <span class="msg-erro">{{ $message }}</span>
                                @enderror
                            </li>
                            <li class="col-3 @error('password') erro @enderror">
                                <label for="confirme-nova-senha" >Confirme a Nova Senha *</label>
                                <input type="password" name="password_confirmation" id="confirme-nova-senha" placeholder="Confirme a Nova Senha">
                            </li>
                            <li class="col-4-5">
                                <small>* Campos Obrigatórios</small>
                            </li>
                            <li class="col-5">
                                <button type="submit" class="bt-principal alignright">Enviar</button>
                            </li>
                        </ol>
                    </form>

                </div>
            </div>

            @if($config['fullbanner_account'])
                <div class="fullbanner">
                    <a href="#"><img src="{{ asset('/images/banners/'.$config['banner_fix']->bannerImg->img) }}"/></a>
                </div>
            @endif

        </div>
    </div>
@endsection

@section('js')
    <script src="{{ mix('/js/account.js') }}"></script>
    @if(session('success'))
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Sucesso!',
                text: '{{ session('success')['msg'] }}',
                showConfirmButton: true
            })
        </script>
    @endif
@endsection
