@extends('layouts.app')

@section('content')
    <div class="page-container ">
        <div class="wrapper tiny">
            <div class="page-title">
                <h1>Seu pedido foi efetuado com sucesso!</h1>
            </div>
            <div class="page-content">
                <h3>{{ $number_order }}</h3>
                <p>Obrigado por escolher a <strong>Exfar Distribuidora</strong>.</p>
                <p>Em breve estaremos enviando seu pedido para processamento.</p>
                <p> </p>
                <p>Acompanhe seu pedidos em: <a href="{{ route('orders') }}">Meus pedidos</a>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection