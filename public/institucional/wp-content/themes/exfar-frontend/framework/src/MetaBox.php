<?php
/**
 * Meta Box
 *
 * @author
 */

class MetaBox {
	/**
	 * Identificador da MetaBox
	 * @var string
	 */
	private $id;

	/**
	 * Título da MetaBox
	 * @var string
	 */
	private $title;

	/**
	 * Contexto da MetaBox ('normal', 'advanced' or 'side')
	 * @var string
	 */
	private $context;

	/**
	 * Prioridade da MetaBox ('high', 'core', 'default' or 'low')
	 * @var string
	 */
	private $priority;

	/**
	 * Evento de carregamento da MetaBox
	 * @var function
	 */
	public $onLoad = null;

	/**
	 * Evento de salvamento da MetaBox
	 * @var function
	 */
	public $onSave = null;

	private $fields = array();

	public function __construct($id, $title, $context, $priority)
	{
		$this->id = $id;
		$this->title = $title;
		$this->context = $context;
		$this->priority = $priority;
	}

	/**
	 * Função callback que é chamada pelo wordpress
	 * @param object $post
	 * @return void
	 */
	public function load($post)
	{
		$value = get_post_meta($post->ID, $this->id, true);

		if (!is_null($this->onLoad))
		{
			return call_user_func($this->onLoad, $value, $this->id, $post);
		}
		else
		{
			if (count($this->fields) == 0)
			{
				echo '<p><input type="text" name="' . $this->id . '" id="' . $this->id . '" value="' . $value . '" style="width: 100%; height: 50px;"></p>';
			}
			else
			{
				foreach ($this->fields as $field)
				{
					$field->load($post->ID);
				}
			}
		}
	}

	/**
	 * Função que chama o hook do wordpress para criar uma MetaBox
	 * @param string $post_type
	 * @return void
	 */
	public function add($post_type)
	{
		add_meta_box(
			$this->id,
			$this->title,
			array(&$this, 'load'),
			$post_type,
			$this->context,
			$this->priority
		);
	}

	/**
	 * Função que é chamada quando é salvo o custom post type
	 * @param int $post_id
	 * @return void
	 */
	public function save($post_id)
	{
		if (!is_null($this->onSave))
		{
			call_user_func($this->onSave, $_POST[$this->id], $this->id, $post_id);
		}
		else
		{
			if (count($this->fields) == 0)
			{
				update_post_meta($post_id, $this->id, $_POST[$this->id]);
			}
			else
			{
				foreach ($this->fields as $field)
				{
					update_post_meta($post_id, $field->id, $_POST[$field->id]);
				}
			}
		}
	}

	public function addField($id, $name, $field, $attrs = null, $values = null, $type = null)
	{
		$this->fields[$id] = new Field($id, $name, $field, $attrs, $values, $type);
		return $this;
	}

	/**
	 * Função para tornar metabox uma galeria
	 * @param boolean $multiple
	 */
	public function gallery($multiple = true)
	{
		$this->onLoad = function($value, $id, $post) use ($multiple)
		{
    		$upload = get_post_meta( $post->ID, $id, true );
    		$array_upload = array();

    		if ($upload !== '') {
    			$array_upload = explode(',', $upload);
    		}
	?>

		<input type="hidden" name="<?php echo $id;?>" id="ids-<?php echo $id;?>" value="<?php echo $upload;?>">
		<button type="button" class="button button-primary" id="btn-select-<?php echo $id;?>">Escolher imagens</button>
		<hr>
		<div id="container-upload-<?php echo $id;?>">
			<?php
			foreach ($array_upload as $id_upload) :
				$icon = wp_get_attachment_thumb_url( $id_upload );
			?>

			<div class="upload-<?php echo $id;?>" data-id="<?php echo $id_upload;?>" style="width: 130px; margin-right: 20px; margin-bottom: 20px; display: inline-block">
				<div style="width: 130px; height: 130px; line-height: 130px; text-align: center">
					<img src="<?php echo $icon;?>" style="max-width: 130px; max-height: 130px; vertical-align: middle">
				</div>
				<hr>
				<button type="button" class="button remove-upload">Remover imagem</button>
			</div>

			<?php endforeach;?>
		</div>

		<script>
		jQuery(document).ready(function ($) {
			multipleUpload('<?php echo $id;?>', 'gallery', <?php echo $multiple;?>);
	    });
		</script>

	<?php
		};
	}

	/**
	 * Função para tornar metabox uma galeria
	 * @param boolean $multiple
	 */
	public function attachment($multiple = true)
	{
		$this->onLoad = function($value, $id, $post) use ($multiple)
		{
    		$upload = get_post_meta( $post->ID, $id, true );
    		$array_upload = array();

    		if ($upload !== '')
    			$array_upload = explode(',', $upload);
	?>

		<input type="hidden" name="<?php echo $id;?>" id="ids-<?php echo $id;?>" value="<?php echo $upload;?>">
		<button type="button" class="button button-primary" id="btn-select-<?php echo $id;?>">Escolher arquivos</button>
		<hr>
		<div id="container-upload-<?php echo $id;?>">
			<?php
			foreach ($array_upload as $id_upload) :
				$type = get_post_mime_type($upload);
				$icon = wp_mime_type_icon($type);
				$title = basename(get_attached_file($id_upload));
			?>

			<div class="upload-<?php echo $id;?>" data-id="<?php echo $id_upload;?>" style="width: 130px; margin-right: 20px; margin-bottom: 20px; display: inline-block">
				<div style="width: 130px; height: 130px; line-height: 130px; text-align: center">
					<img src="<?php echo $icon;?>" style="max-width: 130px; max-height: 130px; vertical-align: middle">
				</div>
				<hr>
				<p style="width: 100%; word-wrap: break-word"><?php echo $title;?></p>
				<button type="button" class="button remove-upload">Remover arquivo</button>
			</div>

			<?php endforeach;?>
		</div>

		<script>
		jQuery(document).ready(function ($) {
			multipleUpload('<?php echo $id;?>', 'attachment', <?php echo $multiple;?>);
	    });
		</script>

	<?php
		};
	}
}
