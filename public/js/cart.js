$(document).ready(() => {
    Cart.list(1,false)

    $('div.wrappper-cart.items form#order').on('submit', (e) => {
        e.preventDefault()
    })

    $('div.wrappper-cart a.bt-principal').on('click', (e) => {
        if($(e.target).hasClass('bt-principal')) {
            let qt = $('table.tabela-produtos.carrinho').find('tfoot tr td span.qt').text()
            if(parseInt(qt) > 0) {
                $('form#order').unbind('submit').submit()
            }
        }
        else {
            e.preventDefault();
        }
    })

    $('div.produtos-content div.wrappper-cart table#cart-items').on('click', (e) => {
        let old_qtd = $(e.target).parent().find('input.qtd').val()
        if($(e.target).hasClass('minus')) {
            if(parseInt(old_qtd) > 0) {
                $(e.target).parent().find('input.qtd').val(parseInt(old_qtd) - 1)
                Cart.cart($(e.target).parent().find('input.qtd'))
            }
        }
        else if($(e.target).hasClass('plus')) {
            $(e.target).parent().find('input.qtd').val(parseInt(old_qtd) + 1)
            Cart.cart($(e.target).parent().find('input.qtd'))
        }
        else if($(e.target).hasClass('del')) {
            let input = $(e.target).parent().parent().find('input.qtd')
            input.val(0)
            Cart.cart(input)
        }

    })

})

const Cart = {
    list: (page, loader) => {
        $.ajax({
            url: '/cart/list',
            method: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                page: page
            },
            beforeSend: () => {
                if(loader) {
                    $('div.produtos-content div.wrappper-cart.items').prepend(`
                        <div class="loading"></div>
                        <div class="loader-container" style="position: absolute; z-index: 999999; width: 100%; margin: 2% auto;text-align: center;">
                            <div class="loader" style="text-align: center; height: 100px; line-height: 140px">
                                <svg width="48" height="48" viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg" version="1.1">
                                    <path d="M 150,0 a 150,150 0 0,1 106.066,256.066 l -35.355,-35.355 a -100,-100 0 0,0 -70.711,-170.711 z" fill="#0071bb">
                                        <animateTransform attributeName="transform" attributeType="XML" type="rotate" from="0 150 150" to="360 150 150" begin="0s" dur=".5s" fill="freeze" repeatCount="indefinite"></animateTransform>
                                    </path>
                                </svg>
                            </div>
                            <h4>Aguarde um instante...</h4>
                        </div>
                    `)
                }
            },
            complete: () => {
                if(loader) {
                    $('div.produtos-content div.wrappper-cart div.loading').remove()
                    $('div.produtos-content div.wrappper-cart div.loader-container').remove()
                }
            },
            success: (res) => {
                if(res.empty) {
                    $('div.produtos-content div.wrappper-cart table#cart-items thead').addClass('empty')
                    $('div.produtos-content div.wrappper-cart table#cart-items tfoot').addClass('empty')
                    $('div.produtos-tools.wrappper-cart.afterclear a.bt-principal').attr('disabled', true).addClass('disabled').removeAttr('href')
                }
                $('div.produtos-content div.wrappper-cart table#cart-items tbody').remove()
                $('div.produtos-content div.wrappper-cart table#cart-items tfoot').remove()
                $('div.produtos-content div.wrappper-cart table#cart-items').append(res.html)
                $('input.qtd').mask('000000')
                // listenner
                $('div.produtos-content div.wrappper-cart table#cart-items input.qtd').on('focus', (e) => {
                    if($(e.target).val() === 0) {
                        $(e.target).val('')
                    }
                })

                $('div.produtos-content div.wrappper-cart table#cart-items input.qtd').on('keyup', (e) => {
                    e.preventDefault()
                    if(e.keyCode === 13 || e.keyCode === 9) {
                        Cart.cart(e.target)
                    }
                })

            },
            error: (err) => {
                console.log(err)
                return false
            }
        })
    },
    cart: (e) => {
        $.ajax({
            url: '/cart/store',
            method: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id': $(e).attr('data-id'),
                'q': $(e).val()
            },
            success: (res) => {
                $(e).parent().parent().parent().find('td').eq(-3).text('').text(res.product_price)
                $('div.produtos-content div.wrappper-cart table#cart-items').find('tfoot tr td small span').text('').text(' ' + res.subtotal)

                $('table.tabela-produtos.carrinho tbody').remove()
                $('table.tabela-produtos.carrinho').append(res.cart_items_html)
                $('table.tabela-produtos.carrinho').find('tfoot tr td span.preco strong').text('').text(' '+res.subtotal)
                $('table.tabela-produtos.carrinho').find('tfoot tr td span.qt').text('').text(' '+res.qt)
                $('.mobile-menu-control.ferramentas-topo li.carrinho span.icon-carrinho-1 span.alert').text('').text(' '+res.qt)
                $('li.carrinho a.cart-info span.icon-carrinho-1 span.alert').text('').text(' '+res.qt)
                $('li.carrinho a.cart-info span.texto span.preco strong').text('').text(' '+res.subtotal)
                $('table.tabela-produtos.carrinho tbody tr td span.del').on('click', (e) => {
                    let input = $(e.target).parent().parent().find('input.qtd')
                    input.val(0)
                    Cart.cart(input)
                })
                $('table.tabela-produtos.carrinho tbody input.qtd').on('change', (e) => {
                    Cart.cart($(e.target))
                })
                Cart.list(1, true)
            },
            error: (error) => {
                console.log(error)
            }
        })
    },
}