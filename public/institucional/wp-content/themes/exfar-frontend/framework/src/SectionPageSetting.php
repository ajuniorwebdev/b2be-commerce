<?php
/**
 * Section Page Setting
 *
 * @author
 */

class SectionPageSetting {
	/**
     * ID da page setting
     * @var string
     */
    private $id;

    /**
     * Nome da page setting
     * @var string
     */
    private $name;

    /**
     * Page id
     * @var string
     */
    private $page_id;

    private $fields = array();

    public function __construct($id, $name, $page_id)
    {
    	$this->id = $id;
    	$this->name = $name;
    	$this->page_id = $page_id;
    }

    public function add()
    {
    	add_settings_section($this->id, $this->name, null, $this->page_id);

    	foreach ($this->fields as $field)
    	{
    		$field->add();
    	}
    }

    public function addField($id, $name, $type = 'text')
    {
    	$this->fields[$id] = new FieldPageSetting($id, $name, null, $this->page_id, $this->id);
    	$this->fields[$id]->option('type', $type);

    	return $this->fields[$id];
    }
}
