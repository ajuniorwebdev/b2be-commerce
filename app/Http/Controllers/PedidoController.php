<?php

namespace App\Http\Controllers;

use App\Client;
use App\User;
use App\Cart;
use App\Pedido;
use App\PedidoItem;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PedidoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $token
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($token)
    {
        $codcli = Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->first()->codcli;
        if(md5($codcli) === $token) {
            return view('orders.process', ['number_order' => session('success')['number_order']]);
        }
        return view('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {

            // inserir cabeçalho do pedido
            $cart = Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->first();
            $bbs = 'N';
            $seller = null;
            $user = User::where(['id' => Auth::id()])->first();
            if($user->hasRole('seller')) {
                $bbs = 'S';
                $seller = $user->codusur;
            }

            $data_order = [
                'user_id' => $cart->user_id,
                'codcli' => $cart->codcli,
                'number_order' => 'PV-'.$cart->codcli . str_pad($cart->id, 8, 0, STR_PAD_LEFT),
                'option_payments_id' => 1,
                'codplpag' => $cart->codplpag,
                'buy_by_seller' => $bbs,
                'codusur' => $seller,
                'order_status_id' => 2,
                'items_qt' => $cart->items_qt,
                'total' => $cart->subtotal,
                'created_at' => now(),
                'updated_at' => null
            ];
            if($pedido = Pedido::create($data_order)) {
                // inserir itens do pedido
                $sql = "insert into pedidos_items(
                            pedidos_id, 
                            codprod, 
                            ean, 
                            description, 
                            qt, 
                            price, 
                            tax, 
                            discount, 
                            price_tax, 
                            price_discount, 
                            price_discount_tax, 
                            total_price, 
                            total_tax, 
                            total_price_tax, 
                            total_price_discount, 
                            total_price_discount_tax, 
                            total,
                            created_at
                        )
                        select 
                            {$pedido->id}, 
                            product_id,
                            product_ean,
                            product_description,
                            qt,
                            price,
                            ifnull(tax,0),
                            ifnull(discount,0),
                            ifnull((price + (price * (tax/100))),0),
                            ifnull((price - (price * (discount/100))),0),
                            ifnull((price - (price * (discount/100)) + ((price - (price * (discount/100))) * (tax/100))),0),
                            ifnull((price * qt),0),
                            ifnull(((price * qt) * (tax/100)),0),
                            ifnull(((price * qt) + ((price * qt) * (tax/100))),0),
                            ifnull(((price - (price * (discount/100))) * qt),0),
                            ifnull((((price - (price * (discount/100))) * qt) + (((price - (price * (discount/100))) * qt) * (tax/100))),0),
                            ifnull((((price - (price * (discount/100))) * qt) + (((price - (price * (discount/100))) * qt) * (tax/100))),0),
                            now()
                        from cart_items
                        where cart_id = {$cart->id}
                    ";
                if(DB::statement($sql)) {
                    // marcar carrinho como enviado
                    if(Cart::where(['id' => $cart->id])
                        ->update([
                            'dt_sended' => now()
                        ])) {

                        // criar outro carrinho
                        $data = [
                            'user_id' => $cart->user_id,
                            'codcli' => $cart->codcli,
                            'codplpag' => $cart->codplpag,
                            'items_qt' => 0,
                            'subtotal' => 0.00,
                            'created_at' => now()
                        ];

                        if($cartn = Cart::create($data)) {
                            // enviar usuario para tela de confirmação
                            return response()
                                ->redirectToRoute('order.create', ['token' => md5($cart->codcli)])
                                ->with('success', ['number_order' => $pedido->number_order]);
                        }
                    }
                }
            }
            throw new Exception('Ocorreu um erro ao criar seu pedido, tente novamente mais tarde!');
        }
        catch(Exception $e) {
            report($e);

            if(!empty($pedido) && $pedido->id) {
                // deletar itens criados com base no id
                DB::statement("delete from pedidos_items where pedidos_id = {$pedido->id}");
                // deletar pedido criado com base no id
                Pedido::destroy($pedido->id);
                // deletar carrinho criado com base no id
            }

            if(!empty($cartn) && $cartn->id) {
                Cart::destroy($cartn->id);
            }
            // marcar carrinho anterior como não lido
            Cart::where(['id' => $cart->id])->update(['dt_sended' => null]);

            return response()
                ->redirectToRoute('cart')
                ->with('error', ['msg' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
