var swiperBannerMain = new Swiper('.template-banner-main', {
    loop: true,
    autoplay: {
        delay: 5000,
        disableOnInteraction: false,
    },
});

var swiperLogos = new Swiper('.template-logos', {
    slidesPerView: 5,
    spaceBetween: 30,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    breakpoints: {
        998: {
            slidesPerView: 2
        },
        768: {
            slidesPerView: 1
        }
    }

});

var swiper = new Swiper('.blog-slider', {
    spaceBetween: 30,
    effect: 'fade',
    loop: true,
    mousewheel: {
      invert: false,
    },
    // autoHeight: true,
    pagination: {
      el: '.blog-slider__pagination',
      clickable: true,
    },
    autoplay: {
        delay: 4000,
        disableOnInteraction: false,
    },
  });

// Mascara para o formulario orçamento (data e telefone)
$(document).ready(function(){
  $('.phone_with_ddd').mask('(00) 00000-0000');
  $('.cnpj').mask('00.000.000/0000-00');
});


window.sr = ScrollReveal();
sr.reveal('.anima', { duration: 600, origin: 'bottom', distance: '60px', easing: 'ease', scale: 0, delay: 300,  reset: true });
sr.reveal('.animaright', { duration: 600, origin: 'right', distance: '60px', easing: 'ease', scale: 0, delay: 300,  reset: true });
sr.reveal('.animaleft', { duration: 600, origin: 'left', distance: '60px', easing: 'ease', scale: 0, delay: 300,  reset: true });
