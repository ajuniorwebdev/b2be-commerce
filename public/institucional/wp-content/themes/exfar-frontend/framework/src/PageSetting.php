<?php
/**
 * Page Setting
 *
 * @author
 */

class PageSetting {
    /**
     * ID da page setting
     * @var string
     */
    private $id;

    /**
     * Nome da page setting
     * @var string
     */
    private $name;

    /**
     * Menu onde irá ser incluída a página
     * @var string
     */
    private $page;

    /**
     * Descrição da página
     * @var string
     */
    private $description;

    private $sections = array();

    public function __construct($id, $name, $page)
    {
        $this->id = $id;
        $this->name = $name;
        $this->page = $page;
    }

    public function add()
    {
        add_action('admin_menu', array( &$this, 'addPage' ));
        add_action('admin_init', array( &$this, 'registerSettings' ));
    }

    public function load()
    {
        echo '<div class="wrap">'.
            '<h2>' . $this->name . '</h2>'.
            (!empty($this->description) ? '<p>' . $this->description . '</p>' : '').
            '<form method="post" action="options.php" novalidate="novalidate">';
                settings_fields($this->id);
                do_settings_sections($this->id);
        echo        '<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Salvar"></p>'.
                '</form>'.
            '</div>';
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function addPage()
    {
        add_submenu_page(
            $this->page,
            $this->name,
            $this->name,
            'manage_options',
            $this->id,
            array( &$this, 'load' )
        );
    }

    public function registerSettings()
    {
        register_setting($this->id, $this->id, array( &$this, 'validate' ));

        foreach ($this->sections as $section)
        {
            $section->add();
        }
    }

    public function addSection($id, $name)
    {
        $this->sections[$id] = new SectionPageSetting($id, $name, $this->id);
        return $this->sections[$id];
    }

    public function validate($input)
    {
        return $input;
    }
}
