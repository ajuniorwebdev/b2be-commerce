<?php

use Illuminate\Support\Facades\Route;

Auth::routes();

/*Login's route*/
Route::post('/password/request/emails', 'Auth\ForgotPasswordController@getLoginEmailList')->name('password.request.emails');
Route::get('/client/request', 'Auth\RegisterController@showClientRequest')->name('client.request.register');
Route::post('/client/request', 'Auth\RegisterController@sendClientInformation')->name('client.request.send');
Route::post('/client/request/files', 'Auth\RegisterController@uploadFiles');
Route::get('/client/request/{token}/success', 'Auth\RegisterController@clientRequestSuccess')->name('client.request.success');

Route::get('/', 'HomeController@index')->name('/');
Route::post('/newsletter/store', 'HomeController@newsletterStore')->name('newsletter.store');

/* Banners */
Route::post('/banners/action', 'HomeController@bannersAction')->name('banners.action');

Route::group(['middleware' => 'auth'], function() {

    /* Products */
    Route::get('/produtos/{list}', 'ProductController@products')->name('products');
    Route::post('/products/{list}', 'ProductController@productsList')->name('products.list');

    /* Account */
    Route::get('/visao-geral', 'AccountController@index')->name('overview');
    Route::get('/minha-conta', 'AccountController@account')->name('account');
    Route::post('/account/password', 'AccountController@passwordChange')->name('account.password');
    
    Route::get('/minhas-filiais', 'AccountController@branchs')->name('branchs');
    Route::post('/branchs/list', 'BranchController@list')->name('branchs.list');
    Route::post('/branchs/store', 'BranchController@store')->name('branchs.store');
    Route::post('/branchs/change', 'BranchController@update')->name('branchs.update');
    
    Route::get('/meus-pedidos', 'AccountController@orders')->name('orders');
    Route::get('/meus-pedidos/detalhes/{number_order}', 'AccountController@ordersDetails')->name('orders.details');
    Route::post('/orders/list', 'AccountController@ordersList')->name('orders.list');
    Route::post('/orders/installments/{number_order}', 'AccountController@ordersInstallments')->name('orders.installments');

    Route::get('/carteira-clientes', 'AccountController@portfolio')->name('portfolio');
    Route::post('/portfolio/change', 'AccountController@portfolioChange')->name('portfolio.change');
    Route::post('/portfolio/list', 'AccountController@portfolioList')->name('portfolio.list');

    /* Cart */
    Route::get('/carrinho', 'CartController@index')->name('cart');
    Route::post('/cart/store', 'CartController@store')->name('cart.store');
    Route::post('/cart/list', 'CartController@cartList')->name('cart.list');

    /* Pedidos */
    Route::get('/pedidos/{token}/processado', 'PedidoController@create')->name('order.create');
    Route::post('/order/store', 'PedidoController@store')->name('order.store');

    /* Prazo */
    Route::post('/prazo/change', 'PaymentPlanController@change')->name('prazo.change');
});


