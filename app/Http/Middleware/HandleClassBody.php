<?php

namespace App\Http\Middleware;

use Closure;
use App\Banner;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class HandleClassBody
{
    public $attributes;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $url_path_info = explode(config()['app']['url'], url()->current());
        $config = [];

        $banners_fix = Banner::with(['status', 'bannerImg', 'bannerTipo'])
            ->where(['status_id' => 1, 'banner_hierarchy' => 3])
            ->orderBy('position', 'asc')
            ->first();

        $pedidos_detalhes = explode('/',end($url_path_info));
        if(isset($pedidos_detalhes[2]) && $pedidos_detalhes[2] == 'detalhes') {
            $number_order = $pedidos_detalhes[3];
            $url_path_info = ['/' . $pedidos_detalhes[1] . '/' . $pedidos_detalhes[2]];
        }

        switch (end($url_path_info)) {
            case '/login':
                $config['title'] = 'Login | Exfar Distribuidora - Portal de Vendas';
                $config['body_class'] = 'home visitante login';
                $config['midia'] = false;
                $config['fullbanner'] = false;
                $config['banner_fix'] = $banners_fix;
                break;
            case '/register':
                $config['title'] = 'Primeiro Acesso | Exfar Distribuidora - Portal de Vendas';
                $config['body_class'] = 'home visitante login';
                $config['midia'] = false;
                $config['fullbanner'] = false;
                $config['banner_fix'] = $banners_fix;
                break;
            case '/password/reset':
                $config['title'] = 'Esqueci minha senha | Exfar Distribuidora - Portal de Vendas';
                $config['body_class'] = 'home visitante login';
                $config['midia'] = false;
                $config['fullbanner'] = false;
                $config['banner_fix'] = $banners_fix;
                break;
            case '/client/request':
                $config['title'] = 'Solicite o Cadastro | Exfar Distribuidora - Portal de Vendas';
                $config['body_class'] = 'home visitante solicitar-cadastro';
                $config['midia'] = false;
                $config['fullbanner'] = false;
                $config['banner_fix'] = $banners_fix;
                break;
            case '':

                if(Auth::check()) {
                    $config['body_class'] = 'home';
                }
                else {
                    $config['body_class'] = 'home visitante';
                }
                $config['title'] = 'Exfar Distribuidora - Portal de Vendas';
                $config['midia'] = true;
                $config['fullbanner'] = true;
                $config['banner_fix'] = $banners_fix;
                break;
            case '/proudutos/az':
                $config['title'] = 'Produtos A-Z | Exfar Distribuidora - Portal de Vendas';
                $config['body_class'] = 'archive-produtos';
                $config['midia'] = true;
                $config['fullbanner'] = true;
                $config['banner_fix'] = $banners_fix;
                break;
            case '/visao-geral':
                $config['title'] = 'Visão Geral | Exfar Distribuidora - Portal de Vendas';
                $config['body_class'] = 'area-cliente';
                $config['midia'] = false;
                $config['fullbanner'] = true;
                $config['fullbanner_account'] = false;
                $config['banner_fix'] = $banners_fix;
                break;
            case '/minha-conta':
                $config['title'] = 'Minha Conta | Exfar Distribuidora - Portal de Vendas';
                $config['body_class'] = 'area-cliente';
                $config['midia'] = false;
                $config['fullbanner'] = true;
                $config['fullbanner_account'] = false;
                $config['banner_fix'] = $banners_fix;
                break;
            case '/minhas-filiais':
                $config['title'] = 'Minhas Filiais | Exfar Distribuidora - Portal de Vendas';
                $config['body_class'] = 'area-cliente filiais';
                $config['midia'] = false;
                $config['fullbanner'] = true;
                $config['fullbanner_account'] = false;
                $config['banner_fix'] = $banners_fix;
                break;
            case '/carteira-clientes':
                $config['title'] = 'Carteira de Clientes | Exfar Distribuidora - Portal de Vendas';
                $config['body_class'] = 'area-cliente filiais';
                $config['midia'] = false;
                $config['fullbanner'] = true;
                $config['fullbanner_account'] = false;
                $config['banner_fix'] = $banners_fix;
                break;
            case '/meus-pedidos':
                $config['title'] = 'Meus Pedidos | Exfar Distribuidora - Portal de Vendas';
                $config['body_class'] = 'area-cliente meus-pedidos';
                $config['midia'] = false;
                $config['fullbanner'] = true;
                $config['fullbanner_account'] = false;
                $config['banner_fix'] = $banners_fix;
                break;
            case '/meus-pedidos/detalhes':
                $config['title'] = 'Pedido Nº: '.$number_order.' | Exfar Distribuidora - Portal de Vendas';
                $config['body_class'] = 'area-cliente meus-pedidos';
                $config['midia'] = false;
                $config['fullbanner'] = false;
                $config['fullbanner_account'] = false;
                $config['banner_fix'] = $banners_fix;
                break;
            case '/carrinho':
                $config['title'] = 'Meu Carrinho | Exfar Distribuidora - Portal de Vendas';
                $config['body_class'] = 'carrinho';
                $config['midia'] = true;
                $config['fullbanner'] = true;
                $config['banner_fix'] = $banners_fix;
                break;
            default:
                $config['body_class'] = 'home visitante';
                $config['title'] = 'Exfar Distribuidora - Portal de Vendas';
                $config['midia'] = true;
                $config['fullbanner'] = true;
                $config['banner_fix'] = $banners_fix;
                break;
        }

        View::share('config', $config);

        return $next($request);
    }
}
