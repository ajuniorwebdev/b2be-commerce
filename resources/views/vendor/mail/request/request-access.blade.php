<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Solicitação de Cadastro E-Commerce</title>
</head>
<body>
<style>

  @media only screen and (max-width: 600px) {
    .inner-body {
      width: 100% !important;
    }

    .footer {
      width: 100% !important;
    }
  }

  @media only screen and (max-width: 500px) {
    .button {
      width: 100% !important;
    }
  }
</style>

<table class="wrapper" width="100%" cellpadding="0" cellspacing="0" role="presentation">
    <tr>
        <td align="center">
            <table class="content" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                <tr>
                    <td class="header">
                        <h3>Solicitação de Cadastro E-Commerce Exfar</h3>
                    </td>
                </tr>

                <!-- Email Body -->
                <tr>
                    <td class="body" width="100%" cellpadding="0" cellspacing="0">
                        <table class="inner-body" align="left" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                            <!-- Body content -->
                            <tr><td class="content-cell"><strong>CNPJ:</strong> {{ $cnpj }}</td></tr>
                            <tr><td class="content-cell"><strong>I.E:</strong> {{ $ie }}</td></tr>
                            <tr><td class="content-cell"><strong>E-mail:</strong> {{ $email_contato }}</td></tr>
                            <tr><td class="content-cell"><strong>Celular:</strong> {{ $telefone_celular }}</td></tr>
                            <tr><td class="content-cell"><strong>Razão Social:</strong>{{ $razao_social }}</td></tr>
                            <tr><td class="content-cell"><strong>Fantasia:</strong>{{ $fantasia }}</td></tr>
                            <tr><td class="content-cell"><strong>Rede:</strong>{{ $rede }}</td></tr>
                            <tr><td class="content-cell"><strong>Email Cobrança:</strong>{{ $email_cobranca }}</td></tr>
                            <tr><td class="content-cell"><strong>Email XML:</strong>{{ $email_xml }}</td></tr>
                            <tr><td class="content-cell"><strong>Telefone Fixo:</strong>{{ $telefone_fixo }}</td></tr>
                            <tr><td class="content-cell"><strong>Nome Prop.:</strong>{{ $nome }}</td></tr>
                            <tr><td class="content-cell"><strong>CPF Prop.:</strong>{{ $cpf }}</td></tr>
                            <tr><td class="content-cell"><strong>E-mail Prop.:</strong>{{ $email }}</td></tr>
                            <tr><td class="content-cell"><strong>Celular Prop.:</strong>{{ $celular }}</td></tr>
                            <tr><td class="content-cell"><strong>Nome Comprador Med.:</strong>{{ $comprador_med_nome }}</td></tr>
                            <tr><td class="content-cell"><strong>E-mail Comprador Med.:</strong>{{ $comprador_med_email }}</td></tr>
                            <tr><td class="content-cell"><strong>Telefone Comprador Med.:</strong>{{ $comprador_med_tel_fixo }}</td></tr>
                            <tr><td class="content-cell"><strong>Celular Comprador Med.:</strong>{{ $comprador_med_tel_celular }}</td></tr>
                            <tr><td class="content-cell"><strong>Nome Comprador Perf.:</strong>{{ $comprador_per_nome }}</td></tr>
                            <tr><td class="content-cell"><strong>E-mail Comprador Perf.:</strong>{{ $comprador_per_email }}</td></tr>
                            <tr><td class="content-cell"><strong>Telefone Comprador Perf.:</strong>{{ $comprador_per_tel_fixo }}</td></tr>
                            <tr><td class="content-cell"><strong>Celular Comprador Perf.:</strong>{{ $comprador_per_tel_celular }}</td></tr>
                            <tr><td class="content-cell"><strong>Limite Sugerido:</strong>{{ $condicao_comercial }}</td></tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
                            <tr>
                                <td class="content-cell" align="center">
                                    Solicitação enviada em: {{ date('d/m/Y H:i:s') }}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>