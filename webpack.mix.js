const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js([
    'resources/js/app.js',
    'resources/js/owlcarousel/owl.carousel.js',
    'resources/js/functions.js',
    'resources/js/plugin/uploader-master/dist/js/jquery.dm-uploader.min.js',
    ], 'public/js/app.js');

mix.styles([
    'resources/js/owlcarousel/assets/owl.carousel.min.css',
    'resources/js/owlcarousel/assets/owl.theme.default.min.css',
    'resources/js/plugin/uploader-master/dist/css/jquery.dm-uploader.min.css',
], 'public/css/app-p.css');

mix.sass('resources/sass/style.scss', 'public/css/style.css');
mix.sass('resources/sass/app.scss', 'public/css/app.css');

mix.scripts([
  'resources/js/modules/main.login.js',
  'resources/js/modules/email.login.js'
], 'public/js/login.js');

mix.scripts([
    'resources/js/modules/client.request.login.js'
], 'public/js/client.request.js');

mix.scripts(['resources/js/main.app.js'], 'public/js/main.js');
mix.scripts('resources/js/modules/main.products.js', 'public/js/products.js');
mix.scripts(['resources/js/modules/main.cart.js'], 'public/js/cart.js');
mix.scripts(['resources/js/modules/account.js'], 'public/js/account.js');
