@extends('layouts.app')

@section('content')
    <div class="page-container">
        <div class="wrapper tiny">
            <div class="login-boxes">
                <div class="login-box">
                    <div class="login">
                        <h1>Esqueci Minha Senha</h1>
                        <form id="verify_info" method="post" action="{{ route('password.request.emails') }}">
                            @csrf
                            <ol class="col-group">
                                <li class="col-1 radio-group">
                                    <input type="radio" name="profile" id="client_profile" value="1" checked>
                                    <label for="client_profile">Cliente</label>
                                    <input type="radio" name="profile" id="seller_profile" value="2">
                                    <label for="seller_profile">Representante</label>
                                </li>
                                <div class="client_form">
                                    <li class="col-1 @error('cnpj') erro @enderror">
                                        <label for="cnpj">CNPJ</label>
                                        <input type="text" id="cnpj" name="cnpj" placeholder="Informe seu CNPJ" autocomplete="off">
                                        @error('cnpj')
                                            <span class="msg-erro">{{ $message }}</span>
                                        @enderror
                                    </li>
                                    <li class="col-1 @error('client_code') erro @enderror">
                                        <label for="client_code">Código</label>
                                        <input type="text" id="client_code" name="client_code" placeholder="Informe seu código de cliente" autocomplete="off">
                                        @error('client_code')
                                            <span class="msg-erro">{{ $message }}</span>
                                        @enderror
                                    </li>
                                </div>
                                <div class="seller_form" style="display:none">
                                    <li class="col-1 @error('seller_code') erro @enderror">
                                        <label for="seller_code">Código</label>
                                        <input type="text" id="seller_code" name="seller_code" placeholder="Informe seu código de representante">
                                        @error('seller_code')
                                            <span class="msg-erro">{{ $message }}</span>
                                        @enderror
                                    </li>
                                </div>
                                <li class="col-1">
                                    <button class="bt-principal alignright" type="submit">Verificar</button>
                                </li>
                            </ol>
                        </form>

                        <form id="login_email_list" method="post" action="{{ route('password.email') }}" style="display: none;">
                            <h4>Selecione um email</h4>

                            @csrf
                            <ol class="col-group">
                                <li class="col-1 radio-group">
                                    @if(session('loginEmailList'))
                                        @foreach(session('loginEmailList') as $list)
                                            <input type="radio" name="email" id="client_list_email" value="{{$list['email']}}" checked>
                                            <label for="client_profile">{{$list['email']}}</label>
                                        @endforeach
                                    @endif
                                </li>
                                <li class="col-1">
                                    <button class="bt-principal alignright" type="submit">Enviar link para resetar senha</button>
                                </li>
                            </ol>
                        </form>

                    </div>
                </div>
                <div class="login-box">
                    <div class="login">
                        <h3>Já possui o acesso?</h3>
                        <p>você já tenha o acesso ao e-commerce <strong>Exfar Distribuidora</strong>? Então basta entrar em nosso sistema</p>
                        <a href="{{ route('login') }}" class="bt-sec">Entrar no Sistema</a>
                    </div>
                    <div class="solicite-acesso">
                        <h3>Solicite o Cadastro</h3>
                        <p>Ainda não é nosso cliente? Solicite a ficha de cadastro para acessar o e-commerce <strong>Exfar Distribuidora</strong></p>
                        <a href="{{ route('client.request.register') }}" class="bt-sec">Solicitar Cadastro</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{mix('js/login.js')}}"></script>
    @if(session('loginEmailList'))
        <script>
            $(document).ready(() => {
                $('form#verify_info').css({display: 'none'})
                $('form#login_email_list').css({display: 'block'})
            })
        </script>
    @endif

    @error('email')
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: '{{ $message }}',
            })
        </script>
    @enderror
@endsection
