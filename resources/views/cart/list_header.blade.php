@if(count($cart_items) > 0)
    <tbody>
    @foreach($cart_items as $item)
        <tr>
            <td width="70" class="imagem">
                <img src="{{ asset('images/produtos/box_default.jpeg') }}" width="50px"/>
            </td>
            <td class="desc-prod">{{ $item->descricao }}</td>
            <td class="col-qtd">
                <div class="qtd">
                    <span class="qtd-btn minus">-</span>
                    <input data-id="{{$item->codprod}}" class="qtd" type="text" name="qtd" value="{{ $item->qt }}" autocomplete="off">
                    <span class="qtd-btn plus">+</span>
                </div>
            </td>
            <td class="col-valor"><span class="preco">R$ {{ number_format($item->subtotal, '2', ',', '.') }}</span></td>
            <td>
                <span class="icon-fechar-small del"></span>
            </td>
        </tr>
    @endforeach
    </tbody>
@else
    <tbody class="empty">
    <tr>
        <td>Carrinho Vazio!</td>
    </tr>
    </tbody>
@endif