@extends('layouts.app')

@section('content')
    <div class="page-container ">
        <div class="wrapper tiny">
            <div class="page-title">
                <h1>Sua solicitação foi enviada com sucesso!</h1>
            </div>
            <div class="page-content">
                <p>Nossa equipe irá validar as informações enviadas e em breve vai entrará em contato.</p>
                <p>Caso prefira, você pode entrar em contato conosco através da nosso SAC: <a href="tel:(85) 3017-9300"><strong>(85) 3017-9300</strong></a></p>
                <p>A <strong>Exfar Distribuidora</strong> agradece a preferência e desejamos a você boas compras!</p>
                <p>&nbsp;</p>
                <p><a href="{{ route('/') }}">← Voltar para a página inicial</a>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection