<tbody>
@foreach($installments as $installment)
    <tr >
        <td>{{ $installment->prest }}</td>
        <td>{{ $installment->dtvenc }}</td>
        <td>{{ $installment->status_boleto }}</td>
        <td class="center">R$ {{ number_format($installment->valor, '2', ',', '.') }}</td>
        <td class="center">{{ $installment->dtpag }}</td>
        <td class="center">
            @if(empty($installment->dtpag))
                <a target="_blank" href="https://winthordanfe.s3.us-east-2.amazonaws.com/BOLETO/{{ $installment->numtransvenda . '-' . $installment->duplic }}.pdf">Imprimir Boleto</a>
            @else
                <a href="javascript:void(0)" class="disabled">Imprimir Boleto</a>
            @endif
        </td>
    </tr>
@endforeach
</tbody>