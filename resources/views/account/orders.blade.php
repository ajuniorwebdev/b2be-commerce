@extends('layouts.app')

@section('content')
    <div class="area-cliente-container">
        <div class="lateral">
            <ul>
                <li class="area-cliente"><a href="{{ route('overview') }}">Visão Geral</a></li>
                <li class="minha-conta"><span class="icon-cliente-1"></span><a href="{{ route('account') }}">Minha Conta</a></li>
                <li class="minhas-filiais"><span class="icon-filiais-1"></span><a href="@role('customer') {{ route('branchs') }} @elserole('seller') {{ route('portfolio') }} @endrole">@role('customer') Minhas Filiais @elserole('seller') Carteira de Clientes @endrole</a></li>
                <li class="meus-pedidos active"><span class="icon-pedidos-1"></span><a href="{{ route('orders') }}">Meus Pedidos</a></li>
{{--                <li class="chat-representante"><span class="icon-chat-1"></span><a href="chat-representante.html">Fale com o seu representante</a></li>--}}
            </ul>
        </div>
        <div id="orders" class="content">
            <div class="wrapper">
                <ul class="mobile-only area-cliente-atalhos">
                    <li class="minha-conta" data-aos="fade-up" data-aos-delay="0"><a href="{{ route('account') }}"><span class="icon-cliente-1"></span>Minha Conta</a></li>
                    <li class="minhas-filiais" data-aos="fade-up" data-aos-delay="100"><a href="@role('customer') {{ route('branchs') }} @elserole('seller') {{ route('portfolio') }} @endrole"><span class="icon-filiais-1"></span>@role('customer') Minhas Filiais @elserole('seller') Carteira de Clientes @endrole</a></li>
                    <li class="meus-pedidos" data-aos="fade-up" data-aos-delay="200"><a href="{{ route('orders') }}"><span class="icon-pedidos-1"></span>Meus Pedidos</a></li>
{{--                    <li class="chat-representante" data-aos="fade-up" data-aos-delay="300"><a href="chat-representante.html" ><span class="icon-chat-1"></span>Fale com o seu representante</a></li>--}}
                </ul>
                <div class="page-title">
                    <h1>Meus Pedidos</h1>
                </div>
                <div class="produtos-tools afterclear fullscreen">
                    <div class="busca-form">
                        <form class="search">
                            <input type="text" name="primary_search" id="busca-topo" placeholder="Pesquise por Nº Pedido, Cód. Cliente, Nº NF...">
                            <button type="submit" class="bt-busca icon-busca"><span>Pesquisar</span></button>
                            <span class="toggle-busca-avancada icon-config"><span class="alert">!</span></span>
                            <div class="busca-avancada">
                                <h5>Busca Avançada</h5>
                                <ol class="col-group">
                                    <li class="col-3">
                                        <label for="numero-pedido">Nº do Pedido</label>
                                        <input type="text" name="numero-pedido">
                                    </li>
                                    <li class="col-3">
                                        <label for="numero-nota">Nº da Nota</label>
                                        <input type="text" name="numero-nota">
                                    </li>
                                    <li class="col-3">
                                        <label for="data-pedido">Data do pedido</label>
                                        <input type="text" name="data-pedido" id="data-pedido" autocomplete="off">
                                    </li>
                                    <li class="col-3">
                                        <label for="origem-venda">Origem da Venda</label>
                                        <input type="text" name="origem-venda">
                                    </li>
                                    <li class="col-1">
                                        <a href="#" class="toggle">Fechar</a>
                                        <button type="submit" class="bt-principal alignright">Pesquisar</button>
                                    </li>
                                </ol>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="page-content">
                    <div class="pedidos afterclear">
                        <div class="table-wrapper">
                            <table>
                                <thead>
                                <tr>
                                    <th>Nº Ped Cli</th>
                                    <th>Nº Ped Distr</th>
                                    <th>Nº Nota</th>
                                    <th>Data do Pedido</th>
                                    <th>Origem da Venda</th>
                                    <th>Filial</th>
                                    <th>Status</th>
                                    <th class="center" width="100">Valor</th>
                                    <th class="center" width="100">Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="12" style="text-align: center;">
                                        <div class="loader" style="text-align: center; height: 100px; line-height: 140px;">
                                            <svg width="48" height="48" viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg" version="1.1">
                                                <path d="M 150,0 a 150,150 0 0,1 106.066,256.066 l -35.355,-35.355 a -100,-100 0 0,0 -70.711,-170.711 z" fill="#0071bb">
                                                    <animateTransform attributeName="transform" attributeType="XML" type="rotate" from="0 150 150" to="360 150 150" begin="0s" dur=".5s" fill="freeze" repeatCount="indefinite"></animateTransform>
                                                </path>
                                            </svg>
                                        </div>
                                        <h4>Aguarde um instante...</h4>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="pagination-container" style="margin-top: 15px;">
                        <ul class="pagination">
                            <li class="prev disabled"><a href="javascript:void(0)"><</a></li>
                            <li class="active"><a href="javascript:void(0)">1</a></li>
                            <li><a href="javascript:void(0)">2</a></li>
                            <li><a href="javascript:void(0)">3</a></li>
                            <li><a href="javascript:void(0)">4</a></li>
                            <li><a href="javascript:void(0)">5</a></li>
                            <li class="next"><a href="javascript:void(0)">></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            @if($config['fullbanner_account'])
                <div class="fullbanner">
                    <a href="#"><img src="{{ asset('/images/banners/'.$config['banner_fix']->bannerImg->img) }}"/></a>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ mix('js/account.js') }}"></script>
    @if(session('success'))
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Sucesso!',
                text: '{{ session('success')['msg'] }}',
                showConfirmButton: true
            })
        </script>
    @endif
@endsection
