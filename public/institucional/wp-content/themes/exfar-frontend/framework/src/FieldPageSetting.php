<?php
/**
 * Field Page Setting
 *
 * @author
 */

class FieldPageSetting {
	/**
     * ID da field page setting
     * @var string
     */
    private $id;

    /**
     * Nome da field page setting
     * @var string
     */
    private $name;

    /**
     * Descrição da field page setting
     * @var string
     */
    private $description;

    private $page_id;
    private $section_id;
    public $opts = array(
    	'type'      => 'text'
	);

    public function __construct($id, $name, $description, $page_id, $section_id)
    {
    	$this->id         	= $id;
    	$this->name       	= $name;
    	$this->page_id    	= $page_id;
    	$this->section_id 	= $section_id;
    	// Opções padrões
    	$this->option('id', $id);
    	$this->option('name', $id);
    	$this->option('label_for', $id);
    	$this->option('desc', $description);
    }

    public function option($name, $value)
    {
    	$this->opts[$name] = $value;
  		return $this;
    }

	public function add()
	{
		add_settings_field(
			$this->id,
			$this->name,
			array( &$this, 'load' ),
			$this->page_id,
			$this->section_id,
			$this->opts
		);
	}

	public function load($args)
	{
		extract($args);

        $options = get_option($this->page_id);

	    switch ($type) {
			case 'text':
				$options[$name] = stripslashes($options[$name]);
				$options[$name] = esc_attr($options[$name]);
				echo '<input class="regular-text code" type="text" id="' . $id . '" name="' . $this->page_id . '[' . $name . ']" value="' . $options[$name] . '" />';
				echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";
			break;

			case 'textarea':
				$options[$name] = stripslashes($options[$name]);
				$options[$name] = esc_attr($options[$name]);
				echo '<textarea id="' . $id . '" name="' . $this->page_id . '[' . $name . ']" cols="50" rows="8">' . $options[$name] . '</textarea>';
				echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";
			break;
	    }
	}
}
