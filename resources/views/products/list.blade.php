<tbody>
@foreach($produtos as $pro)
    <tr class="{{ ($pro->product_id) ? 'hot' : '' }}">
        <td>{{ $pro->descricao_marca }}</td>
        <td>{{ $pro->codauxiliar }}</td>
        <td>{{ $pro->codprod }}</td>
        <td>{{ $pro->descricao_produto }}</td>
        <td class="center">R$ {{ number_format($pro->preco_tabela, '2', ',', '.') }}</td>
        <td class="center hidden-mobile">R${{ number_format(((($pro->preco_tabela - (($pro->preco_tabela * $pro->desconto)/100))/100) * $pro->impostos), '2', ',', '.') }}</td>
        <td class="center hidden-mobile">{{ ($pro->desconto > 0) ? number_format($pro->desconto, '2', ',', '.').'%' : '-' }}</td>
    {{--<td class="center hidden-mobile">R$ {{ number_format($pro->preco_tabela + (($pro->preco_tabela/100) * $pro->impostos), '2', ',', '.') }}</td>--}}
        <td class="center hidden-mobile">
            R$ {{ number_format($pro->preco_tabela - (($pro->preco_tabela * $pro->desconto)/100), '2', ',', '.') }}
        </td>
        <td class="center">
            R$ {{ number_format((($pro->preco_tabela - (($pro->preco_tabela * $pro->desconto)/100)) + ((($pro->preco_tabela - (($pro->preco_tabela * $pro->desconto)/100))/100) * $pro->impostos)), '2', ',', '.') }}
        </td>
        <td class="center">
            <div class="qtd" style="width: 80px">
                <span class="qtd-btn minus">-</span>
                <input data-id="{{$pro->codprod}}" class="qtd" type="text" name="qtd" value="{{ ($pro->qt > 0) ? $pro->qt : 0 }}" autocomplete="off">
                <span class="qtd-btn plus">+</span>
            </div>
        </td>
        <td class="center">R$ {{ ($pro->qt > 0) ? number_format(($pro->preco_tabela - ($pro->preco_tabela * ($pro->desconto/100)) * $pro->qt) + (($pro->preco_tabela - ($pro->preco_tabela * ($pro->desconto/100)) * $pro->qt) * ($pro->impostos/100)), '2', ',', '.') : '0,00'}}</td>
        <td class="center">
            @if($pro->stock_volume == 'C')
                <span class="estoque critico"><span class="icon-estoque-1"></span></span>
            @elseif($pro->stock_volume == 'M')
                <span class="estoque moderado"><span class="icon-estoque-2"></span></span>
            @elseif($pro->stock_volume == 'E')
                <span class="estoque excelente"><span class="icon-estoque-1"></span></span>
            @endif
        </td>
    </tr>
@endforeach
</tbody>
<tfoot>
    <tr>
        <td colspan="12"><small>Total: R$ <span>{{ number_format($subtotal[0]->subtotal, '2', ',', '.') }}</span></small></td>
    </tr>
</tfoot>