<?php
/**
 * Field
 *
 * @author
 */

class Field {
	/**
	 * ID da field
	 * @var string
	 */
	public $id;

	/**
	 * Nome da field
	 * @var string
	 */
	private $name;

	/**
	 * Field (select, input, textarea)
	 * @var string
	 */
	private $field;

	/**
	 * Tipo do field (text, date, email etc)
	 * @var string
	 */
	private $type;

	/**
	 * Atributos da field
	 * @var string
	 */
	private $attrs = array('style' => 'width: 100%');

	/**
	 * Select values
	 * @var string
	 */
	private $values;

	/**
	 * Post ID
	 * @var int
	 */
	public $post_id;

	public function __construct($id, $name, $field, $attrs = null, $values = null, $type = null)
	{
		$this->id = $id;
		$this->name = $name;
		$this->field = $field;
		$this->type = $type;
		if (is_array($attrs)) $this->attrs = array_merge($this->attrs, $attrs);
		$this->values = $values;
	}

	public function load($post_id)
	{
		$this->post_id = $post_id;

		$label = '<label style="display: inline-block; color: #454545; font-weight: bold; margin: 0 0 5px 0;" for="' . $this->id . '">' . $this->name . '</label>';
		$fields = array('image');
		$style = 'style="margin: 0 0 10px 0; padding: 10px; background-color: #f2f2f2;"';
		if (in_array($this->field, $fields)) { $label = null; $style = null;}


		echo 	"<div $style>" .
					$label .
					$this->{$this->field}($this->type) .
				'</div>';
	}

	private function input($type)
	{
		if (is_null($type)) { $type = 'text';}

		$value = get_post_meta($this->post_id, $this->id, true);
		return '<input type="' . $type . '" value="' . $value . '" name="' . $this->id . '" id="' . $this->id . '" ' . $this->attrToHtml() . '>';
	}

	private function datepicker()
	{
		$value = get_post_meta($this->post_id, $this->id, true);
		?>
		<script>
		jQuery(function($){
			$( "#<?=$this->id?>-formated" ).datepicker({
				altField: "#<?=$this->id?>",
				altFormat: "yy-mm-dd",
				changeMonth: true,
				dateFormat: 'dd/mm/yy',
			    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
			    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
			    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
			    nextText: 'Próximo',
			    prevText: 'Anterior',
				numberOfMonths: 1,
			});
		});
		</script>
		<?php

		$date_formatted = date('d/m/Y', strtotime($value));

		$html = '<input type="hidden" value="' . $value . '" name="' . $this->id . '" id="' . $this->id . '">';
		$html .= '<input type="text" value="' . $date_formatted . '" id="' . $this->id . '-formated" ' . $this->attrToHtml() .' >';

		return $html;
	}

	private function rangeFrom()
	{
		$value = get_post_meta($this->post_id, $this->id, true);
		?>
		<script>
		jQuery(function($){
			$( "#date-from" ).datepicker({
				altField: "#<?=$this->id?>",
				altFormat: "yy-mm-dd",
				defaultDate: "+1w",
				changeMonth: true,
				dateFormat: 'dd/mm/yy',
			    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
			    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
			    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
			    nextText: 'Próximo',
			    prevText: 'Anterior',
				numberOfMonths: 1,
				onClose: function( selectedDate ) {
					$( "#date-to" ).datepicker( "option", "minDate", selectedDate );
				}
			});
		});
		</script>
		<?php

		$date_formatted = date('d/m/Y', strtotime($value));

		$html = '<input type="hidden" value="' . $value . '" name="' . $this->id . '" id="' . $this->id . '">';
		$html .= '<input type="text" value="' . $date_formatted . '" id="date-from" ' . $this->attrToHtml() .' >';

		return $html;
	}

	private function rangeTo()
	{
		$value = get_post_meta($this->post_id, $this->id, true);
		?>
		<script>
		jQuery(function($){
			$( "#date-to" ).datepicker({
				altField: "#<?=$this->id?>",
				altFormat: "yy-mm-dd",
				defaultDate: "+1w",
				changeMonth: true,
				dateFormat: 'dd/mm/yy',
			    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
			    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
			    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
			    nextText: 'Próximo',
			    prevText: 'Anterior',
				numberOfMonths: 1,
				onClose: function( selectedDate ) {
					$( "#date-from" ).datepicker( "option", "maxDate", selectedDate );
				}
			});
		});
		</script>
		<?php

		$date_formatted = date('d/m/Y', strtotime($value));

		$html = '<input type="hidden" value="' . $value . '" name="' . $this->id . '" id="' . $this->id . '">';
		$html .= '<input type="text" value="' . $date_formatted . '" id="date-to" ' . $this->attrToHtml() .' >';

		return $html;
	}

	private function textarea()
	{
		$value = get_post_meta($this->post_id, $this->id, true);
		return '<textarea name="' . $this->id . '" id="' . $this->id . '" ' . $this->attrToHtml() . '>' . $value . '</textarea>';
	}

	private function editor()
	{
		$value = get_post_meta($this->post_id, $this->id, true);

		$settings_editor = array(
			'textarea_rows' => 15,
			'teeny' 		=> true,
			'quicktags' 	=> true,
			'wpautop'		=> false
		);

		ob_start();
		wp_editor($value, $this->id, $settings_editor);
		$editor = ob_get_clean();

		$html = '<div>';
		$html .= $editor;
		$html .= '</div>';

		return $html;
	}

	private function select()
	{
		if (!is_array($this->values)) return false;

		$value = get_post_meta($this->post_id, $this->id, true);

		$html = '<select name="' . $this->id . '" id="' . $this->id . '" ' . $this->attrToHtml() . '>';
		foreach ($this->values as $k => $name)
		{
			$chave = (!is_int($k) ? $name : $k);
			$html .= '<option value="' . $chave . '"' . ($value == $chave ? ' SELECTED' : '') . '>' . $name . '</option>';
		}
		$html .= '</select>';

		return $html;
	}

	private function inputMore()
	{
		$value = get_post_meta($this->post_id, $this->id, true);

		$html = '<div id="' . $this->id . '">';

		$html .= '<div class="rows">';

		if (!is_array($value)) {
			$html .= '<div class="row" style="margin: 0 0 5px 0;"><input type="text" value="' . $item . '" name="' . $this->id . '[]" id="' . $this->id . '" ' . $this->attrToHtml() . '></div>';
		} else {
			foreach ($value as $item) {
				$html .= '<div class="row" style="margin: 0 0 5px 0;"><input type="text" value="' . $item . '" name="' . $this->id . '[]" id="' . $this->id . '" ' . $this->attrToHtml() . '></div>';
			}
		}
		$html .= '</div>';

		$html .= '<div class="actions">';
		$html .= '<a href="#" class="button button-primary add-row"><i class="fa fa-plus-square"></i> Adicionar</a> ';
		$html .= '<a href="#" class="button button-primary delete-row"><i class="fa fa-minus-square"></i> Remover</a>';
		$html .= '</div>';

		$html .= '</div>';
		?>

		<script>
		jQuery(function($) {
			var selector = '#<?=$this->id?>';
			var rows     = selector + ' .rows';
			var row      = selector + ' .rows .row';
			var add      = selector + ' .actions .add-row';
			var del   	 = selector + ' .actions .delete-row';

			$(add).click(function(event) {
				event.preventDefault();
				$(rows).append('<div class="row" style="margin: 0 0 5px 0;"><input type="text" value="" name="<?=$this->id?>[]" id="<?=$this->id?>" <?=$this->attrToHtml()?>></div>');
			});

			$(del).click(function(event) {
				event.preventDefault();
				if ($(row).length > 1) {
					$(row + ':last-child').remove();
				} else {
					alert('Não é possível remover todos os campos!');
				}
			});
		})
		</script>

		<?php
		return $html;
	}

    private function image()
    {
    	// Dados da imagem
		$upload_link = esc_url(get_upload_iframe_src($this->id, $this->post_id));
		$value       = get_post_meta($this->post_id, $this->id, true);
		$image       = wp_get_attachment_image_src($value, 'full');
		$has_image   = is_array($image);
        ?>
			<div>
				<div class="custom-img-container-<?=$this->id?>">
				    <?php if ($has_image) : ?>
				        <img src="<?=$image[0]?>" alt="" style="max-width:100%;" />
				    <?php endif; ?>
				</div>

				<p class="hide-if-no-js">
				    <a class="add-custom-img-<?=$this->id?> <?php if ($has_image) { echo 'hidden';} ?>"
				       href="<?=$upload_link?>">
				        <?php _e('Configurar ' . $this->name); ?>
				    </a>
				    <a class="delete-custom-img-<?=$this->id?> <?php if (!$has_image) { echo 'hidden';} ?>"
				      href="#">
				        <?php _e('Remover ' . $this->name); ?>
				    </a>
				</p>
			</div>

			<input type="hidden" class="custom-img-id-<?=$this->id?>" name="<?=$this->id?>" value="<?=esc_attr($value)?>" />

	        <script>
			jQuery(function($){
				var frame,
					id 		 = '<?=$this->id?>',
					add      = $('.add-custom-img-' + id),
					del      = $('.delete-custom-img-' + id),
					image    = $('.custom-img-container-' + id),
					image_id = $('.custom-img-id-' + id);

				// ADD
				add.on('click', function( event ){
					event.preventDefault();

					if (frame) {
						frame.open();
						return;
					}

					frame = wp.media({
						title: 'Definir <?=$this->name?>',
						button: {
							text: 'Configurar <?=$this->name?>'
						},
						library: {
							type: 'image'
						},
						multiple: false
					});

					frame.on('select', function() {
						var attachment = frame.state().get('selection').first().toJSON();
						image.append('<img src="'+attachment.url+'" alt="" style="max-width:100%;"/>');
						image_id.val(attachment.id);
						add.addClass('hidden');
						del.removeClass('hidden');
					});

					frame.open();
				});

				// DELETE
				del.on('click', function( event ){
					event.preventDefault();

					image.html('');
					add.removeClass('hidden');
					del.addClass('hidden');
					image_id.val('');
				});
			});
	        </script>
        <?php
    }

	private function attrToHtml()
	{
		if (!is_array($this->attrs)) return false;

		$html = '';
		foreach ($this->attrs as $k => $attr)
		{
			$html .= (is_int($k) ? $attr : "{$k}=\"{$attr}\"") . " ";
		}

		return $html;
	}

}
