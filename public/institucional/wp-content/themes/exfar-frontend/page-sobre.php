<?php get_header(); 
    /*
    Template Name: sobre
    */

    $posts = get_posts(
        array(
            'name' => 'sobre',
            'post_type'   => 'page',
            'post_status' => 'publish',
            'numberposts' => 1
        )
    );
    $postSobre = $posts[0];
    $Id = $post->ID;

    $titulo = get_field( "titulo", $Id );
    $missao = get_field( "missão", $Id );
    $visao = get_field( "visão", $Id );
    $valores = get_field( "valores", $Id );

?>

    <main id="about">
        <section class="about-header">
            <div class="about-header-shadow">
                <h2>SOBRE</h2>
            </div>
        </section>
        <section class="about-text">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 animaleft">
                        <h3><?php echo $titulo ?></h3>
                        <p><?php echo $postSobre->post_content; ?></p>
                    </div>
                    <div class="col-md-6 animaright">
                        <img class="about-img" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($Id) );?>" alt="">
                    </div>
                </div>
            </div>
        </section>
        <!-- <section class="topicos-missao">
            <div class="container">
                <div class="row">
                    <div class="col-md-3" id="missao">
                        <h4>MISSÃO</h4>
                        <p><?php echo $missao ?></p>
                    </div>
                    <div class="col-md-3" id="visao">
                        <h4>VISÃO</h4>
                        <p><?php echo $visao ?></p>
                    </div>
                    <div class="col-md-5" id="valores">
                        <h4>Valores</h4>
                        <p><?php echo $valores ?></p>
                    </div>
                </div>
            </div>
        </section> -->

        <section class="topicos-missao">
            <div class="container">
                <div class="blog-slider anima">
                    <div class="blog-slider__wrp swiper-wrapper">
                    <div class="blog-slider__item swiper-slide">
                        <div class="blog-slider__img">
                        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/mission.png" alt="">
                        </div>
                        <div class="blog-slider__content">
                        <div class="blog-slider__title">Missão</div>
                        <div class="blog-slider__text"><?php echo $missao ?></div>
                        </div>
                    </div>
                    <div class="blog-slider__item swiper-slide">
                        <div class="blog-slider__img">
                        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/vision.png" alt="">
                        </div>
                        <div class="blog-slider__content">
                        <div class="blog-slider__title">Visão</div>
                        <div class="blog-slider__text"><?php echo $visao ?></div>
                        </div>
                    </div>

                    <div class="blog-slider__item swiper-slide">
                        <div class="blog-slider__img">
                        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/values.png" alt="">
                        </div>
                        <div class="blog-slider__content">
                        <div class="blog-slider__title">Valores</div>
                        <div class="blog-slider__text"><?php echo $valores ?></div>
                    </div>

                    </div>
                    <div class="blog-slider__pagination"></div>
                </div>
            </div>
        </section>
    </main>

<?php get_footer(); ?>