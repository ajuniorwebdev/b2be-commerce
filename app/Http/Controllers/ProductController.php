<?php

namespace App\Http\Controllers;

use App\Cart;
use Exception;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function products(Request $request, $list)
    {
        return view('products.index');
    }

    public function productsList(Request $request)
    {
        try {
            $url = explode('/', explode(config()['app']['url'],$request->input('list'))[1]);
            $list = end($url);

            parse_str($request->input('search'), $search);

            switch($list) {
                case 'az':
                    $produtos = Product::az($request, $list, $search);
                    break;
            }

            $page = $request->input('page');
            $perPage = 20;
            $offset = ($page * $perPage) - $perPage;

            $produtos = new LengthAwarePaginator(array_slice($produtos, $offset, $perPage, true), count($produtos), $perPage, $page, ['path' => $request->url(), 'query' => $request->query()]);
            $subtotal = Cart::getSubtotalCart();

            $html = view('products.list')->with(['produtos' => $produtos, 'subtotal' => $subtotal])->render();

            return response()
                ->json(['html' => $html, 'pagination' => (string)$produtos->links()], 200);
        }
        catch(Exception $e) {
            report($e);
            return response()
                ->json('Não foi possível carregar a listagem de produtos, contate o administrador do sistema!', 401);
        }
    }
}
