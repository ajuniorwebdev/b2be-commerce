@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="wrapper tiny">
            <div class="login-boxes">
                <div class="login-box">
                    <div class="login">
                        <h1>Entrar</h1>
                        <form method="post" action="{{ route('password.update') }}">
                            @csrf

                            <input type="hidden" name="token" value="{{ $token }}">

                            <ol class="col-group">
                                <li class="col-1 @error('email') erro @enderror">
                                    <label for="email">E-mail</label>
                                    <input id="email" name="email" placeholder="Informe seu e-mail" type="email" value="{{ $email ?? old('email') }}" autocomplete="off">
                                    @error('email')
                                    <span class="msg-erro">{{ $message }}</span>
                                    @enderror
                                </li>
                                <li class="col-1 @error('password') erro @enderror">
                                    <label for="password">Nova senha</label>
                                    <input type="password" id="password" name="password" placeholder="Cadastre uma nova senha" autocomplete="off">
                                    @error('password')
                                    <span class="msg-erro">{{ $message }}</span>
                                    @enderror
                                </li>
                                <li class="col-1">
                                    <label for="password-confirm">Confirmar nova senha</label>
                                    <input type="password" id="password-confirm" name="password_confirmation" placeholder="Confirme sua nova senha" autocomplete="off">
                                </li>
                                <li class="col-1">
{{--                                    <a href="#">Esqueci minha senha</a>--}}
                                    <!-- O Botão foi alterado por um link simples apenas para simular a navegação do sistema -->
                                    <!-- <button class="bt-principal alignright" type="submit">Entrar</button> -->
                                    <button class="bt-principal alignright" type="submit">Resetar Senha</button>
                                </li>
                            </ol>
                        </form>
                    </div>
                </div>

                <div class="login-box">
                    <div class="primeiro-acesso">
                        <h3>Primeiro Acesso</h3>
                        <p>Você é nosso cliente mas ainda não Ainda não tem acesso ao e-commerce <strong>Exfar Distribuidora</strong>?</p>
                        <a href="{{ route('register') }}" class="bt-sec">Primeiro Acesso</a>
                    </div>
                    <div class="solicite-acesso">
                        <h3>Solicite o Cadastro</h3>
                        <p>Ainda não é nosso cliente? Solicite a ficha de cadastro para acessar o e-commerce <strong>Exfar Distribuidora</strong></p>
                        <a href="{{ route('client.request.register') }}" class="bt-sec">Solicitar Cadastro</a>
                    </div>
                </div>

            </div>
        </div>

    </div>

{{--<div class="container">--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Reset Password') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    <form method="POST" action="{{ route('password.update') }}">--}}
{{--                        @csrf--}}

{{--                        <input type="hidden" name="token" value="{{ $token }}">--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>--}}

{{--                                @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">--}}

{{--                                @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row mb-0">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Reset Password') }}--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
@endsection
