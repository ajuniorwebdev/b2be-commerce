@if ($paginator->hasPages())
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="prev disabled"><a href="javascript:void(0)">&lt;</a></li>
        @else
            <li><a href="javascript:void(0)" onclick="Account.branchs( {{ $paginator->currentPage() - 1 }}, true)" rel="prev">&lt;</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled"><span class="page-link">{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active"><a href="javascript:void(0)" onclick="Account.branchs({{ $page }}, true)">{{ $page }}</a></li>
                    @else
                        <li><a href="javascript:void(0)" onclick="Account.branchs({{ $page }}, true)">{{ $page }}</a></li>
                        {{----}}
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li><a href="javascript:void(0)" onclick="Account.branchs({{ $paginator->currentPage() + 1 }}, true)" rel="next">&gt;</a></li>
        @else
            <li class="next disabled"><a href="javascript:void(0)">&gt;</a></li>
        @endif
    </ul>
@endif
