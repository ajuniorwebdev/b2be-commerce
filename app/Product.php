<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Product extends Model
{

    public static function az(Request $request, $list, $search = false)
    {
        $where = false;
        $order_by = null;
        if($request->has('order')) {
            switch($request->input('order')) {
                case 'high':
                    $order_by = ' order by preco_tabela desc';
                    break;
                case 'low':
                    $order_by = ' order by preco_tabela asc';
                    break;
                case 'brandaz':
                    $order_by = ' order by ltrim(descricao_marca) asc';
                    break;
                case 'brandza':
                    $order_by = ' order by ltrim(descricao_marca) desc';
                    break;
                case 'productaz':
                    $order_by = ' order by ltrim(descricao_produto) asc';
                    break;
                case 'productza':
                    $order_by = ' order by ltrim(descricao_produto) desc';
                    break;
            }
        }

        if($request->has('codeproduct_search')) {
            $where .= " AND pro.codprod = {$request->input('codeproduct_search')} ";
        }

        if($request->has('codpromocaomed')) {
            $where .= " AND dis.codpromocaomed = {$request->input('codpromocaomed')} ";
        }

        if (!empty($search['codeproduct_search'])) {
            $where .= " AND pro.codprod = {$search['codeproduct_search']} ";
        }

        if(!empty($search['ean_search'])) {
            $where .= " AND pro.codauxiliar = {$search['ean_search']} ";
        }
        if(!empty($search['brand_search'])) {
            $where .= " AND br.marca like '%{$search['brand_search']}%' ";
        }

        if (!empty($search['primary_search'])) {
            $words = explode(' ', $search['primary_search']);
            $words_matched = null;
            $check_special = false;
            foreach ($words as $word) {
                $special_char = ['-','/','+','ç','*','.','(',')'];
                foreach($special_char as $sp) {
                    if(strpos($word, $sp)) {
                        $check_special = true;
                        break;
                    }
                }
                if($check_special) {
                    $words_matched .= '+"' . $word . '*" ';
                    $check_special = false;
                }
                else {
                    $words_matched .= '+' . $word . '* ';
                }
            }
//            var_dump($words_matched);
//            exit;
            $where .= " AND MATCH(pro.fulltext_pcprodut) AGAINST('{$words_matched}' IN BOOLEAN MODE) ";
//            var_dump($where);
//            exit;
        }

        $cart = Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->first();
        $cart_id = $cart->id;
        $prazo_selecionado = $cart->codplpag;
        $codcli = $cart->codcli;

        $sql = "
            select distinct
                bra.codmarca,
                bra.marca descricao_marca,
                pro.codprod,
                pro.descricao descricao_produto,
                pro.codauxiliar,
                tax.perdescrepasse impostos,
                ci.product_id,
                ci.qt,
                stc.qtest,
                pri.pvenda preco_tabela,
                dis.codpromocaomed,
                dis.percdesc desconto,
                case
                   when stc.qtest < 50 then 'C'
                   when stc.qtest >= 50 and stc.qtest < 150 then 'M'
                   when stc.qtest >= 150 then 'E'
                end stock_volume
            from product pro
            inner join brand bra on pro.codmarca = bra.codmarca
            inner join stock stc on pro.codprod = stc.codprod
            inner join price pri on pro.codprod = pri.codprod
            left join cart_items ci on pro.codprod = ci.product_id and ci.cart_id = {$cart_id}
            left join tax on pri.codst = tax.codst
            left join (
                select des.codprod, des.codmarca, des.codpromocaomed, max(ifnull(des.percdesc,0)) percdesc
                from discount des
                where des.tipopoliticapromocaomed = 'D'
                  and des.tipofv = 'PE'
                group by des.codprod, des.codmarca
            ) dis on pro.codprod = dis.codprod or pro.codmarca = dis.codmarca
            where stc.qtest > 0
              and stc.codfilial = 2
              and pri.numregiao = 1000
              {$where}
              {$order_by};";

        return DB::select($sql);
    }

}
