<?php get_header(); ?>



<main id="home">

	<section id="banner-main">
		<div class="swiper-container template-banner-main">
			<?php     
				$banner = new WP_Query( 
					array('post_type' => 'banner') 
				);

				if( $banner->have_posts()) : 
		    ?>
			<div class="swiper-wrapper">
				<?php  while ($banner->have_posts()) :
	                $banner->the_post();
					$id = get_the_id();
				?> 
				<div class="swiper-slide">
					<figure>
						<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($id) );?>" alt="<?php echo get_the_title();?>" class="img-responsive">
					</figure>
				</div>
				<?php endwhile; ?>
			</div>
			<?php endif; ?>
	</section>

	<section id="secao-sobre">
		<!--SOBRE-->

		<?php
			/*
			Template Name: Home
			*/

			$posts = get_posts(
				array(
					'name' => 'Home',
					'post_type'   => 'page',
					'post_status' => 'publish',
					'numberposts' => 1
				)
			);
			$post = $posts[0];
			$Id = $post->ID;
			
			$titulo = get_field( "titulo", $Id );
		?>
		<div class="container">
			<div class="secaoSobre row">
				<div class="descricao col-lg-6 animaleft">
					<h2 class="titleSobre"><?php echo $titulo ?></h2>
					<p class="textSobre"><?php echo $post->post_content; ?></p>
					<a href="<?php echo home_url('/contato');?>" class="btn btn-primary" >FALE CONOSCO</a>
				</div>
				<div class="topicos col-lg-5 offset-md-1 animaright">
						<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($Id) );?>">
				</div>
			</div>
		</div>
	</section>
	<!--SOBRE FIM-->

	<section id="logos">
		<h2 class="background anima"><span>Fornecedores</span></h2>
		<div class="container">
			<div class="swiper-container template-logos anima">
				<?php     
					$fornecedor = new WP_Query( 
						array('post_type' => 'fornecedor', 'posts_per_page' => -1) 
					);

					if( $fornecedor->have_posts()) : 
				?>
				<div class="swiper-wrapper">
					<?php  while ($fornecedor->have_posts()) :
						$fornecedor->the_post();
						$id = get_the_id();
						$url = get_field( "url" );
					?> 
						<div class="swiper-slide">
						<a href="<?php echo $url ?>" target="_blank">
							<div style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id($id) );?>)"></div>
						</a>
						</div>
					<?php endwhile; ?>
				</div>
				<?php endif; ?>
				<!-- Add Pagination -->
				<div class="swiper-pagination"></div>
			</div>
		</div>
	</section>
	<!--LOGOS FIM-->

</main>

<?php get_footer(); ?>