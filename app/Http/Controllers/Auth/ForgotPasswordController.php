<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Client;
use App\ProfileUser;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $this->credentials($request)
        , function(Message $message) {
            $message->subject($this->getEmailSubject());
            $message->from(env('MAIL_FROM_ADDRESS'), env('APP_NAME'));
        });

        /*return $response == Password::RESET_LINK_SENT*/
        return $response == 'passwords.sent'
            ? $this->sendResetLinkResponse($request, $response)
            : $this->sendResetLinkFailedResponse($request, $response);
    }

    /**
     * Validate the email for the given request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateEmail(Request $request)
    {
        $request->validate(['email' => 'required|email']);
    }

    /**
     * Validate the information for the given request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateInfo(Request $request)
    {
        $profile = $request->input('profile');

        switch($profile) {
            case '1':
                $request->validate([
                    'cnpj' => 'required',
                    'client_code' => 'required'
                ]);
                break;
            case '2':
                $request->validate([
                    'seller_code' => 'required'
                ]);
                break;
        }
    }

    public function getLoginEmailList(Request $request)
    {
        $this->validateInfo($request);

        $profile = $request->input('profile');
        $data = [];

        switch($profile) {
            case '1':
                $data = ['cgcent' => $request->input('cnpj'), 'codcli' => $request->input('client_code')];
                $client = Client::where($data)
                    ->select('codcli')
                    ->first();

                $loginEmailList = User::where(['codcli' => $client->codcli])
                    ->select('email')
                    ->get()
                    ->toArray();

                break;
            case '2':
                $data = ['codusur' => $request->input('seller_code')];
                $seller = ProfileUser::where($data)
                    ->select('codusur')
                    ->first();

                $loginEmailList = User::where(['codusur' => $seller->codusur])
                    ->select('email')
                    ->get()
                    ->toArray();

                break;
        }

        return response()
            ->redirectToRoute('password.request')
            ->with('loginEmailList', $loginEmailList);

    }

    /**
     * Get the response for a successful password reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkResponse(Request $request, $response)
    {
        return $request->wantsJson()
            ? new JsonResponse(['message' => trans($response)], 200)
            : response()->redirectToRoute('login')->with('status', trans($response));
    }

}
