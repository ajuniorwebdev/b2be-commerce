$(document).ready(() => {

    // rules applied only if form branchs exists
    if($('div#branchs').length > 0) {

        // mask in applied in fields
        $('input[name="cgcent"]').mask('00.000.000/0000-00');
        $('input[name="codcli"]').mask('0000000');
        Account.branchs(1, false)

        $('form.search').on('submit', (e) => {
            e.preventDefault()
            Account.branchs(1, true, false)
        })
    }

    if($('form#profile-user').length > 0) {
        $('input[name="cnpj"]').mask('00.000.000/0000-00')
    }

    if($('table#portfolio').length > 0) {
        Account.portfolio_list(1, false, false)
        $('form.search').on('submit', (e) => {
            e.preventDefault()
            Account.portfolio_list(1, true, false)
        })
    }

    if($('div#orders').length > 0 || $('div#overview').length > 0) {
        $( "#data-pedido" ).datepicker({
            showAnim: 'slideDown',
            closeText: "Fechar",
            prevText: "&#x3C;Anterior",
            nextText: "Próximo&#x3E;",
            currentText: "Hoje",
            monthNames: [ "Janeiro","Fevereiro","Março","Abril","Maio","Junho",
                "Julho","Agosto","Setembro","Outubro","Novembro","Dezembro" ],
            monthNamesShort: [ "Jan","Fev","Mar","Abr","Mai","Jun",
                "Jul","Ago","Set","Out","Nov","Dez" ],
            dayNames: [
                "Domingo",
                "Segunda-feira",
                "Terça-feira",
                "Quarta-feira",
                "Quinta-feira",
                "Sexta-feira",
                "Sábado"
            ],
            dayNamesShort: [ "Dom","Seg","Ter","Qua","Qui","Sex","Sáb" ],
            dayNamesMin: [ "Dom","Seg","Ter","Qua","Qui","Sex","Sáb" ],
            weekHeader: "Sm",
            dateFormat: "dd/mm/yy",
            firstDay: 0,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ""
        });
        Account.orders(1, false, '')
        $('form.search').on('submit', (e) => {
            e.preventDefault()
            Account.orders(1, true, false)
        })

        // listenner que informa quando algum campo na busca avançada esta preenchido
        $('div.busca-avancada').find('input').on('change',(e) => {
            let length = 0
            $('div.busca-avancada').find('input').each((i, e) => {
                length = parseInt(length) + parseInt($(e).val().length)
            })

            if(length > 0) {
                $('span.icon-config span.alert').css({ display: 'block' })
            }
            else {
                $('span.icon-config span.alert').css({ display: 'none' })
            }

        })
    }

})

let Account = {
    portfolio_list: (page, loader = false, search = false) => {
        /*scrolling up to page*/
        window.scrollTo(0, 0);

        search = (!search) ? $('form.search').serialize() : search

        $.ajax({
            url: '/portfolio/list',
            method: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Accept': 'application/json'
            },
            data: {
                page: page,
                search: search
            },
            beforeSend: () => {
                if(loader) {
                    $('div.wrappper.fullscreen').prepend(`
                        <div class="loading"></div>
                        <div class="loader-container" style="position: absolute; z-index: 999999; width: 100%; margin: 2% auto;text-align: center;">
                            <div class="loader" style="text-align: center; height: 100px; line-height: 140px">
                                <svg width="48" height="48" viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg" version="1.1">
                                    <path d="M 150,0 a 150,150 0 0,1 106.066,256.066 l -35.355,-35.355 a -100,-100 0 0,0 -70.711,-170.711 z" fill="#0071bb">
                                        <animateTransform attributeName="transform" attributeType="XML" type="rotate" from="0 150 150" to="360 150 150" begin="0s" dur=".5s" fill="freeze" repeatCount="indefinite"></animateTransform>
                                    </path>
                                </svg>
                            </div>
                            <h4>Aguarde um instante...</h4>
                        </div>
                    `)
                }
            },
            complete: () => {
                if(loader) {
                    $('div.wrappper.fullscreen div.loading').remove()
                    $('div.wrappper.fullscreen div.loader-container').remove()
                }
            },
            success: (res) => {
                $('div.table-wrapper table tbody').remove()
                $('div.table-wrapper table').append(res.html)
                $('div.pagination-container ul').remove()
                $('div.pagination-container').append(res.pagination)

                $('a#cliente-topo').on('click', (e) => {
                    $.ajax({
                        url: '/portfolio/change',
                        method: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            'Accept': 'application/json'
                        },
                        data: {
                            portfolio: $(e.target).attr('data-id')
                        },
                        success: () => {
                            location.reload()
                        },
                        error: (err) => {
                            Swal.fire({
                                icon: 'error',
                                title: 'Atenção!',
                                text: err.responseJson.msg,
                                showCancelButton: true,
                                showConfirmButton: false
                            })
                        }
                    })
                })
            },
            error: (err) => {
                Swal.fire({
                    icon: 'error',
                    title: 'Atenção!',
                    text: err.responseJSON.msg,
                    showConfirmButton: false,
                    showCancelButton: true
                })
            }
        })
    },

    orders: (page, loader, search) => {
        /*scrolling up to page*/
        window.scrollTo(0, 0);

        search = (!search) ? $('form.search').serialize() : search

        $.ajax({
            url: '/orders/list',
            method: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                page: page,
                search: search
            },
            beforeSend: () => {
                if(loader) {
                    $('div.wrappper').prepend(`
                        <div class="loading"></div>
                        <div class="loader-container" style="position: absolute; z-index: 999999; width: 100%; margin: 2% auto;text-align: center;">
                            <div class="loader" style="text-align: center; height: 100px; line-height: 140px">
                                <svg width="48" height="48" viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg" version="1.1">
                                    <path d="M 150,0 a 150,150 0 0,1 106.066,256.066 l -35.355,-35.355 a -100,-100 0 0,0 -70.711,-170.711 z" fill="#0071bb">
                                        <animateTransform attributeName="transform" attributeType="XML" type="rotate" from="0 150 150" to="360 150 150" begin="0s" dur=".5s" fill="freeze" repeatCount="indefinite"></animateTransform>
                                    </path>
                                </svg>
                            </div>
                            <h4>Aguarde um instante...</h4>
                        </div>
                    `)
                }
            },
            complete: () => {
                if(loader) {
                    $('div.wrappper div.loading').remove()
                    $('div.wrappper div.loader-container').remove()
                }
            },
            success: (res) => {
                $('div.table-wrapper table tbody').remove()
                $('div.table-wrapper table tfoot').remove()
                $('div.table-wrapper table').append(res.html)
                $('div.pagination-container ul').remove()
                $('div.pagination-container').append(res.pagination)
                // listenner
                // listenner
                $('.pedidos-toggle').on('click', (e) => {
                    $('.faturas-hidden[data-id="'+$(e.target).attr('data-id')+'"]').toggleClass('open');
                    Account.installments($(e.target).attr('data-id'))
                });
            },
            error: (err) => {
                Swal.fire({
                    icon: 'error',
                    title: 'Atenção!',
                    text: err.responseJSON.msg,
                    showCancelButton: true,
                    showConfirmButton: false
                })
            }
        })
    },
    installments: (number_order) => {
        $.ajax({
            url: '/orders/installments/'+number_order,
            method: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Accept': 'application/json'
            },
            data:{},
            success: (res) => {
                if(!res.html) {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Quase lá!',
                        text: res.msg,
                        showConfirmButton: true,
                        showCancelButton: false
                    })
                    $('.faturas-hidden[data-id="'+number_order+'"]').toggleClass('open')
                    return
                }
                $('tr.faturas-hidden[data-id="'+number_order+'"] table tbody').remove()
                $('tr.faturas-hidden[data-id="'+number_order+'"] table').append(res.html)
            },
            error: (err) => {
                Swal.fire({
                    icon: 'error',
                    title: 'Atenção!',
                    text: err.responseJSON.msg,
                    showCancelButton: true,
                    showConfirmButton: false
                })
                $('.faturas-hidden[data-id="'+number_order+'"]').toggleClass('open')
            }
        })
    },
    branchs: (page, loader, search) => {
        /*scrolling up to page*/
        window.scrollTo(0, 0);

        search = (!search) ? $('form.search').serialize() : search

        $.ajax({
            url: '/branchs/list',
            method: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                page: page,
                search: search
            },
            beforeSend: () => {
                if(loader) {
                    $('div.wrappper').prepend(`
                        <div class="loading"></div>
                        <div class="loader-container" style="position: absolute; z-index: 999999; width: 100%; margin: 2% auto;text-align: center;">
                            <div class="loader" style="text-align: center; height: 100px; line-height: 140px">
                                <svg width="48" height="48" viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg" version="1.1">
                                    <path d="M 150,0 a 150,150 0 0,1 106.066,256.066 l -35.355,-35.355 a -100,-100 0 0,0 -70.711,-170.711 z" fill="#0071bb">
                                        <animateTransform attributeName="transform" attributeType="XML" type="rotate" from="0 150 150" to="360 150 150" begin="0s" dur=".5s" fill="freeze" repeatCount="indefinite"></animateTransform>
                                    </path>
                                </svg>
                            </div>
                            <h4>Aguarde um instante...</h4>
                        </div>
                    `)
                }
            },
            complete: () => {
                if(loader) {
                    $('div.wrappper div.loading').remove()
                    $('div.wrappper div.loader-container').remove()
                }
            },
            success: (res) => {
                $('div.table-wrapper table tbody').remove()
                $('div.table-wrapper table tfoot').remove()
                $('div.table-wrapper table').append(res.html)
                $('div.pagination-container ul').remove()
                $('div.pagination-container').append(res.pagination)
                // listenner
                $('.btn-remove').on('click', (e) => {
                    let codcli, cgcent
                    codcli = $(e.target).parent().parent().attr('data-id')
                    cgcent = $(e.target).parent().parent().find('td:first-child').text()
                    $.ajax({
                        url: '/branchs/store',
                        method: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            'Accept': 'application/json'
                        },
                        data: {
                            codcli: codcli,
                            cgcent: cgcent,
                            act: 'R'
                        },
                        success: (res) => {
                            console.log(res.msg)
                            Swal.fire({
                                icon: 'success',
                                title: 'Sucesso!',
                                text: res.msg,
                                showConfirmButton: true
                            })
                        },
                        error: (err) => {
                            Swal.fire({
                                icon: 'error',
                                title: 'Atenção!',
                                text: err.responseJSON.msg,
                                showCancelButton: true,
                                showConfirmButton: false
                            })
                        }
                    })
                })
            },
            error: (err) => {
                Swal.fire({
                    icon: 'error',
                    title: 'Atenção!',
                    text: err.responseJSON.msg,
                    showCancelButton: true,
                    showConfirmButton: false
                })
            }
        })

    }
}