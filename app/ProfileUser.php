<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ProfileUser extends Model
{
    protected $table = 'profile_user';

    public static function portfolio($codusur, $search = false)
    {
        $where = null;

        if(!empty($search['primary_search'])) {
            // $words = explode(' ', $search['primary_search']);
            // $words_matched = null;
            // foreach ($words as $word) {
            //     $words_matched .= "%" . $word . "%";
            // }
            $where .= " and fulltext_field like '%".$search['primary_search']."%'";
        }

        $sql = "
            select
                *
            from (
                select 
                    cli.codcli,
                    cli.cliente,
                    cli.fantasia,
                    cli.bairroent,
                    cli.municent,
                    cli.cgcent,
                    cli.codrede,
                    cliu.codusur,
                    cli.limcred, 
                    (select date_format(str_to_date(vencimento, '%d/%m/%Y'), '%d/%m/%Y') from documents where codcli = cli.codcli and tipo = 'ALVARA DE FUNCIONAMENTO') dtvencalvarafunc,
                    (select date_format(str_to_date(vencimento, '%d/%m/%Y'), '%d/%m/%Y') from documents where codcli = cli.codcli and tipo = 'ALVARA SANITARIO') dtvencalvarasanit,
                    (select date_format(str_to_date(vencimento, '%d/%m/%Y'), '%d/%m/%Y') from documents where codcli = cli.codcli and tipo = 'AUTORIZACAO FUNCIONAMENTO(AFE)') dtvencalvarafuncafe,
                    (select date_format(str_to_date(vencimento, '%d/%m/%Y'), '%d/%m/%Y') from documents where codcli = cli.codcli and tipo = 'CRF') dtvenccrf,
                    cli.bloqueio,
                    concat(cli.codcli,' ', cli.cliente, ' ', cli.fantasia, ' ', cli.cgcent, ' ', cli.codrede, ' ', cli.bairroent, ' ', cli.municent) fulltext_field,
                    (select count(*) from installment where codcli = cli.codcli and dtpag = '') qtd_aberto,
                    (select ifnull(sum(valor), 0) from installment where codcli = cli.codcli and dtpag = '') vr_aberto,
                    (select count(*) from installment where codcli = cli.codcli and dtpag = '' and str_to_date(dtvenc, '%d-%b-%y') > now()) qtd_vencido,
                    (select ifnull(sum(valor), 0) from installment where codcli = cli.codcli and dtpag = '' and str_to_date(dtvenc, '%d-%b-%y') > now()) vr_vencido
                from client cli
                left join client_user cliu on cli.codcli = cliu.codcli
            ) portifolios
            where codusur = {$codusur}
            {$where}
            order by cliente asc
            ;
        ";

        return DB::select($sql);
    }
}
