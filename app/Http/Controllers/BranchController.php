<?php

namespace App\Http\Controllers;

use App\User;
use App\Client;
use App\Cart;
use App\ClientBranchRequest;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BranchController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request)->validate();
        try {
            // verificar se o codigo de cliente e cnpj existem
            $exist = Client::where([
                'codcli' => $request->input('codcli'),
                'cgcent' => $request->input('cgcent')
            ])->exists();
            if(!$exist) {
                $msg = 'Cliente não localizado, se caso não souber o seu código de cliente entre em contato conosco.';
                return $request->wantsJson()
                    ? response()->json(['msg' => $msg], 401)
                    : response()->redirectToRoute('branchs')->with('err', ['msg' => $msg]);
            }

            // verificar se a solicitação já foi feita anteriomente
            $exist_requested = ClientBranchRequest::where([
                'codcli' => $request->input('codcli'),
                'status' => 'P'
            ])->whereNull(['updated_at'])->exists();
            if($exist_requested) {
                $msg = 'Solicitação já feita anteriormente, em breve estaremos entrando em contato.';
                return $request->wantsJson()
                    ? response()->json(['msg' => $msg], 401)
                    : response()->redirectToRoute('branchs')->with('err', ['msg' => $msg]);
            }

            // verificar qual a flag de ação para a solicitação e executar ação
            $client_owner = User::where(['id' => Auth::id()])->first()->codcli;
            switch($request->input('act')){
                case 'I':
                    $data = [
                        'codcli' => $request->input('codcli'),
                        'cgcent' => $request->input('cgcent'),
                        'codcliprinc' => $client_owner,
                        'action_sign' => 'I',
                        'status' => 'P',
                        'created_at' => now(),
                        'updated_at' => null
                    ];
                    if(ClientBranchRequest::create($data)) {
                        $msg = 'A sua solicitação de inclusão foi enviada para nossa equipe, em breve entraremos em contato para confirmação.';
                        return $request->wantsJson()
                            ? response()->json(['success' => true, 'msg' => $msg])
                            : response()->redirectToRoute('branchs')->with('success', ['msg' => $msg]);
                    }
                break;
                case 'R':
                    $data = [
                        'codcli' => $request->input('codcli'),
                        'cgcent' => $request->input('cgcent'),
                        'codcliprinc' => $client_owner,
                        'action_sign' => 'R',
                        'status' => 'P',
                        'created_at' => now(),
                        'updated_at' => null
                    ];
                    if(ClientBranchRequest::create($data)) {
                        $msg = 'A sua solicitação de remoção foi enviada para nossa equipe, em breve entraremos em contato para confirmação.';
                        return $request->wantsJson()
                            ? response()->json(['success' => true, 'msg' => $msg])
                            : response()->redirectToRoute('branchs')->with('success', ['msg' => $msg]);
                    }
                break;
            }

        }
        catch(Exception $e) {
            report($e);
            $msg = 'Algo de errado ocorreu na sua solicitação, estaremos avaliando, tente mais tarde.';
            return $request->wantsJson()
                ? response()->json(['msg' => $msg], 401)
                : response()->redirectToRoute('branchs')->with('err', ['msg' => $msg]);
        }
    }

    protected function validateRequest(Request $request)
    {
        return Validator::make($request->all() ,[
            'cgcent' => 'required',
            'codcli' => 'required'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validateChangeRequest($request)->validate();
        try {
            if(Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->update(['codcli' => $request->input('codcli')])) {
                return response()
                    ->json(['success' => true, 'msg' => 'Filial de compra alterada com sucesso.'], 200);
            } 
        }
        catch(Exception $e) {
            report($e);
            $msg = 'Algo errado ocorreu ao tentar alterar a sua filial de compra, estamos avaliando, tente novamente mais tarde.';
            return response()->json(['msg' => $msg], 401);
        }
    }

    protected function validateChangeRequest(Request $request)
    {
        return Validator::make($request->all(), [
            'codcli' => 'required'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function list(Request $request) {
        parse_str($request->input('search'), $search);
        $branchs = Client::subsidiary($request, $search);
        $page = $request->input('page');
        $perPage = 10;
        $offset = $perPage * ($page - 1);

        $branchs = new LengthAwarePaginator(array_slice($branchs, $offset, $perPage, true), count($branchs), $perPage, $page, ['path' => $request->url(), 'query' => $request->query()]);
        $html = view('account.branch-list')->with(['branchs' => $branchs])->render();

        return response()
            ->json(['html' => $html, 'pagination' => (string)$branchs->links('vendor.pagination.branchs-list-link')], 200);
    }
}
