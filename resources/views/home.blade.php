@extends('layouts.app')

@section('content')
    @if(count($banners) > 0)
        <div id="banner-principal">
            <div class="banner-content">
                @foreach($banners as $banner)
                    <div class="banner">
                        <a href="javascript:void(0)"><img data-id="{{ $banner->id }}" src="https://winthordanfe.s3.us-east-2.amazonaws.com/PRODUTO/{{ $banner->bannerImg->img }}"/></a>
                    </div>
                @endforeach
            </div>
        </div>
    @endif

    @if(count($banners_sec) > 0)
        <div id="banner-secundario">
            <div class="banner-content">
                @foreach($banners_sec as $bsec)
                    <div class="banner">
                        <a href="javascript:void(0)"><img src="{{ asset('/images/banners/'.$bsec->bannerImg->img) }}"/></a>
                    </div>
                @endforeach
            </div>
        </div>
    @endif

    <div class="produtos-content grid4">
        <h1 class="center"><strong>Promoções Imperdíveis</strong><br/>para a sua Farmácia!</h1>
        <div class="wrappper">
            <ul class="produtos-list">
                @foreach($destaques_vitrine as $desv)
                    @if(strpos(get_headers('https://winthordanfe.s3.us-east-2.amazonaws.com/PRODUTO/'. $desv->img)[0], '404') == 0)
                        {{ $img = 'https://winthordanfe.s3.us-east-2.amazonaws.com/PRODUTO/'.$desv->img }}
                    @else
                        {{ $img = 'https://winthordanfe.s3.us-east-2.amazonaws.com/PRODUTO/caixa_padrao.jpeg' }}
                    @endif
                    <li class="produto-item vitr">
                        <div class="miniatura">
                            <a href="javascript:void(0)"><img src="{{ $img }}" width="100%"/></a>
                        </div>
                        <div class="descricao">
                            <form id="prod-vtr">
                                @auth
                                    @if($desv->percdesc > 0)
                                        <div class="desconto hot valign-container">
                                            <div class="valign"><strong>{{ number_format($desv->percdesc, '0', ',', '.') }}%</strong> <span>desconto</span></div>
                                        </div>
                                    @else 
                                        <div class="desconto valign-container">
                                            <div class="valign"><span style="font-size: 1rem">Preço Especial</span></div>
                                        </div>
                                    @endif
                                @endauth

                                <h3 class="nome-produto">
                                    <a href="javascript:void(0)">{{ $desv->descricao }}</a>
                                </h3>

                                @auth
                                    <div class="precos">
                                        @if($desv->percdesc > 0)
                                            <span class="preco-disabled">R$ {{ number_format($desv->pvenda, '2', ',', '.') }}</span>
                                        @endif
                                        <span class="preco">R$ {{ number_format($desv->price, '2', ',', '.') }}</span>
                                    </div>
                                    <div class="qtd">
                                        <span class="qtd-btn minus">-</span>
                                        <input class="qtd" type="text" data-id="{{ $desv->codprod }}" data-prm="{{ $desv->codpromocaomed }}" name="qtd" value="0">
                                        <span class="qtd-btn plus">+</span>
                                    </div>
                                    <button type="submit" class="bt-principal">Adicionar ao Carrinho</button>
                                @endauth
                            </form>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

    <!-- SLIDER PRODUTOS -->
    <div class="produtos-content grid5 slider">
        <h1 class="center">Produtos <strong>Em Destaque</strong></h1>
        <div class="wrappper">
            <ul class="produtos-list">
                @foreach($destaques_randon as $desr)
                    @if(strpos(get_headers('https://winthordanfe.s3.us-east-2.amazonaws.com/PRODUTO/'. $desr->img)[0], '404') == 0)
                        {{ $img = 'https://winthordanfe.s3.us-east-2.amazonaws.com/PRODUTO/'.$desr->img }}
                    @else
                        {{ $img = 'https://winthordanfe.s3.us-east-2.amazonaws.com/PRODUTO/caixa_padrao.jpeg' }}
                    @endif
                    <li class="produto-item vitrr">
                        <div class="miniatura">
                            <a href="javascript:void(0)"><img src="{{ $img }}" width="100%"/></a>
                        </div>
                        <div class="descricao">
                            <form id="prod-vtrr">
                                @auth
                                    @if(intval($desr->percdesc) > 0)
                                        <div class="desconto hot valign-container">
                                            <div class="valign"><strong>{{ number_format(floatval($desr->percdesc), '0', ',', '.') }}%</strong> <span>desconto</span></div>
                                        </div>
                                    @else 
                                        <div class="desconto valign-container">
                                            <div class="valign"><span style="font-size: 1rem">Preço Especial</span></div>
                                        </div>
                                    @endif
                                @endauth
                                <h3 class="nome-produto">
                                    <a href="javascript:void(0)">{{ $desr->descricao}}</a>
                                </h3>
                                @auth
                                    <div class="precos">
                                        @if(intval($desr->percdesc) > 0)
                                            <span class="preco-disabled">R$ {{ number_format($desr->pvenda, '2', ',', '.') }}</span>
                                        @endif
                                        <span class="preco">R$ {{ number_format($desr->price, '2', ',', '.') }}</span>
                                    </div>
                                    <div class="qtd">
                                        <span class="qtd-btn minus">-</span>
                                        <input class="qtd" type="text" data-id="{{ $desr->codprod }}" data-prm="{{ $desr->codpromocaomed }}" name="qtd" value="0">
                                        <span class="qtd-btn plus">+</span>
                                    </div>
                                    <button type="submit" class="bt-principal">Adicionar ao Carrinho</button>
                                @endauth
                            </form>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

@endsection
