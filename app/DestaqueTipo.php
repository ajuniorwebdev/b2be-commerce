<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DestaqueTipo extends Model
{
    protected $fillable = [
        'descricao', 'created_at', 'updated_at'
    ];

    protected $table = 'destaques_tipos';
}
