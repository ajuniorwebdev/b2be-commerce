<?php
// Redes sociais
$general = new PageSetting('general', 'General', 'general');

$localization = $general->addSection('dados', 'Dados adicionais');
$localization->addField('linkSegundaVia', 'Link Segunda Via', 'text');
$localization->addField('phone1', 'Telefone Cabeçalho', 'text');
$localization->addField('phone2', 'Telefone Rodapé', 'text');
$localization->addField('logradouro', 'Endereço Logradouro', 'text');
$localization->addField('complemento', 'Endereço complemento', 'text');
$localization->addField('email', 'email', 'text');
$localization->addField('mapa', 'Url do mapa', 'text');


$general->add();