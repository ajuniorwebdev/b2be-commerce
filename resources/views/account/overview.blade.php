@extends('layouts.app')

@section('content')
    <div class="area-cliente-container">
        <div class="lateral">
            <ul>
                <li class="area-cliente active"><a href="{{ route('overview') }}">Visão Geral</a></li>
                <li class="minha-conta"><span class="icon-cliente-1"></span><a href="{{ route('account') }}">Minha Conta</a></li>
                <li class="minhas-filiais"><span class="icon-filiais-1"></span><a href="@role('customer') {{ route('branchs') }} @elserole('seller') {{ route('portfolio') }} @endrole">@role('customer') Minhas Filiais @elserole('seller') Carteira de Clientes @endrole</a></li>
                <li class="meus-pedidos"><span class="icon-pedidos-1"></span><a href="{{ route('orders') }}">Meus Pedidos</a></li>
                {{--                <li class="chat-representante"><span class="icon-chat-1"></span><a href="chat-representante.html">Fale com o seu representante</a></li>--}}
            </ul>
        </div>
        <div id="overview" class="content">
            <div class="wrapper small">
                <ul class="mobile-only area-cliente-atalhos">
                    <li class="area-cliente" data-aos="fade-up" data-aos-delay="0"><a href="{{ route('overview') }}">Área do Cliente</a></li>
                    <li class="minha-conta" data-aos="fade-up" data-aos-delay="0"><a href="{{ route('account') }}"><span class="icon-cliente-1"></span>Minha Conta</a></li>
                    <li class="minhas-filiais" data-aos="fade-up" data-aos-delay="100"><a href="@role('customer') {{ route('branchs') }} @elserole('seller') {{ route('portfolio') }} @endrole"><span class="icon-filiais-1"></span>@role('customer') Minhas Filiais @elserole('seller') Carteira de Clientes @endrole</a></li>
                    <li class="meus-pedidos" data-aos="fade-up" data-aos-delay="200"><a href="{{ route('orders') }}"><span class="icon-pedidos-1"></span>Meus Pedidos</a></li>
                {{--<li class="chat-representante" data-aos="fade-up" data-aos-delay="300"><a href="chat-representante.html" ><span class="icon-chat-1"></span>Fale com o seu representante</a></li>--}}
                </ul>
                <div class="page-title">
                    <h1>Área do Cliente</h1>
                </div>
                <div class="page-content">
                    @role('customer')
                        <p>Olá <strong>{{ ucwords(strtolower($current_user->fantasia)) }}</strong>. Bem vindo à Área do Cliente da plataforma de e-commerce da <strong>Exfar Distribuidora</strong>. Aqui você pode consultar os seus dados, bem como as informações da sua empresa e das filiais, além de muitas outras funcionalidades, como reimpressão de Boletos e Notas Fiscais.</p>
                    @elserole('seller')
                        <p>Olá <strong>{{ ucwords(strtolower($current_user->nome)) }}</strong>. Bem vindo à Área do Representante da plataforma de e-commerce da <strong>Exfar Distribuidora</strong>. Aqui você pode consultar os seus dados, bem como as informações dos seus clientes e suas filiais, além de muitas outras funcionalidades, como reimpressão de Boletos e Notas Fiscais.</p>
                    @endrole
                    <ul class="area-cliente-atalhos">
                        <li class="minha-conta aos-init" data-aos="fade-up" data-aos-delay="0"><a href="{{ route('account') }}"><span class="icon-cliente-1"></span>Minha Conta</a></li>
                        <li class="minhas-filiais aos-init" data-aos="fade-up" data-aos-delay="100"><a href="@role('customer') {{ route('branchs') }} @elserole('seller') {{ route('portfolio') }} @endrole"><span class="icon-filiais-1"></span>@role('customer') Minhas Filiais @elserole('seller') Carteira de Clientes @endrole</a></li>
                        <li class="meus-pedidos aos-init" data-aos="fade-up" data-aos-delay="200"><a href="{{ route('orders') }}"><span class="icon-pedidos-1"></span>Meus Pedidos</a></li>
                    {{--<li class="chat-representante aos-init" data-aos="fade-up" data-aos-delay="300"><a href="chat-representante.html"><span class="icon-chat-1"></span>Fale com o seu representante</a></li>--}}
                    </ul>
                    <div class="pedidos afterclear">
                        <h4>Últimos Pedidos</h4>
                        <div class="table-wrapper">
                            <table>
                                <thead>
                                <tr>
                                    <th>Nº Pedido</th>
                                    <th>Nº Nota</th>
                                    <th>Data do Pedido</th>
                                    <th>Origem da Venda</th>
                                    <th>Filial</th>
                                    <th>Status</th>
                                    <th class="center" width="100">Valor</th>
                                    <th class="center" width="100">Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="12" style="text-align: center;">
                                        <div class="loader" style="text-align: center; height: 100px; line-height: 140px;">
                                            <svg width="48" height="48" viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg" version="1.1">
                                                <path d="M 150,0 a 150,150 0 0,1 106.066,256.066 l -35.355,-35.355 a -100,-100 0 0,0 -70.711,-170.711 z" fill="#0071bb">
                                                    <animateTransform attributeName="transform" attributeType="XML" type="rotate" from="0 150 150" to="360 150 150" begin="0s" dur=".5s" fill="freeze" repeatCount="indefinite"></animateTransform>
                                                </path>
                                            </svg>
                                        </div>
                                        <h4>Aguarde um instante...</h4>
                                    </td>
                                </tr>
                                </tbody>

                            </table>
                        </div>
                        <a href="{{ route('orders') }}" class="bt-principal alignright">Ver todos os Pedidos</a>
                    </div>
{{--                    <div class="pedidos">--}}
{{--                        <h4>Histórico dos pedidos por mês</h4>--}}
{{--                        <img src="images/dinamico/graph.jpg">--}}
{{--                    </div>--}}

                </div>
            </div>

            @if($config['fullbanner_account'])
                <div class="fullbanner">
                    <a href="#"><img src="{{ asset('/images/banners/'.$config['banner_fix']->bannerImg->img) }}"/></a>
                </div>
            @endif

        </div>
    </div>
@endsection

@section('js')
    <script src="{{ mix('js/account.js') }}"></script>
@endsection