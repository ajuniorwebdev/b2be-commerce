<?php
/**
 * General Setting
 *
 * @author
 */

class GeneralSetting {

	private $f_id;
	private $f_name;
	private $f_description;
	private $f_type;

	function __construct($array)
	{
		$this->f_id = $array[0];
		$this->f_name = $array[1];
		$this->f_description = $array[2];
		$this->f_type = $array[3];
		add_filter( 'admin_init' , array( &$this , 'register_fields' ) );
	}

	function register_fields()
	{
		register_setting( 'general', $this->f_name, 'esc_attr' );
		//add_settings_field( $this->f_id, $this->f_name, array(&$this, 'fields_html') , 'general' );

		$function = 'fields_' . $this->f_type;
		add_settings_field( $this->f_id, '<label for="'.$this->f_name.'">'.$this->f_description.'</label>' , array(&$this, $function) , 'general' );
	}

	function fields_input()
	{
		$value = get_option( $this->f_name, '' );
		echo '<input class="regular-text code" type="text" id="'.$this->f_id.'"" name="'.$this->f_name.'" value="' . $value . '" />';
	}

	function fields_textarea()
	{
		$value = get_option( $this->f_name, '' );
		echo '<textarea class="regular-text code" id="'.$this->f_id.'"" name="'.$this->f_name.'" cols="80" rows="10">' . $value . '</textarea>';
	}

}
