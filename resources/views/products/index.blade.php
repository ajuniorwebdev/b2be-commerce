@extends('layouts.app')

@section('content')
    <div class="produtos-content table-view">
        <h1 class="center">Produtos de A a Z</h1>
        <div class="produtos-tools afterclear fullscreen">
            <div class="busca-form">
                <form class="search">
                    <input type="text" name="primary_search" id="busca-topo" placeholder="Pesquise por Produtos, Marcas, EAN...">
                    <button class="bt-busca icon-busca btn-search"><span>Pesquisar</span></button>
                    <span class="toggle-busca-avancada icon-config"></span>
                    <div class="busca-avancada">
                        <h5>Busca Avançada</h5>
                        <ol class="col-group">
                            <li class="col-3">
                                <label for="busca-marca">Marca</label>
                                <input type="text" name="brand_search">
                            </li>
                            <li class="col-3">
                                <label for="busca-ean">EAN</label>
                                <input type="text" name="ean_search">
                            </li>
{{--                            <li class="col-3">--}}
{{--                                <label for="produto">Código do Produto</label>--}}
{{--                                <input type="text" name="codeproduct_search">--}}
{{--                            </li>--}}
                            <li class="col-1">
                                <a href="javascript:void(0)" class="toggle">Fechar</a>
                                <button class="bt-principal alignright btn-search">Pesquisar</button>
                            </li>
                        </ol>
                    </div>
                </form>
            </div>
            <hr />
            <div class="other-tools afterclear">
                <form class="form-order">
                    <select id="prod_filter_1">
                        <option value="">Ordenar por</option>
                        <option value="high">Maior Preço</option>
                        <option value="low">Menor Preço</option>
                        <option value="brandaz">Marca A-Z</option>
                        <option value="brandza">Marca Z-A</option>
                        <option value="productaz">Produto A-Z</option>
                        <option value="productza">Produto</option>
                    </select>
                </form>
                <div class="legenda">
                    <span>Estoque: </span>
                    <span class="estoque excelente"><span class="icon-estoque-1">Excelente</span></span>
                    <span class="estoque moderado"><span class="icon-estoque-2">Moderado</span></span>
                    <span class="estoque critico"><span class="icon-estoque-1">Crítico</span></span>
                {{--<a href="#" class="export"><span class="icon-export"></span></a>--}}
                {{--<a href="produtos-az.html" class="grid-view"><span class="icon-grade"></span></a>--}}
                {{--<a href="produtos-az-list.html" class="list-view disabled"><span class="icon-lista"></span></a>--}}
                </div>
            </div>
        </div>
        <div class="wrappper fullscreen">
            <div class="table-wrapper">
                <form>
                    <table id="products">
                        <thead>
                        <tr>
                            <th>Marca</th>
                            <th>EAN</th>
                            <th>Cód. Produto</th>
                            <th>Produto</th>
                            <th class="center">Preço</th>
                            <th class="center hidden-mobile">Impostos Unit</th>
                            <th class="center hidden-mobile">Desconto</th>
                        {{--<th class="center hidden-mobile">Preço + Impostos</th>--}}
                            <th class="center hidden-mobile">Preço c/ Desc.</th>
                            <th class="center ">Preço c/ Desc. + Impostos</th>
                            <th class="center" width="100">Qtd.</th>
                            <th class="center ">Subtotal</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td colspan="12" style="text-align: center;">
                                <div class="loader" style="text-align: center; height: 100px; line-height: 140px;">
                                    <svg width="48" height="48" viewBox="0 0 300 300" xmlns="http://www.w3.org/2000/svg" version="1.1">
                                        <path d="M 150,0 a 150,150 0 0,1 106.066,256.066 l -35.355,-35.355 a -100,-100 0 0,0 -70.711,-170.711 z" fill="#0071bb">
                                            <animateTransform attributeName="transform" attributeType="XML" type="rotate" from="0 150 150" to="360 150 150" begin="0s" dur=".5s" fill="freeze" repeatCount="indefinite"></animateTransform>
                                        </path>
                                    </svg>
                                </div>
                                <h4>Aguarde um instante...</h4>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
        <div class="produtos-tools afterclear fullscreen">
            <div class="other-tools afterclear">
                <form>
                    <select id="prod_filter_2">
                        <option value="">Ordenar por</option>
                        <option value="high">Maior Preço</option>
                        <option value="low">Menor Preço</option>
                        <option value="brandaz">Marca A-Z</option>
                        <option value="brandza">Marca Z-A</option>
                        <option value="productaz">Produto A-Z</option>
                        <option value="productza">Produto</option>
                    </select>
                </form>
                <div class="legenda">
                    <span>Estoque: </span>
                    <span class="estoque excelente"><span class="icon-estoque-1">Excelente</span></span>
                    <span class="estoque moderado"><span class="icon-estoque-2">Moderado</span></span>
                    <span class="estoque critico"><span class="icon-estoque-1">Crítico</span></span>
                    {{--<a href="#" class="export"><span class="icon-export"></span></a>--}}
                    {{--<a href="produtos-az.html" class="grid-view"><span class="icon-grade"></span></a>--}}
                    {{--<a href="produtos-az-list.html" class="list-view disabled"><span class="icon-lista"></span></a>--}}
                </div>
            </div>
            <hr />
            <div class="pagination-container">
                <ul class="pagination">
                    <li class="prev disabled"><a href="javascript:void(0)"><</a></li>
                    <li class="active"><a href="javascript:void(0)">1</a></li>
                    <li><a href="javascript:void(0)">2</a></li>
                    <li><a href="javascript:void(0)">3</a></li>
                    <li><a href="javascript:void(0)">4</a></li>
                    <li><a href="javascript:void(0)">5</a></li>
                    <li class="next"><a href="javascript:void(0)">></a></li>
                </ul>
            </div>
            <a href="{{ route('cart') }}" class="bt-principal alignright">Ver Carrinho</a>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{mix('js/products.js')}}"></script>
@endsection
