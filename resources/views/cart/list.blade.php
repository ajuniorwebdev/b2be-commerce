@if(count($cart_items) > 0)
    <tbody>
        @foreach($cart_items as $item)
            <tr>
                <td>{{ $item->marca }}</td>
                <td>{{ $item->product_ean }}</td>
                <td>{{ $item->product_description }}</td>
                <td class="center ">R$ {{ number_format($item->price, '2', ',', '.') }}</td>
                <td class="center  hidden-mobile">R$ {{ number_format((($item->price - (($item->price * $item->discount)/100)) * ($item->tax/100)), '2', ',', '.') }}</td>
                <td class="center hidden-mobile">{{ number_format($item->discount, '2', ',', '.') }}%</td>
                <td class="center  hidden-mobile">R$ {{ number_format(($item->price - (($item->price * $item->discount)/100)), '2', ',', '.') }}</td>
                <td class="center ">R$ {{ number_format(($item->price - (($item->price * $item->discount)/100) + (($item->price - (($item->price * $item->discount)/100)) * ($item->tax/100))), '2', ',', '.') }}</td>
                <td class="center">
                    <div class="qtd">
                        <span class="qtd-btn minus">-</span>
                        <input data-id="{{ $item->product_id }}" type="text" class="qtd" name="qtd" value="{{ $item->qt }}">
                        <span class="qtd-btn plus">+</span>
                    </div>
                </td>
                <td class="center ">R$ {{ number_format((($item->price - (($item->price * $item->discount)/100)) * $item->qt) + ((($item->price - (($item->price * $item->discount)/100)) * $item->qt) * ($item->tax/100)), '2', ',', '.') }}</td>
                <td class="center"><a href="javascript:void(0)" class="del">Remover</a></td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="13"><small>Total: R$ <span>{{ number_format($subtotal[0]->subtotal, '2', ',', '.') }}</span></small></td>
        </tr>
    </tfoot>
@else
    <tbody class="empty">
        <tr>
            <td>Carrinho Vazio!</td>
        </tr>
    </tbody>
@endif
