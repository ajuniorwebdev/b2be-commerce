$(document).ready(() => {
    // field mask
    $('input[name="cnpj"]').mask('00.000.000/0000-00', {reverse: true})
    $('input[name="client_code"]').mask('00000000')
    $('input[name="seller_code"]').mask('00000000')

    $('input[name="profile"]').on('change', (e) => {
        let profile = $(e.target).val()
        $('input[name="email"]').val('')
        switch(profile) {
            case '1':
                $('div.client_form input').each((i, el) => {
                    $(el).val('')
                })
                $('div.seller_form').css({display: 'none'})
                $('div.client_form').css({display: 'block'})
                break
            case '2':
                $('div.seller_form input').each((i, el) => {
                    $(el).val('')
                })
                $('div.client_form').css({display: 'none'})
                $('div.seller_form').css({display: 'block'})
        }
    })
})
$(document).ready(() => {

})