<?php

namespace App;

use App\Cart;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $fillable = [
        'cart_id','product_id','product_ean','product_description','price','discount','tax','qt','price_discount','price_tax','price_discount_tax','subtotal',
        'created_at','updated_at','dt_deleted'
    ];
    protected $table = 'cart_items';

    public function items()
    {
        return $this->belongsTo('App\Cart', 'cart_id', 'id');
    }

    public static function list()
    {
        $cart_id = Cart::where(['user_id' => Auth::id()])->whereNull(['dt_deleted', 'dt_sended'])->first()->id;
        $sql = "
            select
                br.marca,
                ci.product_id,
                ci.product_ean,
                ci.product_description,
                ci.price,
                ci.discount,
                ci.tax,
                ci.qt
            from cart_items ci
            inner join product pr on ci.product_id = pr.codprod
            inner join brand br on pr.codmarca = br.codmarca
            where cart_id = {$cart_id};
        ";
        return DB::select($sql);
    }
}
